<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReservationContractor extends Model
{
    /**
     * Mass variable assignment
     * @var type 
     */
    protected $fillable = ['reserv_id', 'contractor_id'];
    
    /**
     * get the reservation details
     * @return type
     */
    public function reservation_detail(){
        return $this->belongsTo('App\Model\ReservationDetail', 'reserv_id', 'reserv_id');
    }
}
