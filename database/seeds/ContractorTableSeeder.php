<?php

use Illuminate\Database\Seeder;

class ContractorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        DB::table('contractors')->insert([
        	[
        		'firstname' => 'Irma',
        		'lastname' => 'Rutherford',
        		'age' => '22',
        		'contact_number' => '0949000213',
        		'status' => '1',
        	],

        	[
        		'firstname' => 'Maxine',
        		'lastname' => 'Hoppe',
        		'age' => '23',
        		'contact_number' => '0949000212',
        		'status' => '1',
        	],

        	[
        		'firstname' => 'Daphnee',
        		'lastname' => 'Hudson',
        		'age' => '24',
        		'contact_number' => '0949009214',
        		'status' => '1',
        	],

        	[
        		'firstname' => 'Stephania',
        		'lastname' => 'Stamm',
        		'age' => '25',
        		'contact_number' => '0949090211',
        		'status' => '1',
        	],

        	[
        		'firstname' => 'Trever',
        		'lastname' => 'Pagac',
        		'age' => '26',
        		'contact_number' => '0949009015',
        		'status' => '1',
        	],

        	[
        		'firstname' => 'Rayleigh',
        		'lastname' => 'Mraz',
        		'age' => '27',
        		'contact_number' => '0949009216',
        		'status' => '1',
        	]
        ]);
    }
}
