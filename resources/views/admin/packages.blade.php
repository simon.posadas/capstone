@extends('layouts.sidebar') 

@section('title', 'Packages')

@section('content')

<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="fa fa-home"></i> Home</a>
            <a href="#" class="current">Packages</a>
        </div>
        <h1>Packages</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="float-left mb-2">
            <button class="btn btn-success btn-large fa fa-plus addPack"> Add Package</button>
        </div>
        <div class="row-fluid">
            <div class="colspan">
                <div class="widget-box">
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table" id="packagesTbl">
                            <thead>
                                <tr>
                                    <th>Package Name</th>
                                    <th>Price per Person</th>
                                    <th width="20%">Actions</th>
                                </tr>
                            </thead>
                            @foreach($packages as $pack)
                            <tbody>
                                <tr>
                                    <td>{{$pack->package_name}}</td>
                                    <td class="currency">₱{{$pack->package_price}}.00</td>
                                    <td class="btn-center">
                                        <button id ="sizes" class="btn btn-primary fa fa-pencil edittype" data-id="{{ $pack->package_id }}"> Edit</button>
                                        <button id ="sizes" type="submit" class="btn btn-inverse fa fa-eye viewtype" data-id="{{ $pack->package_id }}" > View Food</button>
                                    </td>
                                </tr>
                            </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('modal')
<!-- add modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="addModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add Food</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/addPack">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label>Package Name</label>
                        <input type="text" class="form-control" placeholder="Package Name" name="pname" required>
                    </div>
                    <div class="form-group">
                        <label>Price per person</label>
                        <input type="number" class="form-control" placeholder="Price" name="price" required>
                    </div>
                    <hr>
                    <!-- <div class="control-group">
          <h5 class="control-label">Main Dishes</h3>
          <label class="control-label">Select input</label>
          <div class="controls select2-container">
            <select name="food_type">
              <option disabled>Select food type</option>
              @foreach($mains as $mains)
                <option value="{{$mains->food_id}}">{{$mains->food_name}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="control-group">
          <h5 class="control-label">Soup</h3>
          <label class="control-label">Select input</label>
          <div class="controls select2-container">
            <select name="food_type">
              <option disabled>Select food type</option>
              @foreach($soups as $soups)
                <option value="{{$soups->food_id}}">{{$soups->food_name}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <hr>
        <div class="control-group">
          <h5 class="control-label">Appetizer</h3>
          <label class="control-label">Select input</label>
          <div class="controls select2-container">
            <select name="food_type">
              <option disabled>Select food type</option>
              @foreach($apps as $apps)
                <option value="{{$apps->food_id}}">{{$apps->food_name}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <hr>
        <div class="control-group">
          <h5 class="control-label">Salads</h3>
          <label class="control-label">Select input</label>
          <div class="controls select2-container">
            <select name="food_type">
              <option disabled>Select food type</option>
              @foreach($salads as $salads)
                <option value="{{$salads->food_id}}">{{$salads->food_name}}</option>
              @endforeach
            </select>
          </div>
        </div> -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Add</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end add modal -->

<!-- edit modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="editModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edit Package</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/editPack">
                    {{csrf_field()}}
                    <input type="hidden" name="id" id="editID">
                    <div class="form-group">
                        <label>Package Name</label>
                        <input type="text" class="form-control inpname" name="name">
                    </div>
                    <div class="form-group">
                        <label>Price per person</label>
                        <input type="number" class="form-control inprice" name="price">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end edit modal -->

<!-- delete modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="showFoodModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Hello</h4>

                <table class="table table-bordered data-table">
                    <thead>
                        <tr>
                            <th>Package ID</th>
                            <th>Food Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td id="pack_id" class="pack_id"></td>
                            <td id="food_name" class="food_name"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- end delete modal -->
@endsection

@section('script')
<script>
    $('.addPack').click(function () {
        $('#addModal').modal('show');
    });

    $('.edittype').click(function () {
        $.ajax({
            type: "get",
            url: '/getPack',
            data: {
                id: $(this).data('id')
            },
            dataType: "json",
            success: function (response) {
                response.forEach(function (data) {
                    $('#editModal .id').val($(this).data('id'));
                    $('#editModal .inpname').val(data.package_name);
                    $('#editModal .inprice').val(data.package_price);
                })
            }
        });
        $('#editModal').modal('show');
        $('#editID').val($(this).data('id'));
    });

    $(document).on('click', '.viewtype', function (e) {
      var id = $(this).data('id');
      window.location.href = '/FoodPack/' + id;
    });

    // $('.viewtype').click(function () {
    //     $.ajax({
    //         type: "post",
    //         url: '/getFoodPack',
    //         data: {
    //             id: $(this).data('id')
    //         },
    //         dataType: "json",
    //         success: function (data) {
    //             window.location = '/getFoodPack';
    //         }
    //     });
    //     // window.location = '/viewFood';
    // });
</script>
@endsection