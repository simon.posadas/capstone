<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use DB;
use Carbon;

class CollectionsController extends Controller
{
    public function index()
    {
        $reservations = App\Reservation::all();

        // $reservations->total;
        // $payment = App\Payment::groupBy('reservation_id')
        // ->selectRaw('reservation_id, sum(amount) as sum')
        // ->get();
        // $payment->reservation_id = $payment;
        // dd($payment->reservation_id);
    //    $payment = db::table('reservations')->join('payments', 'reservations.id','=','payments.reservation_id')->select(Db::raw("total - SUM(amount) as hello"), 'payments.reservation_id as id')->groupBy('payments.reservation_id')->get();



        return view('collection.index')
            ->with('reservations', $reservations);
    }

    public function getPayments($id)
    {
        $reservation = App\Reservation::find($id);
        $billing = $reservation->billings;
        $total = $reservation->total;

        $amount_billing = $billing->sum('amount');
        $amount_payment = $reservation->payments->sum('amount');

        $total2 = $amount_billing - $amount_payment;

    	return view('collection.payment')
                ->with('payments', $reservation->payments)
                ->with('total', $reservation->total)
                ->with('total2', $total)
                ->with('total3', $total2)
                ->with('payment', $amount_payment)
                ->with('status', $reservation->status)
                ->with('id', $id);
    }

    public function getBillings($id)
    {
        $reservation = App\Reservation::find($id);

        return view('collection.billing')
                ->with('billings', $reservation->billings);
    }

    public function getSales($id)
    {
        $reservation = App\Reservation::find($id);

        return view('collection.billing')
                ->with('billings', $reservation->billings);
    }

    public function pay(Request $request, $id)
    {
        $amount = $request->get('paid');
        $reservation_id = $request->get('id');
        $payee = $request->get('payee');

        $reservation = App\Reservation::find($id);
        $payment = new App\Payment;
        $code = $payment->generateCode();
        $date = Carbon\Carbon::now()
            ->format('Y-m-d H:m:s');//di pa maayos toooo

        $total_payment = $reservation->total;
        $half_payment = $total_payment * 0.5;
        $payment_details = "";

        DB::beginTransaction();

        if ($amount < $half_payment)
        {
            alert()->error('Pay at least 50%', 'Error')->persistent('Close');
        }
        elseif($amount > $total_payment)
        {
            alert()->error('Paid amount is greater than total amount.', 'Error')->persistent('Close');
        }
        elseif($amount == $total_payment)
        {
            $reservation->status = 3;
            $payment_details = "Full Payment";
            alert()->success('Successfully paid full.', 'Success')->persistent('Close');
        }
        else
        {
            $reservation->status = 2;
            $payment_details = "Downpayment";
            alert()->success('Successfully paid the downpayment.', 'Success')->persistent('Close');
        }
        $payment->code = $payment->generateCode();
        $payment->reservation_id = $reservation->id;
        $payment->amount = $amount;
        $payment->details = $payment_details;
        $payment->payee = $payee;
        $payment->save();
        $reservation->save();

        DB::commit();

        return redirect('collection');
    }
    // report for collection
    public function CollectionReport(Request $request){

        $date = $request->get('month');
        
        $month = DB::table('payments')
        ->selectRaw('sum(amount) as paid')
        ->wheremonth('created_at',$date)
        ->groupBy('reservation_id')
        ->orderby('reservation_id','asc')
        ->get();

        $refund = DB::table('refunds')
        ->SELECT('refunds.name','refunds.received','refunds.reservation_id')
        ->SELECT('refunds.name','refunds.received','refunds.reservation_id')
        ->JOIN('payments','payments.reservation_id','=','refunds.reservation_id')
        ->groupBy('refunds.reservation_id','refunds.name','refunds.received')
        ->get();

        $client = DB::table('payments')
        ->SELECT('reservation_id','payee','created_at')
        ->groupby('reservation_id', 'payee', 'created_at')
        ->orderby('reservation_id','asc')
        ->get();

        $refund2 = $refund->sum('received');
        $payment = $month->sum('paid');
        $gross = $payment + $refund2;
        return \Response::json(['month'=>$month,'client'=>$client,'sum'=>$month->sum('paid'),'refunds'=>$refund,'ref'=>$refund->sum('received'), 'gross'=> $gross]);


    }

    public function CollectionReportYear(Request $request){
       
        $date = $request->get('year');
        
        $month = DB::table('payments')
        ->selectRaw('sum(amount) as paid')
        ->whereYear('created_at',$date)
        ->groupBy('reservation_id')
        ->orderby('reservation_id','asc')
        ->get();

        $refund = DB::table('refunds')
        ->SELECT('refunds.name','refunds.received','refunds.reservation_id')
        ->SELECT('refunds.name','refunds.received','refunds.reservation_id')
        ->JOIN('payments','payments.reservation_id','=','refunds.reservation_id')
        ->groupBy('refunds.reservation_id','refunds.name','refunds.received')
        ->get();

        $client = DB::table('payments')
        ->SELECT('reservation_id','payee','created_at')
        ->groupby('reservation_id', 'payee', 'created_at')
        ->orderby('reservation_id','asc')
        ->get();

        $refund2 = $refund->sum('received');
        $payment = $month->sum('paid');
        $gross = $payment + $refund2;
        // return \Response::json(['month'=>$month,'client'=>$client,'sum'=>$month->sum('paid'),'refunds'=>$refund,'ref'=>$refund->sum('received')]);
        return \Response::json(['month'=>$month,'client'=>$client,'sum'=>$month->sum('paid'),'refunds'=>$refund,'ref'=>$refund->sum('received'), 'gross'=>$gross]);
      

    }

    // end report for collection

    public function full_pay() {
        $id = $_POST['id'];
        $paid = $_POST['paid'];
        $payee = $_POST['payee'];

        $reservation = App\Reservation::find($id);
        $billing = $reservation->billings;
        $payment = $reservation->payments;

        $total = $billing->sum('amount');
        $amount_payment = $payment->sum('amount');

        $amount_payment += $paid;
        
        // dd($total);

        if($amount_payment > $total) {
            alert()->error('Paid amount is greater than balance amount.', 'Error')->persistent('Close');
            return redirect()->route('getpayments', ['id' => $id]);
        } elseif($amount_payment < $total) {
            App\Reservation::where('id', $id)
            ->update([
                'status' => 8
            ]);
            
            $payment = new App\Payment;
            $payment->amount = $paid;
            $payment->reservation_id = $id;
            $payment->code = $payment->generateCode();
            $payment->details = "Partial Payment";
            $payment->payee = $payee;
            $payment->save();
            alert()->success('Successfully paid partially', 'Success')->persistent('Close');
            return redirect()->route('getpayments', ['id' => $id]);
        } else {
            App\Reservation::where('id', $id)
            ->update([
                'status' => 3
            ]);
            
            $payment = new App\Payment;
            $payment->amount = $paid;
            $payment->reservation_id = $id;
            $payment->code = $payment->generateCode();
            $payment->details = "Full Payment";
            $payment->payee = $payee;
            $payment->save();
            alert()->success('Successfully paid full', 'Success')->persistent('Close');
            return redirect()->route('getpayments', ['id' => $id]);
        }



    }

    public function event_done(Request $request) {
        $id = $request->get('id');

        $reservation = App\Reservation::find($id);
        $reservation->status = 6;
        $reservation->save();

        alert()->success('Successfully completed the event', 'Success')->persistent('Close');
        return redirect()->route('getpayments', ['id' => $id]);
    }
}
