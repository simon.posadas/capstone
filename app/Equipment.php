<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * 
 */
class Equipment extends Model
{
    /**
     * Unique key
     * @var type 
     */
    protected $primaryKey = 'id';
    
    /**
     * Disable automatic creation of created_at and updated_at
     * @var type 
     */
    public $timestamps = FALSE;
    
    /**
     * Table to connect
     * @var type 
     */
    protected $table = 'equipments';
    
    /**
     * Mass variable assignment
     * @var type 
     */
    protected $fillable = ['equipment_name', 'cost', 'quantity', 'status'];
    
    public function equipment_type(){
        return $this->belongsTo('App\Model\EquipmentType', 'type_id', 'id');
    }
    
}
