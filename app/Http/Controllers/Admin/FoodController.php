<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\FoodDetail;
use App\FoodType;
use App\FoodCategory;
use \App\Food;
use App\Http\Requests\Reservation\ReservationIdRequest;
use Notification;
use App\Notifications\ReservationApprove;
use App\Notifications\ReservationDisapprove;
use App\Http\Controllers\Controller;

/**
 * 
 */
class FoodController extends Controller {
    /**
     * View directory
     * @var type 
     */
    protected $view = 'admin.';

    // FOOD
    public function food(){
        $var = FoodDetail::join('food_type','food_details.food_type_id','=','food_type.food_type_id')->where('food_details.status',0)->get();
        $types = FoodType::where('status',0)->get();
        $cats = FoodCategory::get();
        return view($this->view . 'food', ['var' => $var,'types' => $types, 'cats' => $cats ]);
    }

    public function getFood(Request $req){
        // dd('flkjasldf');
        $types = FoodDetail::select('*')
            ->where('food_id',$req->id)
            ->get();
        // dd($type);
        return response()->json($types);
    }

    public function addFood(){
        $food = FoodDetail::select('food_name')->get();
        $food_name = $_POST['name'];


        if($food_name == $food){
            alert()->error('Food already exists', 'Error')->persistent('Close');
        } elseif($_POST['food_cat'] == NULL) {
            FoodDetail::insert([ 
                'food_name' => $food_name,
                'food_type_id' => $_POST['food_type'],
                'price' => $_POST['price'],
                ]);
            alert()->success('Successfully added a food', 'Success')->persistent('Close');
        } else {
            FoodDetail::insert([ 
                'food_name' => $food_name,
                'food_type_id' => $_POST['food_type'],
                'category_id' => $_POST['food_cat'],
                'price' => $_POST['price'],
                ]);
            alert()->success('Successfully added a food', 'Success')->persistent('Close');
        }
        
        return redirect('/admin/food');
    }

    public function editFood(){
        $food = FoodDetail::select('food_name')->get();
        $food_name = $_POST['name'];


        if($food == $food_name){
            alert()->error('Food already exists', 'Error')->persistent('Close');
        } else {
            FoodDetail::where('food_id', $_POST['id'])
                ->update(['food_name' => $_POST['name'], 
                'price' => $_POST['price']
            ]);
            alert()->success('Successfully edited a food', 'Success')->persistent('Close');
        }

        return redirect('/admin/food');
    }

    
    public function searchFood(){

        $searchItem = $_GET["searchFood"];
        $types = FoodType::all();
        $categories = FoodCategory::all();

        $details = Food::where('id', $searchItem)->get();

        if(empty($searchItem))
            return redirect()->to('food');

        if(count($details) == 0){
            $details = self::searchFoodByName($searchItem)->get();
            if(count($details) == 0)
            {
                alert()->error('No result to display. Please try again.', 'Error')->persistent('Close');
                return redirect()->back();
            }

            return view('food.index')->with('title', 'Foods')->with('foods', $details)->with('types', $types)->with('categories', $categories);
        }

        return view('food.index')->with('title', 'Foods')->with('foods', $details)->with('types', $types)->with('categories', $categories);

    }

    public function searchFoodByName($name)
    {
        $details = Food::where('name', 'LIKE', '%'.$name.'%');

        return $details;
    }
    
    public function deleteFood(){
        if(FoodDetail::where('food_id', $_POST['id'])->delete()){
            FoodDetail::where('food_id', $_POST['id'])->delete();
            alert()->success('Successfully deleted a food', 'Success')->persistent('Close');
        }else{
        alert()->error('Something went wrong deleting the food', 'Error')->persistent('Close');
        }
        return redirect('/admin/food');
    }
    // END FOOD
}