<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use DB;
use Validator;
use Carbon;

class SoaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reservations = App\Reservation::all();
        
        $days = Carbon\Carbon::now()
        ->addDays(5)
        ->format('Y-m-d');

        return view('report.soaReport')
        ->with('days', $days)
        ->with('reservations', $reservations);
    }

    public function detail()
    {
        $id = $_GET('id');
        $reservations = App\Reservation::all();
        
        $days = Carbon\Carbon::now()
        ->addDays(5)
        ->format('Y-m-d');
        
        return view('report.soaDetail')
        ->with('days', $days)
        ->with('reservations', $reservations);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $reservation = App\Reservation::find($id);
        
        $validator = Validator::make([
            'reservation' => $id
        ], $reservation->deleteRules());

        if($validator->fails())
        {
            return back()->withInput()->withErrors($validator);
        }

        $reservation->status = array_search('Cancelled by the Customer', $this->status_list);
        $reservation->save();

        $request->session()->flash('success-message', 'Reservation Cancelled');
        return redirect('reservation');
    }

    public function cancel() {
        App\Reservation::where('id', $_POST['id'])
        ->update(['status' => 1]);
        /*return response()->json(['success' => true, 'title' => 'Success', 'message' => 'Successfuly mark as paid 1st half the reservation']);*/
        alert()->success('Successfully cancelled the reservation', 'Success')->persistent('Close');
        return redirect('reservation');
    }
    
    public function getFoods($id){

        $data = db::table('package_food')->join('foods', 'package_food.food_id', '=', 'foods.id')->where('package_id', $id)->get()->toArray();

    }

    public function reschedule(){
        $date = $_POST['resched_date'];
        $time = $_POST['resched_time'];
        $id = $_POST['id'];
        $charge = App\Reservation::find($id);
        $charge = $charge->total;
        $charge *= 1.05;
        $datetime = $date . " " . $time ;
        App\Reservation::where('id', $id)
        ->update([
            'event_date', $datetime,
            'total', $charge
        ]);
    }
}
