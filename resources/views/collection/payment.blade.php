@extends('layouts.template') 

@section('content')

<!--main-container-part-->
<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="icon icon-home"></i> Home</a>
            <a href="/collection">Collection</a>
            <a href="#" class="current">Payment</a>
        </div>
        <h1>Payments</h1>
    </div>
    <!--End-breadcrumbs-->
    <div class="container-fluid">
        <hr>
        
        @if(session()->has('success-message'))
        <div class="alert alert-success">
            <ul>
                {{ session()->get('success-message') }}
            </ul>
        </div>
        @endif


        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="row-fluid">
            <div class="span12">
                <div class="pull-left">
                    @if($status == 3)
                        <button id ="sizes1" class="btn-large btn-success icon icon-check event_done" data-id="{{$id}}"> Event Done</button>
                    @elseif($status == 2 or $status == 8)
                        <button id ="sizes1" class="btn-large btn-primary icon icon-check show-payment-modal"
                        data-toggle="modal"
                        data-amount="{{ $payment }}"
                        data-total="{{ $total3 }}"
                        data-target="#fullPaymentModal"
                        data-id="{{$id}}"> Pay</button>
                    @endif
                </div>
                <div class="widget-box">
                    <div class="widget-title">
                        <i class="fa fa-th fa-sm pt-1"></i>
                        <h5 style="margin-right:150px">Payments</h5>
                        <h5>Total Remaining Balance: &#8369; {{ number_format($total3, 2) }}</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Code</th>
                                    <th>Details</th>
                                    <th>Amount</th>
                                    <th>Payee</th>
                                    <th>Transaction Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($payments as $payment)
                                <tr>
                                    <td>{{ $payment->id }}</td>
                                    <td>{{ $payment->code }}</td>
                                    <td>{{ $payment->details }}</td>
                                    <td style="text-align: right">&#8369; {{ number_format($payment->amount, 2) }}</td>
                                    <td>{{ $payment->payee }}</td>
                                    <td>{{ $payment->created_at }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end-main-container-part-->

@include('modal.collection.pay-full')
@include('modal.collection.event')
@endsection 

@section('script')
<script type="text/javascript">
    $(document).ready(function(){

$('.show-payment-modal').click(function (event) {
    id = $(this).data('id')
    amount = $(this).data('amount')
    total = $(this).data('total')

    $('#fullPaymentModal').find('#paid_fullID').val(id);
    $('#fullPaymentModal').find('#total_pay').text(total);
    $('#fullPaymentModal').find('#amount_paid').text(amount);

    $('#reservationPaymentModal').find('#payment-confirm').on('click', function () {

        var id = $('#reservation-id').val()
        $('#payReservationForm').attr('action', function(i, value) {
            return "{{ url('reservation') }}/" + id + '/pay' ;
        });

        $('#payment-confirm').submit();
    });

})
})

    $('.event_done').click(function () {
        $('#event_doneID').val($(this).data('id'));
        $('#eventDoneConfirmationModal').modal('show');
    });
</script>
@endsection