@extends('layouts.sidebar')

@section('title', 'WIP')

@section('content')

<!--main-container-part-->
<meta name="csrf-token" content="{{ csrf_token() }}">
<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="fa fa-home"></i> Home</a>
            <a href="#" class="current">Budget</a>
        </div>
        <h1>Budget</h1>
    </div>
    <div class="container-fluid">
        <form method="post" action="/addBudget" class="form-horizontal">
            <div class="control-group">
             <label id="label-margin" class="control-label">Category :</label>
                    <div id='divCategoryPicker'class="controls">
                        <select id="dropdownz" class="span4">
                            <option class="sizee" disabled>Select event type</option>
                            @foreach($allCategories as $category)
                                <option class="sizee">{{$category->category_name}}</option>
                            @endforeach
                        </select>
                    </div>
               </div>
            </div>
        </form>
        <div class="row-fluid">
            <div class="colspan">
                <div class="widget-box bordered">
                        <table id="dashTable" class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Specifics</th>
                                    <th width="20%">Price</th>
                                    <th width="20%">Action</th>
                            </thead>
                            <tbody>
                                @foreach($allData as $data)
                                <tr>
                                    <td><input type="text" class="span12" placeholder="Text Here" value="{{$data->specifics}}" /></td>
                                    <td width="20%"><div class="input-prepend"> <span class="add-on">PHP</span>
                                         <input type="text" placeholder="Price" class="span9 input-price" value="{{$data->price}}"/></div></td>
                                    <td width="20%" class="btn-center"></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                      </div>
                      <button id ="sizes" class="btn btn-primary icon-plus addRow">Add Button</button>
                      <button id ="sizes" class="btn btn-success icon-star saveAll">Save</button>
                      <div class="total">
                        <h3 class="total_position">Total Price : 
                            <div class="input-prepend"> <span class="add-on">PHP</span>
                            <input type="text" placeholder="Price" class="span9 total-price" readonly></div>
                        </h3>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--main-container-part-->

@endsection 

@section('script')
    <script type="text/javascript">
        $(document).ready(function(){

            $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
            });

            var selectedCategory = 1;

            $('#dashTable').on('change keypress focusout keyup focusin mouseout mousein', '.input-price', function(){

                //console.log('running')

                    var total = 0;

                    $('.input-price').each(function ()
                    {
                        var input = $(this).val()
                        if(!isNaN(parseFloat(input)) && isFinite(input))
                            total = parseFloat(total) + parseFloat(input)
                    
                    })

                    //console.log(total)

                    $('.total-price').val(total)
            })

            $('.input-price').trigger('change')


            $('.addRow').on('click',function(){
                var newRow = $("<tr>");
                var cols = "";

                cols += `
                    <td>
                        <input type="text" class="span12 inputName" placeholder="Text Here"/>
                    </td>`;
                cols += `
                    <td width="20%">
                        <div class="input-prepend"> 
                            <span class="add-on">PHP</span> 
                            <input type="text" placeholder="Price" class="span9 input-price inputPrice">
                        </div>
                    </td>`;
                cols += `
                    <td width="20%" class="btn-center">
                        <button id="sizesxc" class="btn btn-danger icon-minus delRow">
                            Remove
                        </button>
                    </td>`;

                cols += `</tr>`;

                newRow.append(cols);
                $('#dashTable > tbody').append(newRow);
            });

            
            $('#dashTable > tbody').on('click','.delRow',function(){
                    $(this).closest('tr').remove();
            });

            $('#dashTable > tbody').on('click','.addSpecifics',function(){
                var newRow = $("<tr>");
                var cols = "";

                cols += '<td><input type="text" class="span12 inputName" placeholder="Text Here"/></td>';
                cols += '<td width="20%"><div class="input-prepend"> <span class="add-on">PHP</span> <input type="text" placeholder="Price" class="span9 inputPrice"></div></td>';

                newRow.append(cols);
                $('#dashTable > tbody').append(newRow);
            });
            
        
            $('.saveAll').on('click',function(){
                var dataToSave = [];
                var inputCat = selectedCategory; //from category box

                //console.log(inputCat);


                $('#dashTable > tbody > tr').each(function(){
                    $this = $(this);
                    var inputName = $this.find('input.inputName').val();
                    var inputPrice = $this.find('input.inputPrice').val();

                    var data = {
                        inputName: inputName,
                        inputPrice: inputPrice
                    };

                    dataToSave.push(data);
                })

                //console.log(dataToSave);

                $.ajax({
                    type: 'POST',
                    url: '{{ url("admin/addBudget") }}',
                    data: {
                        rows: dataToSave,
                        category: inputCat
                    },
                    dataType: 'JSON',
                    success: function(msg){
                        console.log('insert to budget_form: SUCCESS');
                    }
                });
            });
            
            $("#dropdownz").on('change',function(){
                $('select option:selected').each(function(){
                    console.log($(this).text());
                    selectedCategory = $(this).text();
                });
            }) 
        })   
        
    </script>
@endsection