<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Model\PackageDetail;
use App\Model\PackageFood;
use App\Model\FoodDetail;
use App\Model\FoodType;
use App\Http\Requests\Reservation\ReservationIdRequest;
use Notification;
use App\Notifications\ReservationApprove;
use App\Notifications\ReservationDisapprove;
use App\Http\Controllers\Controller;

/**
 * 
 */
class PackageController extends Controller {
    /**
     * View directory
     * @var type 
     */
    protected $view = 'admin.';

    // PACKAGES
    public  function packages()
    {
        $package = PackageDetail::where('status', 0)->get();
        $food = FoodType::where('status', 0)->get();
        $main = FoodDetail::where('food_type_id', 10)->get();
        $soup = FoodDetail::where('food_type_id', 2)->get();
        $app = FoodDetail::where('food_type_id', 11)->get();
        $salad = FoodDetail::where('food_type_id', 12)->get();
        
        return view($this->view . 'packages', ['packages' => $package, 'foods' => $food, 'mains' => $main, 'soups' => $soup, 'apps' => $app, 'salads' => $salad]);
    }

    public function getPack(Request $req){
        $types = PackageDetail::where('package_id',$req->id)->get();
        // dd($types);
        return response()->json($types);
    }

    public function addPack(){
        $check = PackageDetail::select('package_name')->get();
        $pname = $_POST['pname'];

        if($pname == $check){
            alert()->error('Package name already exists', 'Error')->persistent('Close');
        } else {
        PackageDetail::insert([ 
             'package_name' => $pname,
             'package_price' => $_POST['price']
            ]);
        alert()->success('Successfully added a package', 'Success')->persistent('Close');
        }

        return redirect('/admin/packages');
    }

    public function editPack(){
        PackageDetail::where('package_id', $_POST['id'])
            ->update(['package_name' => $_POST['name'],
            'package_price' => $_POST['price']
        ]);
        
        alert()->success('Successfully edited a package', 'Success')->persistent('Close');
        
        return redirect('/admin/packages');
    }

    public function deletePack(){
        
        PackageDetail::where('package_id', $_POST['id'])->delete();
        alert()->success('Successfully deleted a package', 'Success')->persistent('Close');
        
        return redirect('/admin/packages');
    }
    // END PACKAGES


    //  PACKAGE FOOD
    public function getFoodPack($id){
        $mains = PackageFood::select('pd.package_id as pid', 'pd.package_name as pname', 'fd.food_name as name')
            ->join('food_details as fd','package_food.food_id','=','fd.food_id')
            ->join('package_details as pd','package_food.package_id','=','pd.package_id')
            ->where('package_food.package_id', $id)
            ->where('food_type_id', 10)
            ->get();
        
        $soups = PackageFood::select('pd.package_id as pid', 'pd.package_name as pname', 'fd.food_name as name')
            ->join('food_details as fd','package_food.food_id','=','fd.food_id')
            ->join('package_details as pd','package_food.package_id','=','pd.package_id')
            ->where('package_food.package_id', $id)
            ->where('food_type_id', 2)
            ->get();

        $appetizers = PackageFood::select('pd.package_id as pid', 'pd.package_name as pname', 'fd.food_name as name')
            ->join('food_details as fd','package_food.food_id','=','fd.food_id')
            ->join('package_details as pd','package_food.package_id','=','pd.package_id')
            ->where('package_food.package_id', $id)
            ->where('food_type_id', 11)
            ->get();

        $salads = PackageFood::select('pd.package_id as pid', 'pd.package_name as pname', 'fd.food_name as name')
            ->join('food_details as fd','package_food.food_id','=','fd.food_id')
            ->join('package_details as pd','package_food.package_id','=','pd.package_id')
            ->where('package_food.package_id', $id)
            ->where('food_type_id', 12)
            ->get();

        // dropdowns
        $mains2 = FoodDetail::select('food_id', 'food_name')
            ->where('food_type_id', 10)
            ->get();
        
        $soups2 = FoodDetail::select('food_id', 'food_name')
            ->where('food_type_id', 2)
            ->get();

        $appetizers2 = FoodDetail::select('food_id', 'food_name')
            ->where('food_type_id', 11)
            ->get();

        $salads2 = FoodDetail::select('food_id', 'food_name')
            ->where('food_type_id', 12)
            ->get();

        $pid = $id;
        // $foods = $_POST['id'];
        // if($package == $_POST['id'])
        // return response()->json($foods);
        return view($this->view . 'packfood',
        ['mains' => $mains,
        'soups' => $soups,
        'appetizers' => $appetizers,
        'salads' => $salads,
        'mains2' => $mains2,
        'soups2' => $soups2,
        'appetizers2' => $appetizers2,
        'salads2' => $salads2,
        'pid' => $pid
        ]);
    }

    public function addMain(){
        PackageFood::insert([
            'package_id' => $_POST['id'],
            'food_id' => $_POST['food_main']
        ]);

        alert()->success('Successfully added a food', 'Success')->persistent('Close');

        return redirect()->back();
        // return redirect('');
    }

    public function addSoup(){
        PackageFood::insert([
            'package_id' => $_POST['id'],
            'food_id' => $_POST['food_soup']
        ]);

        alert()->success('Successfully added a food', 'Success')->persistent('Close');
        return redirect()->back();
    }

    public function addApp(){
        PackageFood::insert([
            'package_id' => $_POST['id'],
            'food_id' => $_POST['food_app']
        ]);

        alert()->success('Successfully added a food', 'Success')->persistent('Close');

        return redirect()->back();
    }

    public function addSalad(){
        PackageFood::insert([
            'package_id' => $_POST['id'],
            'food_id' => $_POST['food_salad']
        ]);

        alert()->success('Successfully added a food', 'Success')->persistent('Close');

        return redirect()->back();
    }
    // END PACKAGE FOOD
}