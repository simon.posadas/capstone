<?php

return [
    'event_types' => ['wedding' => 'Wedding', 'conference' => 'Conference', 'bday' => 'Birthday',
        'baptism' => 'Baptism', 'anniv' => 'Anniversary'],
    'status' => [
        '0' => 'Pending',
        '1' => 'Cancelled by the customer',
        '2' => 'Paid (Downpayment)',
        '3' => 'Fully Paid',
        '4' => 'Cancelled (with refund)',
        '5' => 'Cancelled (with no refund)',
        '6' => 'Done'
    ]
];

