<?php

namespace App\Http\Controllers\Admin;

use DB;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ReservationDetail;
use App\Model\ReservationContractor;
use App\Model\PackageDetail;
use App\Model\EventDetail;
use App\Model\EventType;
use App\Model\Contractor;
use App\Model\Equipment;
use App\Model\Payment;
use App\Model\PackageFood;
use App\Model\CustomerInfo;
use App\Http\Requests\Reservation\ReservationIdRequest;
use App\Model\ReservationEquipment;
use App\Model\ReservationWorker;

/**
 * 
 */
class ReservationController extends Controller {
    // RESERVATION
    /**
     * View directory
     */
    protected $view = 'admin.';

    // wip
    public function wip() {

        $details = ReservationDetail::where('status', '!=', 1)
            ->where('status', '!=', 4)
            ->where('status', '!=', 5)
            ->where('status', '!=', 6)
            ->join('customer_info', 'customer_info.cust_id', '=', 'reservation_details.cust_id')
            ->join('event_details', 'event_details.event_id', '=', 'reservation_details.event_id')
            ->get();

        return view($this->view . 'wip', [
            'details' => $details
            ]);
    }
    // end wip

    /**
     * Populate the list of reservation
     * @return type
     */
    public function reservation() {

        $details = ReservationDetail::where('status', '=', 0)
            ->join('customer_info', 'customer_info.cust_id', '=', 'reservation_details.cust_id')
            ->join('event_details', 'event_details.event_id', '=', 'reservation_details.event_id')
            ->get();

        return view($this->view . 'reservation.reservations', [
            'details' => $details
            ]);
    }

    public function downpay() {

        $details = ReservationDetail::where('status', '=', 2)
            ->join('customer_info', 'customer_info.cust_id', '=', 'reservation_details.cust_id')
            ->join('event_details', 'event_details.event_id', '=', 'reservation_details.event_id')
            ->get();

        return view($this->view . 'reservation.downpay', [
            'details' => $details
            ]);
    }

    public function cancelled() {

        $details = ReservationDetail::where('status', '=', 1)
            ->join('customer_info', 'customer_info.cust_id', '=', 'reservation_details.cust_id')
            ->join('event_details', 'event_details.event_id', '=', 'reservation_details.event_id')
            ->get();

        return view($this->view . 'reservation.cancelled', ['details' => $details]);
    }


    public function refund() {

        $details = ReservationDetail::where('status', '=', 4)
            ->join('customer_info', 'customer_info.cust_id', '=', 'reservation_details.cust_id')
            ->join('event_details', 'event_details.event_id', '=', 'reservation_details.event_id')
            ->get();

        return view($this->view . 'reservation.refund', [
            'details' => $details
            ]);
    }

    public function monitoring() {

        $details = ReservationDetail::where('status', '!=', 1)
            ->where('status', '!=', 4)
            ->where('status', '!=', 5)
            ->where('status', '!=', 6)
            ->join('customer_info', 'customer_info.cust_id', '=', 'reservation_details.cust_id')
            ->join('event_details', 'event_details.event_id', '=', 'reservation_details.event_id')
            ->get();

        $contractors = Contractor::get();

        $count = ReservationContractor::get();
        // $counts = $count[0]->reserv_id;

        // dd($details,$count);
        return view($this->view . 'monitoring', [
            'details' => $details,
            'contractors' => $contractors,
            'counts' => $count
            ]);
    }

    public function budget() {

            $allData = DB::table('category_specifics')->get(); //TODO GET FROM SPECIFIC RESERVATION
            $allCategories = DB::table('expense_category')->get();

            //dd($allData);

            return view($this->view . 'budget',[
                'allData' => $allData,
                'allCategories' => $allCategories
            ]);
    }

    public function addBudget(Request $req){

        $rows = $req->input('rows');
        //$cat = $req->id('category_id');
        //dd($rows);
        $category = $req->get('category');


        // $category_id = DB::table('expense_category')
        //     ->where('category_name','==',$category)
        //     ->get();

        foreach($rows as $row){

            if(isset($row['inputName']))
            {
                $name = $row['inputName'];
                $price = $row['inputPrice'];

                DB::table('category_specifics')->insert([
                    'budget_id'=> '1', //TODO CHANGE THE RESERVATION ID
                    'specifics'=> $name,
                    'price' => $price,
                    'category_id' =>  $category
                ])
                ;   
            }
               // dd('category_id');
        }
        
    }

    public function viewPack(Request $req){
        dd('flkjasldf');
        $types = PackageFood::select('pd.package_name as pname', 'fd.food_name as name')
            ->join('package_details as pd','package_food.package_id','=','pd.package_id')
            ->where('package_food.package_id', $req->id)
            ->get();
        // dd($type);
        return response()->json(['type' => $types]);
    }

    public function packView(Request $req){
        // dd($req->id);
        $view = PackageFood::select('pd.package_name as pname', 'fd.food_name as name', 'ft.food_type_name as type')
            ->join('package_details as pd','package_food.package_id','=','pd.package_id')
            ->join('food_details as fd','package_food.food_id','=','fd.food_id')
            ->join('food_type as ft','fd.food_type_id','=','ft.food_type_id')
            ->where('package_food.package_id', $req->id)
            ->get();
        $price = PackageDetail::select('package_price as price')
            ->where('package_id', $req->id)
            ->get();
        $prices = $price[0]->price;
            // dd($view);
        return view($this->view . 'viewpackage', ['view' => $view, 'price' => $prices]);
    }

    public function getDetails(Request $req){
        // $details = $req->id;
        $details = ReservationDetail::join('event_details as ed', 'reservation_details.event_id', '=', 'ed.event_id')
            ->where('reserv_id', $req->id)
            ->get();

        // $details = PackageFood::select('pd.package_name as pname', 'fd.food_name as name', 'ft.food_type_name as type')
        //     ->join('package_details as pd','package_food.package_id','=','pd.package_id')
        //     ->join('food_details as fd','package_food.food_id','=','fd.food_id')
        //     ->join('food_type as ft','fd.food_type_id','=','ft.food_type_id')
        //     ->where('package_food.package_id', $req->id)
        //     ->get();
        return response()->json(['details' => $details]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function goCreate(){
        $events_data = [];
        foreach (ReservationDetail::all() as $reservation_detail) {
            $obj = new \stdClass();
            $event_detail = $reservation_detail->event_detail;
            $obj->title = 'fasfas';
            $obj->start = $reservation_detail->reserv_date . ' T ' . $reservation_detail->reserv_time;
            $obj->end = $reservation_detail->reserv_date . ' T '
                    . date("H:i:s", strtotime("$reservation_detail->reserv_date $reservation_detail->reserv_time + 4 hours"));
            $obj->allDay = false;
            $events_data [] = $obj;
        }

        // dd($events_data);

        $package = PackageDetail::select('package_id', 'package_name', 'package_price')
        ->get();
        $type = EventType::get();
        return view($this->view . 'reservation_create', ['packages' => $package, 'types' => $type, 'events_data' => $events_data]);
    }

    public function createReservation(){

        $count = ReservationDetail::where('reserv_date', date('Y-m-d', strtotime($_POST['reservedate'])))
            ->count('reserv_date');

        $price = PackageDetail::select('package_price')
            ->where('package_id', $_POST['package'])
            ->get();

        $price2 = $price[0]->package_price;
        $pipno = $_POST['pipno'];
        // dd($price2);
        $total = $price2 * $pipno;

        // dd($total);

        if($count == 3){
            alert()->error('Exceeded max event per day' , 'Error')->persistent('Close');
            return redirect('/admin/reservation_create');
        } else {
            CustomerInfo::insert([
                'cust_fname' => $_POST['fname'],
                'cust_lname' => $_POST['lname'],
                'gender' => $_POST['gender'],
                'email' => $_POST['email'],
                'contNo' => $_POST['contno'],
                'address' => $_POST['address']
            ]);

            $customer = CustomerInfo::orderBy('cust_id', 'desc')
                ->first();  

            Payment::insert([
                'total' => $total
            ]);

            $payment = Payment::orderBy('payment_ID', 'desc')
                ->first();  

            EventDetail::insert([
                'event_type' => $_POST['event_type'],
                'place' => $_POST['location']
            ]);

            $event = EventDetail::orderBy('event_id', 'desc')
                ->first();  

            ReservationDetail::insert([
                'reserv_date' => date('Y-m-d', strtotime($_POST['reservedate'])),
                'reserv_guestNo' => $_POST['pipno'],
                'cust_id' => $customer->cust_id,
                'event_id' => $event->event_id,
                'package_id' => $_POST['package'],
                'reserv_time' => $_POST['time'],
                'payment_ID' => $payment->payment_ID
            ]);    

            

            alert()->success('Successfully booked a reservation. Amount to pay ₱' . $total, 'Success')->persistent('Close');
            
            return redirect('/admin/reservation');
        }
    }

    public function get_Paid(Request $req) {
        dd('flasj');
        $details = ReservationDetail::where('reserv_id', $req->id)
            ->get();
        dd($details);
        return response()->json($details);

    

    }

    // change status
    public function downpayment() {
        $reserv = ReservationDetail::where('reserv_id', $_POST['id'])
            ->get();
        
        $payment = Payment::where('payment_id', $reserv[0]->payment_ID)
            ->get();

        $total = $payment[0]->total;
        // dd($total);

        if($_POST['paid'] < $total){
            ReservationDetail::where('reserv_id', $_POST['id'])
            ->update([
                'status' => 2
            ]);
            
            Payment::where('payment_ID', $reserv[0]->payment_ID)
            ->update([
                'first_pay' => $_POST['paid']
            ]);

            alert()->success('Successfully paid the downpayment', 'Success')->persistent('Close');
        } else {
            alert()->error('Exceeded the total amount to pay of ₱' . $total, 'Error')->persistent('Close');
            return redirect('/admin/reservation');
        }
        
        return redirect('/admin/downpayment');
    }

    public function cancel() {
        ReservationDetail::where('reserv_id', $_POST['id'])
            ->update(['status' => 1]);
            /*return response()->json(['success' => true, 'title' => 'Success', 'message' => 'Successfuly mark as paid 1st half the reservation']);*/
        alert()->success('Successfully cancelled the reservation', 'Success')->persistent('Close');
        return redirect('/admin/reservation');
    }


    public function full_pay() {
        $reserv = ReservationDetail::where('reserv_id', $_POST['id'])
            ->get();
            
        $payment = Payment::where('payment_id', $reserv[0]->payment_ID)
        ->get();

        $total = $payment[0]->total;
        $first = $payment[0]->first_pay;
        $second = $_POST['paid'];
        $full = $first + $second;

        // dd($total);

        if($full == $total){
            ReservationDetail::where('reserv_id', $_POST['id'])
            ->update([
                'status' => 3
            ]);
            
            Payment::where('payment_ID', $reserv[0]->payment_ID)
            ->update([
                'second_pay' => $_POST['paid']
            ]);
                /*return response()->json(['success' => true, 'title' => 'Success', 'message' => 'Successfuly mark as paid 1st half the reservation']);*/
            alert()->success('Successfully paid full', 'Success')->persistent('Close');
            return redirect('/admin/reservation');
        } elseif ($full > $total){
            alert()->error('Exceeded the total amount to pay of ₱' . $total, 'Error')->persistent('Close');
            return redirect('/admin/downpayment');
        } elseif ($full < $total) {
            alert()->error('Insufficient amount ₱' . $_POST['paid'], 'Error')->persistent('Close');
            return redirect('/admin/downpayment');
        }
    }

    public function cancel_refund() {
        ReservationDetail::where('reserv_id', $_POST['id'])
            ->update(['status' => 4]);
            /*return response()->json(['success' => true, 'title' => 'Success', 'message' => 'Successfuly mark as paid 1st half the reservation']);*/
        alert()->success('Successfully cancelled the reservation with refund', 'Success')->persistent('Close');
        return redirect('/admin/reservation');
    }

    public function event_done() {
        ReservationDetail::where('reserv_id', $_POST['id'])
            ->update(['status' => 6,
            ]);
            /*return response()->json(['success' => true, 'title' => 'Success', 'message' => 'Successfuly mark as paid 1st half the reservation']);*/
        alert()->success('Successfully completed the event', 'Success')->persistent('Close');
        return redirect('/admin/reservation');
    }

    public function cancel_norefund() {
        ReservationDetail::where('reserv_id', $_POST['id'])
            ->update(['status' => 5]);
            /*return response()->json(['success' => true, 'title' => 'Success', 'message' => 'Successfuly mark as paid 1st half the reservation']);*/
        alert()->success('Successfully cancelled the reservation', 'Success')->persistent('Close');
        return redirect('/admin/reservation');
    }
    // end change status
// END RESERVATION

    public function searchReserve(){

        $searchItem = $_GET["searchReserve"];

        $details = ReservationDetail::select('ci.cust_fname', 'ci.cust_lname', 'reserv_guestNo', 'reserv_date', 'reserv_time','ci.contNo', 'place', 'status')
            ->join('customer_info as ci', 'ci.cust_id', '=', 'reservation_details.cust_id')
            ->join('event_details as ed', 'ed.event_id', '=', 'reservation_details.event_id')
            ->Where('cust_fname','LIKE', '%' . $searchItem .'%')
            ->orWhere('cust_lname','LIKE', '%' .$searchItem  .'%')
            ->orWhere('reserv_guestNo','LIKE', '%' .$searchItem  .'%')
            ->orWhere('place','LIKE', '%' .$searchItem  .'%')
            ->orWhere('reserv_date','LIKE', '%' .$searchItem  .'%')
            ->orWhere('reserv_time','LIKE', '%' .$searchItem  .'%')
            ->orWhere('status','LIKE', '%' .$searchItem  .'%')
            ->get();
       
        if(count($details) == 0){
            alert()->error('No result to display. Please try again.', 'Error')->persistent('Close');
        return redirect()->back();
        }

        return view('/admin/reservation/reservations')->with(['details' => $details]);
    }

    public function searchDown(){

        $searchItem = $_GET["searchDown"];

        $details = ReservationDetail::select('ci.cust_fname', 'ci.cust_lname', 'reserv_guestNo', 'reserv_date', 'reserv_time','ci.contNo', 'place', 'status')
            ->join('customer_info as ci', 'ci.cust_id', '=', 'reservation_details.cust_id')
            ->join('event_details as ed', 'ed.event_id', '=', 'reservation_details.event_id')
            ->Where('cust_fname','LIKE', '%' . $searchItem .'%')
            ->orWhere('cust_lname','LIKE', '%' .$searchItem  .'%')
            ->orWhere('reserv_guestNo','LIKE', '%' .$searchItem  .'%')
            ->orWhere('place','LIKE', '%' .$searchItem  .'%')
            ->orWhere('reserv_date','LIKE', '%' .$searchItem  .'%')
            ->orWhere('reserv_time','LIKE', '%' .$searchItem  .'%')
            ->orWhere('status','LIKE', '%' .$searchItem  .'%')
            ->get();
       
        if(count($details) == 0){
            alert()->error('No result to display. Please try again.', 'Error')->persistent('Close');
        return redirect()->back();
        }

        return view('/admin/reservation/downpay')->with(['details' => $details]);
    }













    // old controller
    // /**
    //  * Mark the reservation paid as half
    //  * 
    //  * @param ReservationIdRequest $reservationIdRequest
    //  * @return type
    //  */
    // public function markHalf(ReservationIdRequest $reservationIdRequest) {
    //     if (ReservationDetail::find($reservationIdRequest->reserv_id)->update(['status' => 5, 'receipt_no' => $reservationIdRequest->receipt_no , 'amount_paid' => $reservationIdRequest->amount_paid]))
    //         /*return response()->json(['success' => true, 'title' => 'Success', 'message' => 'Successfuly mark as paid 1st half the reservation']);*/
    //         alert()->success('Successfully marked the reservation as half paid', 'Success');
    //         return redirect()->action('Admin\ReservationController@index');
    //     return response()->json(['success' => false, 'title' => 'Error', 'message' => 'Something went wrong in marking half paid the reservation']);
    // }

    // /**
    //  * Mark the reservation paid as full paid
    //  * 
    //  * @param ReservationIdRequest $reservationIdRequest
    //  * @return type
    //  */
    // public function markSecondHalf(ReservationIdRequest $reservationIdRequest) {
    //     $reservation_detail = ReservationDetail::find($reservationIdRequest->reserv_id);
    //     $yiz = $reservationIdRequest->amount_paid;
    //     $no = $_POST["amount_paid"];
    //     $amount = $yiz + $no;
        
    //     if ($reservation_detail->update(['status' => 6, 'amount_paid' => $amount]))
    //         /*return response()->json(['success' => true, 'title' => 'Success', 'message' => 'Successfuly mark as paid 2nd half the reservation']);*/
    //     alert()->success('Successfully marked the reservation as fully paid', 'Success');
    //         return redirect()->action('Admin\ReservationController@index');
    //     return response()->json(['success' => false, 'title' => 'Error', 'message' => 'Something went wrong in marking 2nd half paid the reservation']);
    // }

    // /**
    //  * Mark the reservation as done
    //  * 
    //  * @param ReservationIdRequest $reservationIdRequest
    //  * @return type
    //  */
    // public function markDone(ReservationIdRequest $reservationIdRequest) {
    //     if (ReservationDetail::find($reservationIdRequest->reserv_id)->update(['status' => 9]))
    //         return response()->json(['success' => true, 'title' => 'Success', 'message' => 'Successfuly mark the reservation as done']);
    //     return response()->json(['success' => false, 'title' => 'Error', 'message' => 'Something went wrong in marking the reservation as done']);
    // }

    // /**
    //  * Mark the reservation cancelled with refund
    //  * 
    //  * @param ReservationIdRequest $reservationIdRequest
    //  * @return type
    //  */
    // public function cancelWithRefund(ReservationIdRequest $reservationIdRequest) {
    //     $reservation_detail = ReservationDetail::find($reservationIdRequest->reserv_id);

    //     if ($reservation_detail->update(['status' => 7])) {
    //         $this->UpdateEquipmentWorker($reservation_detail);
    //         return response()->json(['success' => true, 'title' => 'Success', 'message' => 'Successfuly cancelled the reservation with refund']);
    //     }

    //     return response()->json(['success' => false, 'title' => 'Error', 'message' => 'Something went wrong in cancelling the reservation with refund']);
    // }

    // /**
    //  * Mark the reservation cancelled with no refund
    //  * 
    //  * @param ReservationIdRequest $reservationIdRequest
    //  * @return type
    //  */
    // public function cancelNoRefund(ReservationIdRequest $reservationIdRequest) {
    //     $reservation_detail = ReservationDetail::find($reservationIdRequest->reserv_id);
        
    //     if ($reservation_detail->update(['status' => 8])) {
    //         $this->UpdateEquipmentWorker($reservation_detail);
    //         return response()->json(['success' => true, 'title' => 'Success', 'message' => 'Successfuly cancelled the reservation with no refund']);
    //     }

    //     return response()->json(['success' => false, 'title' => 'Error', 'message' => 'Something went wrong in cancelling the reservation with no refund']);
    // }

    // /**
    //  * Update the equipment/worker assigned in reservation
    //  * 
    //  * @param ReservationDetail $reservation_detail
    //  */
    // private function UpdateEquipmentWorker(ReservationDetail $reservation_detail) {
    //     /**
    //      * update the inventory
    //      */
    //         foreach ($reservation_detail->equipment as $equipment) {
    //             $equipment->quantity = $equipment->quantity + $equipment->pivot->quantity;
    //             $equipment->save();
    //     }
    //     /**
    //      * delete the assigned worker in the event
    //      */
    //     ReservationWorker::where('reserv_id', $reservation_detail->reserv_id)->delete();
    //     /**
    //      * Delete all the assigned equipment
    //      */
    //     ReservationEquipment::where('reserv_id', $reservation_detail->reserv_id)->delete();
    // }
    // end old controller

    
}
