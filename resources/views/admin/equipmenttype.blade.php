@extends('layouts.sidebar') 

@section('title', 'Equipment Type') 

@section('content')

<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <<i class="fa fa-home"></i> Home</a>
            <a href="/admin/equipment">Items</a>
            <a href="#" class="current">Item Type</a>
        </div>
        <h1>Item Types</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="float-left mb-2">
            <button class="btn btn-success btn-large icon-plus addEquipType"> Add Item Type</button>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table" id="equipmentTypeTbl">
                            <thead>
                                <tr>
                                    <th>Item Name</th>
                                    <th width="30%">Actions</th>
                                </tr>
                            </thead>
                            @foreach($type as $type)
                            <tbody>
                                <tr>
                                    <td>{{$type->type_description}}</td>
                                    <td class="btn-center">
                                        <button class="btn icon-pencil btn-primary edittype" data-id="{{ $type->type_id }}"> Edit</button>
                                        <button class="btn btn-danger icon-trash deletetype" data-id="{{ $type->type_id }}"> Delete</button>
                                    </td>
                                </tr>
                            </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection 

@section('modal')
<!-- add modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="addModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add Item Type</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/addEquipType">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label>Item Type Name</label>
                        <input type="text" class="form-control" placeholder="Item Type Name" name="name" required>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Add</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end add modal -->

<!-- edit modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="editModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edit Item Type</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/editEquipType">
                    {{csrf_field()}}
                    <input type="hidden" name="id" id="editID">
                    <div class="form-group">
                        <label>Item Type Name</label>
                        <input type="text" class="form-control inpname" name="etname">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end edit modal -->

<!-- delete modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="deleteModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Delete</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/deleteEquipType">
                    {{csrf_field()}}
                    <input type="hidden" class="id" name="id" id="deleteID">
                    <div class="form-group">
                        <h4>Delete item type?</h4>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Confirm</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end delete modal -->
@endsection 

@section('script')

<script type="text/javascript">
    $('.addEquipType').click(function () {
        $('#addModal').modal('show');
    });


    $('#editModal').on('show.bs.modal', function () {
        $(this).find('#btn-primary').on('click', function () {
            $('#editModal').find('form').submit();
        });
    })

    $('.edittype').click(function () {
        // $.ajax({
        //     type : "get",
        //     url : '/getFoodType' + $(this).data('id'),
        //     data : {id : $(this).data('id')},
        //     dataType: "json",
        //     success: function(response) {
        //         response.forEach(function(data){
        //             // $('#editModal .id').val($(this).data('id'));
        //             // $('#editModal .inpname').val(data.food_name);
        //             // $('#editModal .inprice').val(data.price);
        //             alert('fladjf');
        //         })
        //     }
        // });
        $('#editModal').modal('show');
        $('#editID').val($(this).data('id'));
    });


    $('.deletetype').click(function () {
        $('#deleteID').val($(this).data('id'));
        $('#deleteModal').modal('show');
    });
</script>

@endsection