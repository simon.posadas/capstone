<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FoodCategory extends Model
{
    /**
     * The table to fetch
     * @var type 
     */
    protected $table = 'categories';
    
     /**
     * Unique ID
     * @var type 
     */
    protected $primaryKey = 'id';
    
    /**
     *
     * @var type Remove automatic generation of timestamp
     */
    public $timestamps = false;

    public function rules()
    {
        return [
            'name' => 'required|min:2|max:20|unique:categories,name'
        ];
    }

    public function updateRules()
    {
        $name = $this->name;

        return [
            'name' => 'required|min:2|max:20|unique:categories,name,'.$name.',name'
        ];
    }

    public function deleteRules()
    {
        return [
            'name' => 'exists:categories,name'
        ];
    }

    public function foods()
    {
        return $this->belongsToMany('App\Food', 'category_food', 'category_id', 'food_id')
                ->withTimestamps();
    }
}