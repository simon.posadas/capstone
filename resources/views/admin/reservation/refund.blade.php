@extends('layouts.sidebar')

@section('title', 'Cancelled Reservation with Refund')

@section('content')

<!--main-container-part-->
<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="icon-home"></i> Home</a>
            <a href="#" class="current">Cancelled Reservation with Refund</a>
        </div>
        <h1>Cancelled Reservation with Refund</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <form method="get" action="/searchDown" role="search">
            <div class="pull-right">   
                <div class="input-group custom-search-form">
                    <input type="text" name="searchDown" class="form-control" placeholder="Search" id="search_bar">
                    <button class="btn btn-info" id="adjust-height-right" type="submit"><i class="icon-search"></i> Search</button>
                </div>
            </div>
        </form>
        <div class="row-fluid">
            <div class="colspan">
                <div class="widget-box">
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Customer Name</th>
                                    <th>Guest Number</th>
                                    <th>Contact Number</th>
                                    <th>Place</th>
                                    <th>Event Date</th>
                                    <th>Event Time</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                            </thead>
                            @foreach($details as $detail)
                            <tbody>
                                <tr>
                                    <td>{{$detail->cust_fname . " " . $detail->cust_lname}}</td>
                                    <td>{{$detail->reserv_guestNo}}</td>
                                    <td>{{$detail->contNo}}</td>
                                    <td>{{$detail->place}}</td>
                                    <td>{{$detail->reserv_date}}</td>
                                    <td>{{$detail->reserv_time}}</td>
                                    <td>{{ config()->get('constants.status')[$detail->status] }}</td>
                                    <td>
                                        @if($detail->status == 0 )
                                        <button id ="sizes1" class="btn btn-primary icon-ok paid_down" data-id="{{$detail->reserv_id}}"> Pay downpayment</button></br>
                                        <button id ="sizes1" class="btn btn-danger icon-remove first_cancel" data-id="{{$detail->reserv_id}}"> Cancel</button>
                                        @elseif($detail->status == 2)
                                        <button id ="sizes1" class="btn btn-primary icon-ok paid_full" data-id="{{$detail->reserv_id}}"> Pay in Full</button>
                                        <button id ="sizes1" class="btn btn-danger icon-remove refund_cancel" data-id="{{$detail->reserv_id}}"> Cancel with refund</button>
                                        @elseif($detail->status == 3)
                                        <button id ="sizes1" class="btn btn-success icon-ok event_done" data-id="{{$detail->reserv_id}}"> Event Done</button>
                                        <button id ="sizes1" class="btn btn-danger icon-remove cancel_norefund" data-id="{{$detail->reserv_id}}"> Cancel without refund</button>
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--main-container-part-->
<script src="{{ asset('js/jquery.min.js') }}"></script>

@endsection 