@extends('layouts.sidebar') 

@section('title', 'Food Category') 

@section('content')

<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="fa fa-home"></i> Home</a>
            <a href="/admin/food">Food</a>
            <a href="#" class="current">Food Category</a>
        </div>
        <h1>Food Categories</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="pull-left">
            <button class="btn btn-success btn-large fa fa-plus addFoodCat"> Add Food Category</button>
        </div>
        <div class="row-fluid">
            <div id="positioning" class="span8">
                <div class="widget-box">
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Category Name</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            @foreach($cats as $cats)
                            <tbody>
                                <tr>
                                    <td width=65%>{{$cats->category_name}}</td>
                                    <td class="btn-center">
                                        <button id ="sizes" class="btn fa fa-pencil btn-primary edittype" data-id="{{ $cats->category_id }}"> Edit</button>
                                        <button id ="sizes" class="btn btn-danger fa fa-trash deletetype" data-id="{{ $cats->category_id }}"> Delete</button>
                                    </td>
                                </tr>
                            </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection 

@section('modal')
<!-- add modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="addModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add Category Type</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/addFoodCat">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label class="fontsize">Food Type Name</label>
                        <input type="text" class="form-control" placeholder="Food Category Name" name="catname" required>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Add</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end add modal -->

<!-- edit modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="editModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edit Food Category</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/editFoodCat">
                    {{csrf_field()}}
                    <input type="hidden" name="id" id="editID">
                    <div class="form-group">
                        <label>Food Category Name</label>
                        <input type="text" class="form-control inpname" name="catname">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end edit modal -->

<!-- delete modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="deleteModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Delete</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/deleteFoodCat">
                    {{csrf_field()}}
                    <input type="hidden" class="id" name="id" id="deleteID">
                    <div class="form-group">
                        <h4>Delete food category?</h4>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Confirm</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end delete modal -->
@endsection 

@section('script')

<script type="text/javascript">
    $('.addFoodCat').click(function () {
        $('#addModal').modal('show');
    });


    $('#editModal').on('show.bs.modal', function () {
        $(this).find('#btn-primary').on('click', function () {
            $('#editModal').find('form').submit();
        });
    })

    $('.edittype').click(function () {
        // $.ajax({
        //     type : "get",
        //     url : '/getFoodType' + $(this).data('id'),
        //     data : {id : $(this).data('id')},
        //     dataType: "json",
        //     success: function(response) {
        //         response.forEach(function(data){
        //             // $('#editModal .id').val($(this).data('id'));
        //             // $('#editModal .inpname').val(data.food_name);
        //             // $('#editModal .inprice').val(data.price);
        //             alert('fladjf');
        //         })
        //     }
        // });
        $('#editModal').modal('show');
        $('#editID').val($(this).data('id'));
    });


    $('.deletetype').click(function () {
        $('#deleteID').val($(this).data('id'));
        $('#deleteModal').modal('show');
    });
</script>

@endsection