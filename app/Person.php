<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    use \Illuminate\Notifications\Notifiable;
    /**
     * The table to fetch
     * @var type 
     */
    protected $table = 'customers';
    
     /**
     * Unique ID
     * @var type 
     */
    protected $primaryKey = 'id';
    
    /**
     *
     * @var type Remove automatic generation of timestamp
     */
    public $timestamps = true;
    
    /**
     * Mass variable assignement
     */
    protected $fillable = [
        'firstname', 
        'lastname', 
        'middlename', 
        'contact', 
        'email', 
        'address'
    ];
    
    /**
     * Make a relation with reservation detail
     * @return type
     */
    public function reservations(){
        return $this->hasMany('App\Reservation', 'customer_id', 'id');
    }

    public function rules()
    {
        return [
            'firstname' => 'required|min:2|max:50',
            'middlename' => 'nullable|min:2|max:50',
            'lastname' => 'required|min:2|max:50',
            'email' => 'email',
            'address' => '',
            'contact' => ''
        ];
    }
}

