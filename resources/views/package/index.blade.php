@extends('layouts.template') 

@section('content')

@include('modal.package.add')
@include('modal.package.edit')
@include('modal.package.delete')

<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="icon icon-home"></i> Home</a>
            <a href="#" class="current">Package</a>
        </div>
        <h1>Packages</h1>
    </div>
    <div class="container-fluid">
        <hr>

        @if(session()->has('success-message'))
        <div class="alert alert-success">
            <ul>
                {{ session()->get('success-message') }}
            </ul>
        </div>
        @endif


        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="pull-left">
            <button class="btn btn-success btn-large icon icon-plus" data-toggle="modal" data-target="#addModal" type="button"> Add Package</button>
        </div>
        <form method="get" action="/searchPackage" role="search">
            <div class="pull-right">   
                <div class="input-group custom-search-form">
                    <input type="text" name="searchPackage" class="form-control" placeholder="Search" id="search_bar">
                    <button class="btn btn-info" id="adjust-height-right" type="submit"><i class="icon icon-search"></i> Search</button>
                </div>
            </div>
        </form>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-content nopadding">

                        <table class="table table-bordered data-table" id="packagesTable">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Price</th>
                                    <th width="20%">Actions</th>
                                </tr>
                            </thead>
                            @foreach($packages as $package)
                            <tbody>
                                <tr>
                                    <td>{{ $package->id }}</td>
                                    <td>{{ $package->name }}</td>
                                    <td>{{ $package->type }}</td>
                                    <td style="text-align:right">&#8369; {{ number_format($package->price, 2) }}</td>
                                    <td>
                                        <div class="flex-row1">
                                            <a href="{{ url("food/package/$package->id/list") }}" class="btn icon icon-eye-list btn-info btn-margins3"> Show</a>
                                            <a href="{{ url("food/package/$package->id/limit") }}" class="btn icon icon-eye-list btn-info btn-margins4"> Limitations</a>
                                        </div>
                                        <div class="flex-row2">
                                            <button id ="sizes" class="btn fa fa-pencil btn-primary edittype btn-margins" data-id="{{ $package->id }}" data-name="{{ $package->name }}" data-type="{{ $package->type }}" data-toggle="modal" data-price="{{ $package->price }}" data-target="#editModal"> Edit</button>
                                            <button id ="sizes" class="btn fa fa-trash btn-danger deletetype btn-margins2" data-id="{{ $package->id }}" data-name="{{ $package->name }}" data-toggle="modal" data-target="#deleteModal"> Delete</button>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection 

@section('scripts_include')

<script type="text/javascript">

    $(document).ready(function(){

        $('#packagesTable').on('click', '.edittype', function (event) {
            name = $(this).data('name')
            id = $(this).data('id')
            type = $(this).data('type')
            price = $(this).data('price')

            $('#editModal').find('input[name=name]').val(name);
            $('#editModal').find('.type').val(type).change();
            $('#editModal').find('input[name=price]').val(price);

            $('#editModal').find('#btn-edit').on('click', function () {

                $('#editForm').attr('action', function(i, value) {
                    return "{{ url('food/package') }}/" + id ;
                });

                $('#editForm').submit();
            });

        })

        $('#packagesTable').on('click', '.deletetype', function (event) {
            id = $(this).data('id')

            $('#deleteModal').find('#btn-delete').on('click', function () {

                $('#deleteForm').attr('action', function(i, value) {
                    return "{{ url('food/package') }}/" + id ;
                });

                $('#deleteForm').submit();
            });

        })
        
        $('#packagesTable').dataTable({
            searching   :   false
        });

    })

</script>

@endsection