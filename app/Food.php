<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon;

class Food extends Model
{
    /**
     * The table to fetch
     * @var type 
     */
    protected $table = 'foods';
    
     /**
     * Unique ID
     * @var type 
     */
    protected $primaryKey = 'id';

    /**
     * Variable mass assignement
     * @var type 
     */
    protected $fillable = ['name', 'price', 'status', 'type_id', ];

    protected $appends = [
        'type_name'
    ];

    // public function getTypeNameAttribute()
    // {
    //     return $this->type->name;
    // }

    public function type()
    {
        return $this->belongsTo('App\FoodType', 'type_id', 'id');
    }

    public function packages()
    {
        return $this->belongsToMany('App\Package', 'package_food', 'food_id', 'package_id')
                ->withTimestamps();
    }

    public function categories()
    {
        return $this->belongsToMany('App\FoodCategory', 'category_food', 'category_id', 'food_id');
    }
    
    /**
     *
     * @var type Remove automatic generation of timestamp
     */
    public $timestamps = false;

    public function rules()
    {
        return [
            'name' => 'required|min:2|max:50|unique:foods,name',
            'category' => 'required|exists:categories,id',
            'type' => 'exists:food_types,id',
            'price' => ''
        ];
    }

    public function updateRules()
    {
        $name = $this->name;
        return [
            'name' => 'required|min:2|max:50|unique:foods,name,' . $name . ',name' ,
            'type' => 'required|exists:food_types,id',
            'price' => ''
        ];
    }

    public function deleteRules()
    {
        return [
            'Food' => 'required|exists:foods,id'
        ];
    }

    public function assignFoodRules()
    {
        return [
            'name' => 'required|exists:foods,id'
        ];
    }

    public function generateCode()
    {
        $count = 1;
        $date = Carbon\Carbon::now();
        $constant = 'FOOD-' . $date->format('y') . '-' . $date->format('m') . '-';
        $id = Food::orderBy('id', 'desc')->pluck('id')->first() + 1;

        if(!isset($id) || $id == null || $id == '') $id = 0;

        return $constant . $id;
    }
}