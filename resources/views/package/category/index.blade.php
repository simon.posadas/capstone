@extends('layouts.template') 

@section('content')

@include('modal.package.category.add')
@include('modal.package.category.edit')
@include('modal.package.category.delete')

<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="icon icon-home"></i> Home</a>
            <a href="{{ url('food/package') }}">Package</a>
            <a href="#" class="current">Limitations</a>
        </div>
        <h1>{{ $package->name }}</h1>
    </div>
    <div class="container-fluid">
        <hr>

        @if(session()->has('success-message'))
        <div class="alert alert-success">
            <ul>
                {{ session()->get('success-message') }}
            </ul>
        </div>
        @endif


        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="pull-left">
            <button class="btn btn-success btn-large fa fa-plus" data-toggle="modal" data-target="#addModal" type="button"> Add Category</button>
        </div>
        <form method="get" action="/searchPackage" role="search">
            <div class="pull-right">   
                <div class="input-group custom-search-form">
                    <input type="text" name="searchPackage" class="form-control" placeholder="Search" id="search_bar">
                    <button class="btn btn-info" id="adjust-height-right" type="submit"><i class="fa fa-search"></i> Search</button>
                </div>
            </div>
        </form>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-content nopadding">

                        <table class="table table-bordered data-table" id="categoriesTable">
                            <thead>
                                <tr rowspan="2">
                                    <th colspan=3>Name:  {{ $package->name }}</th>
                                    <th colspan=3>Type: {{ $package->type }}</th>
                                </tr>
                                <tr rowspan="2">
                                    <th colspan=6>Price:  {{ $package->price }}</th>
                                </tr>
                                <tr>
                                    <th>Category</th>
                                    <th>Type</th>
                                    <th>Limit</th>
                                    <th width="20%">Actions</th>
                                </tr>
                            </thead>
                            @foreach($package_categories as $category)
                            <tbody>
                                <tr>
                                    <td>{{ $category->category_name }}</td>
                                    <td>{{ $category->type_name }}</td>
                                    <td class="currency">{{ $category->amount }}</td>
                                    <td class="btn-center">
                                        <button id ="sizes" class="btn fa fa-pencil btn-primary edittype" data-id="{{ $category->package_id }}" data-limit="{{ $category->limitation }}" data-type="{{ $category->type_id }}" data-toggle="modal" data-category="{{ $category->category_id }}" data-target="#editModal"> Edit</button>
                                        <button id ="sizes" class="btn fa fa-trash btn-danger deletetype" data-id="{{ $category->package_id }}" data-type="{{ $category->type_id }}" data-category="{{ $category->category_id }}" data-toggle="modal" data-target="#deleteModal"> Delete</button>
                                    </td>
                                </tr>
                            </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection 

@section('scripts_include')

<script type="text/javascript">

    $(document).ready(function(){
        document.getElementById("type").disabled=true;

        $('#categoriesTable').on('click', '.edittype', function (event) {
            type = $(this).data('type')
            category = $(this).data('category')
            limit = $(this).data('limit')
            id = $(this).data('category')

            $('#editModal').find('input[name=limit]').val(limit);
            $('#editModal').find('.type').val(type).change();
            $('#editModal').find('.category').val(category).change();

            $('#editModal').find('#btn-edit').on('click', function () {

                $('#editForm').attr('action', function(i, value) {
                    return "{{ url("food/package/$package->id") }}/" + 'limit/' + id ;
                });

                $('#editForm').submit();
            });

        })

        $('#categoriesTable').on('click', '.deletetype', function (event) {
            id = $(this).data('category')
            type = $(this).data('type')
            $('#deleteModal').find('.type').val(type).change();

            $('#deleteModal').find('#btn-delete').on('click', function () {

                $('#deleteForm').attr('action', function(i, value) {
                    return "{{ url("food/package/$package->id") }}/" + 'limit/' + id ;
                });

                $('#deleteForm').submit();
            });

        })

        $('#categoriesTable').dataTable({
            searching   :   false
        });

    })

    function enableCat(){
        var t = document.getElementById("category");
        var food_cat = t.options[t.selectedIndex].value;

        if(food_cat == 4){
            document.getElementById("type").disabled=false;
        } else {
            document.getElementById("type").disabled=true;
        }
    }

</script>

@endsection