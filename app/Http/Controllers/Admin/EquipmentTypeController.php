<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Model\Equipment;
use App\Model\EquipmentType;
use App\Http\Requests\Reservation\ReservationIdRequest;
use Notification;
use App\Notifications\ReservationApprove;
use App\Notifications\ReservationDisapprove;
use App\Http\Controllers\Controller;

/**
 * 
 */
class EquipmentTypeController extends Controller {
    /**
     * View directory
     * @var type 
     */
    protected $view = 'admin.';

    // EQUIPMENT TYPE
    public function equipType(){
        $type = EquipmentType::get();
        return view($this->view . 'equipmenttype', ['type' => $type ]);
    }

    public function getEquipType(Request $req){
        // $type = FoodDetail::select('*')
        //     ->where('food_id',$req->id)
        //     ->get();
        // dd($type);
        return response()->json($types);
    }

    public function addEquipType(){
        EquipmentType::insert([ 
            'type_description' => $_POST['name']
            ]);
        alert()->success('Successfully added an equipment type', 'Success')->persistent('Close');

        return redirect('/admin/equipmenttype');
    }

    public function editEquipType(){
        EquipmentType::where('type_id', $_POST['id'])
            ->update(['type_description' => $_POST['etname']
        ]);
            
        alert()->success('Successfully edited an equipment type', 'Success')->persistent('Close');

        return redirect('/admin/equipmenttype');
    }
    
    public function deleteEquipType(){
        if(EquipmentType::where('type_id', $_POST['id'])->delete()){
            EquipmentType::where('type_id', $_POST['id'])->delete();
            alert()->success('Successfully deleted an equipment type', 'Success')->persistent('Close');
        }else{
        alert()->error('Something went wrong deleting the equipment type', 'Error')->persistent('Close');
        }
        return redirect('/admin/equipmenttype');
    }
    // END EQUIPMENT TYPE
}