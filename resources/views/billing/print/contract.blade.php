@extends('layouts.printables.report')

@section('content')

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class = "container">
    <div id="header">
        <div class="col-sm-12">
        </div>
        <div class="col-sm-12" style="color: #800000;">
            <div class="clearfix"></div>
            <div>
                <img src="{{ asset('images/logo.png') }}" class="img img-responsive img-circle pull-left" style="height: 80px;width:auto;" />
            </div>
            <div style="margin-left: 5em;">
              <div style="font-size:18pt; margin-left: 20%;">IRMALYN'S CAKE AND CATERING  </div>
              <div style="font-size:13pt; margin-left: 23%; margin-top: 2%;">#22 Lempira St. Meralco Village Taytay Rizal <span class="pull-right"> {{ Carbon\Carbon::now()->toDayDateTimeString() }} </span></div>
              <div style="font-size:13pt; margin-left: 22%;">Tel no.: (02) 661-0076 / Cel no.: 0922 853 7164 </div>
            </div>
        </div>
        <div class="col-sm-12">
          <hr />
        </div>
    </div>
  </body>
</html>

  <style>
      th , tbody{
        text-align: center;
      }

      #content{
        font-family: "Times New Roman";
      }

      @media print {
          tr.page-break  { display: block; page-break-after: always; }
      }   

  </style>
  <div id="content" class="col-sm-12">
    <h4 class="text-center"> <small class="pull-right"></small></h4>
      <thead>
        <center><h3>CONTRACT FORM</h3></center>

       
    
    <div><h5>Client:  {{$customer->firstname . " " . $customer->lastname}}</h5></div>
    <div><h5>Address:  {{$customer->address}}</h5></div>
    <div><h5>Package:  {{$packages->name}}</h5></div>
    <div><h5>Reservation Code:  {{$reservation->code}}</h5></div><br>
       
    <h5>Preferred Viands:</h5>
    <div style="margin-left: 10%; margin-bottom: 10%;">{{implode( ", " , $food->pluck('name')->toArray())}}<br></div>

    
    <div style="margin-left: 10%; margin-bottom: 4%;"><h5>Amenities:</h5></div>
    <div style="margin-left: 20%;"><p>Elegantly arranged buffet table with attractive designed centerpiece and lights.</p></div>
    <div style="margin-left: 20%;"><p>Guests tables and chairs w/ table cloths, runners according to motif, seat cover and bough accent.</p></div>
    <div style="margin-left: 20%;"><p>Flower centerpiece for guests table.</p></div>
    <div style="margin-left: 20%;"><p>Use of catering utensils, equipments and other needed materials.</p></div>
    <div style="margin-left: 20%;"><p>Services of well trained, courteous and uniformed waiters and attendants.</p></div>
    <div style="margin-left: 20%; margin-bottom: 4%;"><p>Purified water and ice for drinks.</p></div>

    
    <div style="margin-left: 10%; "><h5>Terms:</h5></div>
    <div style="margin-left: 10%; margin-bottom: 8%;"><p>50% down payment upon contract signing, balanced to be paid in full 1 week before the date of the event.</p></div>

    
    <div style="margin-left: 10%; "><p>(For further details pls. call the nos. stated above and look for Irma.)</p></div>


    <div style="margin-left: 10%; margin-bottom: 8%;"><h5>Conforme:</h5></div>
    <div style="margin-left: 20%;"><HR></div>

      </thead>
      <tbody>

      </tbody>
    </table>
  </div>

  
@endsection