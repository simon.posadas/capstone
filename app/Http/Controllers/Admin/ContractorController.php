<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Model\Contractor;
use App\Model\WorkerRole;
use App\Http\Requests\Reservation\ReservationIdRequest;
use Notification;
use App\Notifications\ReservationApprove;
use App\Notifications\ReservationDisapprove;
use App\Http\Controllers\Controller;

/**
 * 
 */
class ContractorController extends Controller {
    /**
     * View directory
     * @var type 
     */
    protected $view = 'admin.';

    // EMPLOYEE

    public  function contractor()
    {
        $contractor = Contractor::where('status', 0)
            ->get();

        return view($this->view . 'contractor', ['contractors' => $contractor]);
    }
    
    public function getContractor(Request $req){
        $types = Contractor::select('*')
            ->where('contractor_id',$req->id)
            ->get();
        return response()->json($types);
    }

    public function addContractor(){
        $contractor_fname = Contractor::select('contractor_fname')->get();
        $contractor_mname = Contractor::select('contractor_mname')->get();
        $contractor_lname = Contractor::select('contractor_lname')->get();
        $contractor_name = array($_POST['fname'], $_POST['mname'], $_POST['lname']);

        if($contractor_fname == $contractor_name[0] || $contractor_mname == $contractor_name[1] || $contractor_lname == $contractor_name[2]){
            alert()->error('Contractor already exists', 'Error')->persistent('Close');
        } else {
        Contractor::insert([ 
            'contractor_lname' => $contractor_name[2],
            'contractor_fname' => $contractor_name[0],
            'contractor_mname' => $contractor_name[1],
            'contractor_age' => $_POST['age'],
            'contactNo' => $_POST['contno']
            ]);
        alert()->success('Successfully added a contractor', 'Success')->persistent('Close');
        }

        return redirect('/admin/contractor');
    }
    
    public function editContractor(){
        $contractor_fname = Contractor::select('contractor_fname')->get();
        $contractor_mname = Contractor::select('contractor_mname')->get();
        $contractor_lname = Contractor::select('contractor_lname')->get();
        $contractor_name = array($_POST['fname'], $_POST['mname'], $_POST['lname']);

        if($contractor_fname == $contractor_name[0] || $contractor_mname == $contractor_name[1] || $contractor_lname == $contractor_name[2]){
            alert()->error('Employee already exists', 'Error')->persistent('Close');
        } else {
            Contractor::where('contractor_id', $_POST['id'])
                ->update(['contractor_fname' => $contractor_name[0],
                'contractor_mname' => $contractor_name[1],
                'contractor_lname' => $contractor_name[2],
                'contractor_age' => $_POST['age'],
                'contactNo' => $_POST['contno']
            ]);

            alert()->success('Successfully edited a worker', 'Success')->persistent('Close');
        }
        return redirect('/admin/contractor');
    }

    public function searchContractor(){

        $searchItem = $_GET["searchContractor"];
        // $contractor = Contractor::get();


        $details = Contractor::select('contractor_fname', 'contractor_lname', 'contactNo')
            ->Where('contractor_fname','LIKE', '%' . $searchItem .'%')
            ->orWhere('contractor_lname','LIKE', '%' .$searchItem  .'%')
            ->orWhere('contactNo','LIKE', '%' .$searchItem  .'%')
            ->get();
    
        if(count($details) == 0){
            alert()->error('No result to display. Please try again.', 'Error')->persistent('Close');
        return redirect()->back();
        }

        return view('/admin/contractor')->with(['contractors' => $details]);

    }

    public function deleteContractor(){
        if(Contractor::where('contractor_id', $_POST['id'])->delete()){
            Contractor::where('contractor_id', $_POST['id'])->delete();
            alert()->success('Successfully deleted a contractor', 'Success')->persistent('Close');
        } else {
            alert()->error('Something went wrong deleting the contractor', 'Error')->persistent('Close');
        }
           return redirect('/admin/contractor');
    }
    // END EMPLOYEE
}