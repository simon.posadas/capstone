<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use DB;
use Validator;
use Carbon;

class DashboardController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.index');
    }

    public function getData()
    {   
        $date = array();
        $data = array();
        $now = Carbon\Carbon::now();
        $year = Carbon\Carbon::parse($now)->format('Y');
        $month = 'month';

        for($i = 1; $i <= 12; $i++)
        {
        $date[$i] = DB::table('reservations')
            ->select('created_at AS month')
            ->whereYear('created_at', $year)
            ->whereMonth('created_at', $i)
            ->count();
        }

        $result = $date;

        foreach($result as $row)
        {
            $data[] = $row;
        }

        return $data;

        // $month = Carbon\Carbon::parse($date->created_at)->format('m');

        // $query = DB::table('reservations')
        //     ->selectRaw(count('created_at'))
        //     ->whereYear('created_at', $year)
        //     ->get();
        // $data = $date;
    }
}