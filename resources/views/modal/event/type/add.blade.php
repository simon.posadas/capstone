
<!-- add modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="addModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add Event Type</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="{{ url('event/type') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="form-group form-padds">
                        <label class="add-typeLabel1">Name :</label>
                        <input id="input-addNames" type="text" class="form-control" placeholder="Event Type Name" name="name" required>
                    </div>
                    <div class="form-group form-padds">
                        <label class="add-typeLabel1">Range :</label>
                        <input id="input-addRange" type="number" class="form-control" placeholder="Range" name="range" required>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Create</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end add modal -->