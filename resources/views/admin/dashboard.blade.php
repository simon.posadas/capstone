@extends('layouts.sidebar') 
@include('/layouts/css')

@section('title', 'Dashboard') 

@section('content')
<!--main-container-part-->
<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="fa fa-home"></i> Home</a>
        </div>
        <h1>Dashboard</h1>
    </div>
    <!--End-breadcrumbs-->
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <i class="fa fa-th fa-sm pt-1"></i>
                        <h5>Reservations</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Reserve ID</th>
                                    <th>Customer Name</th>
                                    <th>Guest Number</th>
                                    <th>Budget</th>
                                    <th>Place</th>
                                    <th>Event Date</th>
                                    <th>Event Time</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            @foreach($type as $type)
                            <tbody>
                                <tr>
                                    <td>{{$type->reserv_id}}</td>
                                    <td>{{$type->cust_fname . " " . $type->cust_lname}}</td>
                                    <td>{{$type->reserv_guestNo}}</td>
                                    <td>{{$type->cust_budget}}</td>
                                    <td>{{$type->place}}</td>
                                    <td>{{$type->reserv_date}}</td>
                                    <td>{{$type->reserv_time}}</td>
                                   <td class="btn-center">
                                        <button id ="sizes" class="btn btn-primary fa fa-pencil">&nbsp;Edit</button>
                                        <button id ="sizes" class="btn btn-danger fa fa-trash">&nbsp;Delete</button>
                                    </td>
                                </tr>
                            </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end-main-container-part-->

@endsection 

@section('script') 
@endsection