@extends('layouts.template') 

@section('content')

@include('modal.item.add')

<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="icon icon-home"></i> Home</a>
            <a href="#" class="current">Item</a>
        </div>
        <h1>Item</h1>
    </div>
    <div class="container-fluid">
        <hr>

        @if(session()->has('success-message'))
        <div class="alert alert-success">
            <ul>
                {{ session()->get('success-message') }}
            </ul>
        </div>
        @endif


        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="pull-left">
            <button class="btn btn-success btn-large icon icon-plus" data-toggle="modal" data-target="#addModal" type="button"> Add Item</button>
        </div>
        <form method="get" action="/searchItem" role="search">
            <div class="pull-right">   
                <div class="input-group custom-search-form">
                    <input type="text" name="searchFood" class="form-control" placeholder="Search" id="search_bar">
                    <button class="btn btn-info" id="adjust-height-right4" type="submit"><i class="icon icon-search"></i> Search</button>
                </div>
            </div>
        </form>
        <div class="row-fluid">
            <div id="table-margin" class="span12">
                <div class="widget-box">
                    <div class="widget-content nopadding">

                        <table class="table table-bordered data-table" id="foodsTable">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Quantity</th>
                                    <th>Price</th>
                                    <th width="20%">Actions</th>
                                </tr>
                            </thead>
                            @foreach($items as $item)
                            <tbody>
                                <tr>
                                    <td>{{ $item->name }}</td>
                                    <td style="text-align: right">{{ $item->quantity }}</td>
                                    <td style="text-align:right">&#8369; {{ number_format($item->price, 2) }}</td>
                                    <td class="btn-center">
                                        <button id ="sizes" class="btn icon icon-pencil btn-primary edittype" data-id="{{ $item->id }}"
                                            data-name="{{ $item->name }}"
                                            data-quantity="{{ $item->quantity }}"
                                            data-price="{{ $item->price }}"> Edit</button>
                                        <button id ="sizes" class="btn icon icon-trash btn-danger deletetype" data-id="{{ $item->id }}" data-name="{{ $item->name }}"> Delete</button>
                                    </td>
                                </tr>
                            </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('modal')
<!-- delete modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="deleteModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Delete</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/item/delete" id="deleteForm">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <!-- <input type="hidden" name="_method" value="DELETE" /> -->
                    <input type="hidden" class="id" name="id" id="deleteID">
                    <div class="form-group">
                        <h4>Delete item?</h4>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="btn-delete">Confirm</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end delete modal -->

<!-- edit modal -->
<div class="modal fade modal-size-s1" tabindex="-1" role="dialog" id="editModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edit Item</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/item/edit" id="editForm">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <!-- <input type="hidden" name="_method" value="PUT" /> -->

                    <input type="hidden" name="id" id="editID">
                    <div class="form-group">
                        <label class="input-margin2">Name :</label>
                        <input id="input-addName2" type="text" class="form-control inpname" name="name">
                    </div>
                    <div class="form-group div-margin4">
                        <label class="label-margin1">Quantity :</label>
                        <input id = "input-addQuantity2" type="number" class="form-control inquan" name="quantity">
                    </div>
                    <div class="form-group div-margin4">
                        <label class="label-margin1">Price per Piece :</label>
                        <input id = "input-addQuantity2" type="number" class="form-control inprice" name="price">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="btn-edit">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end edit modal -->
@endsection

@section('script')
<script type="text/javascript">
    $('.deletetype').click(function () {
            $('#deleteID').val($(this).data('id'));
            $('#deleteModal').modal('show');
        });

    $('.edittype').click(function () {
        name = $(this).data('name')
        price = $(this).data('price')
        id = $(this).data('id')
        quantity = $(this).data('quantity')

        $('#editID').val(id);
        $('#editModal').find('input[name=quantity]').val(quantity);
        $('#editModal').find('input[name=name]').val(name);
        $('#editModal').find('input[name=price]').val(price);

        $('#editModal').modal('show');
    });
</script>
@endsection
