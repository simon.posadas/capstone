<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageLimitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_limits', function (Blueprint $table) {
            $table->integer('package_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->integer('type_id')->nullable()->unsigned();
            $table->integer('amount')->nullable();
            $table->timestamps();

            
            $table->foreign('package_id')
            ->references('id')
            ->on('packages')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->foreign('category_id')
            ->references('id')
            ->on('categories')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->foreign('type_id')
            ->references('id')
            ->on('food_types')
            ->onUpdate('cascade')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_limits');
    }
}
