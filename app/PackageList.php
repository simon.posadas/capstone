<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageList extends Model
{
    protected $table = 'package_lists_v';
}