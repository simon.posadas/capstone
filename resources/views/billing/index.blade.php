@extends('layouts.template')

@section('content')

<!--main-container-part-->
<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="icon icon-home"></i> Home</a>
                <a href="#" class="current">Billing</a>
        </div>
        <h1>Billing</h1>
    </div>
    <!--End-breadcrumbs-->
    <div class="container-fluid">
        <hr> 
        @if(session()->has('success-message'))
        <div class="alert alert-success">
            <ul>
                {{ session()->get('success-message') }}
            </ul>
        </div>
        @endif
        
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="pull-left">
            <a class="btn btn-primary btn-large fa fa-plus" href="{{ url('billing/create') }}" type="button"> Create Bill</a>
        </div>

        <div class="container-tab-pill">
            <ul class="nav nav-pills nav-justified">
                <li class="active">
                    <a class="address-color" href="#tab_a" data-toggle="pill">Reservation Billing</a>
                </li>
                <li>
                    <a class="address-color" href="#tab_b" data-toggle="pill">Item Billing</a>
                </li>
                <li>
                    <a class="address-color" href="#tab_c" data-toggle="pill">Other Billing</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_a">
                    <div class="widget-box">
                        <div class="widget-title">
                            <h5>Reservation Billing</h5>
                        </div>
                        <div class="row-fluid">
                            <div class="span12">

                                <div class="widget-content nopadding">
                                    <table class="table table-bordered data-table">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Code</th>
                                                <th>Description</th>
                                                <th>Amount</th>
                                                <th>Reference</th>
                                                <th>Bill Date</th>
                                                <th>Status</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        @foreach($billings->where('billing_type', '=', 0) as $billing)
                                        <tbody>
                                            <tr>
                                                <td>{{ $billing->id }}</td>
                                                <td>{{ $billing->code }}</td>
                                                <td>{{ $billing->description }}</td>
                                                <td style="text-align: right">&#8369; {{ number_format($billing->amount) }}</td>
                                                <td>{{ $billing->reservation->code }}</td>
                                                <td>{{ Carbon\Carbon::parse($billing->created_at)->toDayDateTimeString () }}</td>
                                                <td>{{ $billing->status_name }}</td>
                                                <td class="btn-center">
                                                    
                                                    <a class="btn btn-primary" href="{{ route('Receipt', ['id' => $billing-> id , 'id2' => $billing -> reservation_id ]) }}">Receipt</a>
                                                    
                                                </td>
                                            </tr>
                                        </tbody>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab_b">
                    <div class="widget-box">
                        <div class="widget-title">
                            <h5>Item Billing</h5>
                        </div>
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="widget-content nopadding">
                                    <table class="table table-bordered data-table">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Code</th>
                                                <th>Description</th>
                                                <th>Amount</th>
                                                <th>Reference</th>
                                                <th>Bill Date</th>
                                                <th>Status</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        @foreach($billings->where('billing_type', '=', 1) as $billing)
                                        <tbody>
                                            <tr>
                                                <td>{{ $billing->id }}</td>
                                                <td>{{ $billing->code }}</td>
                                                <td>{{ $billing->description }}</td>
                                                <td style="text-align: right">&#8369; {{ number_format($billing->amount) }}</td>
                                                <td>{{ $billing->reservation->code }}</td>
                                                <td>{{ Carbon\Carbon::parse($billing->created_at)->toDayDateTimeString () }}</td>
                                                <td>{{ $billing->status_name }}</td>
                                                <td class="btn-center">
                                                    @if($billing->status == 0)
                                                    <form method="post" action="{{ url(" billing/$billing->id") }}">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                        <input type="hidden" name="_method" value="put" />
                                                        <button type="submit" name="pay" value="{{ csrf_token() }}" id="sizes" class="btn btn-success fa fa-trash">&nbsp;Pay</button>
                                                    </form>
                                                    @elseif($billing->status == 1)
                                                    <a class="btn btn-primary" href="{{ route('OtherReceipt', ['id' => $billing-> id , 'id2' => $billing -> reservation_id ]) }}">Receipt</a>
                                                    @endif
                                                </td>
                                            </tr>
                                        </tbody>
                                        @endforeach
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="tab_c">
                    <div class="widget-box">
                        <div class="widget-title">
                            <h5>Other Bills</h5>
                        </div>
                        <div class="row-fluid">
                            <div class="span12">

                                <div class="widget-content nopadding">
                                    <table class="table table-bordered data-table">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Code</th>
                                                <th>Description</th>
                                                <th>Amount</th>
                                                <th>Reference</th>
                                                <th>Bill Date</th>
                                                <th>Status</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        @foreach($billings->where('billing_type', '=', 2) as $billing)
                                        <tbody>
                                            <tr>
                                                <td>{{ $billing->id }}</td>
                                                <td>{{ $billing->code }}</td>
                                                <td>{{ $billing->description }}</td>
                                                <td style="text-align: right">&#8369; {{ number_format($billing->amount) }}</td>
                                                <td>{{ $billing->reservation->code }}</td>
                                                <td>{{ Carbon\Carbon::parse($billing->created_at)->toDayDateTimeString () }}</td>
                                                <td>{{ $billing->status_name }}</td>
                                                <td class="btn-center">
                                                    @if($billing->status == 0)
                                                    <form method="post" action="{{ url(" billing/$billing->id") }}">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                        <input type="hidden" name="_method" value="put" />
                                                        <button type="submit" name="pay" value="{{ csrf_token() }}" id="sizes" class="btn btn-success fa fa-trash">&nbsp;Pay</button>
                                                    </form>
                                                    @elseif($billing->status == 1)
                                                    <a class="btn btn-primary" href="{{ route('OtherReceipt', ['id' => $billing-> id , 'id2' => $billing -> reservation_id ]) }}">Receipt</a>
                                                    @endif
                                                </td>
                                            </tr>
                                        </tbody>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- tab content -->
        </div>
        <!-- end of container -->

        <!-- end of container -->


        <!-- <div class="row-fluid">
                <div class="span12">
                    <div class="widget-box">
                        <div class="widget-title">
                            <i class="fa fa-th fa-sm pt-1"></i>
                            <h5>Bills</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <table class="table table-bordered data-table">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Code</th>
                                        <th>Purpose</th>
                                        <th>Description</th>
                                        <th>Amount</th>
                                        <th>Reference</th>
                                        <th>Bill Date</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                @foreach($billings as $billing)
                                <tbody>
                                    <tr>
                                        <td>{{ $billing->id }}</td>
                                        <td>{{ $billing->code }}</td>
                                        <td>{{ $billing->title }}</td>
                                        <td>{{ $billing->description }}</td>
                                        <td>{{ number_format($billing->amount) }}</td>
                                        <td>{{ $billing->reservation->code }}</td>
                                        <td>{{ Carbon\Carbon::parse($billing->created_at)->toDayDateTimeString  () }}</td>
                                        <td>{{ $billing->status_name }}</td>
                                       <td class="btn-center">
                                            @if($billing->status == 0)
                                            <form method="post" action="{{ url("billing/$billing->id") }}">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                <input type="hidden" name="_method" value="put" />
                                                <button type="submit" name="pay" value="{{ csrf_token() }}" id ="sizes" class="btn btn-success fa fa-trash">&nbsp;Pay</button>
                                            </form>

                                                @elseif($billing->status == 1)


                                                <a class="btn btn-primary" href="{{ route('OtherReceipt', ['id' => $billing-> id , 'id2' => $billing -> reservation_id ]) }}">Receipt</a>

                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div> -->
    </div>
</div>
    <!--end-main-container-part-->

@endsection