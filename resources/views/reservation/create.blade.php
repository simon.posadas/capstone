@extends('layouts.template')

@section('content')
<!--main-container-part-->
<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="icon icon-home"></i> Home</a>
            <a href="/reservation">Reservation</a>
            <a href="#" class="current">Create</a>
        </div>
        <h1>Create Reservation</h1>
    </div>
 
 <div class = "col-md-12">
    <form action = "{{ url ('reservation')}}" method="post">
 <div class="container-fluid">
        <hr>
      <div class="row-flex">
        
            <input type="hidden" name="_token" value ="{{ csrf_token() }}">
                <div class="col-md-6 bg-color">
                    <div class="pb-2">
                            <div class="column">
                                <h3 class="pb-2"><center>Customer Details</center></h3>
                                  <div class="col-margin">
                                        <div class="input-group row-flex">
                                            <label class="control-label label-sizes">First Name :</label>
                                            <input id="input-positioning1" type="text" class="form-control input-sizes" placeholder="First name" name="firstname" id="firstname" value="{{ old('firstname') }}" required/>
                                        </div>

                                        <div class="input-group row-flex">
                                            <label class="control-label label-sizes">Last Name :</label>
                                            <input id="input-positioning2" type="text" class="form-control input-sizes" placeholder="Last name" name="lastname" id="lastname" value="{{ old('lastname') }}" required/>
                                        </div>

                                        <div class="input-group row-flex">
                                            <label class="control-label label-sizes">Email :</label>
                                            <input id="input-positioning3" type="email" class="form-control input-sizes" placeholder="Email" name="email" id="email" value="{{ old('email') }}" required/>
                                        </div>

                                        <div class="input-group row-flex">
                                            <label class="control-label label-sizes">Address :</label>
                                            <input id="input-positioning4" type="text" class="form-control input-sizes" placeholder="Address" name="address" id="address" value="{{ old('address') }}" required/>
                                        </div>
                                        <div class="input-group pt-1  row-flex">
                                            <label class="control-label label-sizes">Contact Number :</label>
                                            <input  style="margin-left: 33px;" type="text" class="form-control input-sizes" placeholder="Contact Number" name="contact" id="contact" value="{{ old('contact') }}" required/>
                                        </div>
                                  </div>
                            </div>
                    </div>
                </div>

                <div class="col-md-6 span4-bg">
                        <div class="pb-2">
                            <h3 class="pb-2"><center>Reservation Details</center></h3>
                            <div class="input-group">
                                <label class="control-label">Reservation Date (mm / dd / yyyy)</label>
                                <div class="input-append date" id="dp3">
                                    <!-- <input name="reservedate" class="form-control date datepicker" id="reservedate" type="text" value="12-02-2012" class="span11"> -->
                                    <input id="reservedate" class="span2" size="16" type="date" min="{{ $date }}">
                                    <span class="add-on"><i class="icon-th"></i></span>
                                </div>
                            </div>

                            <div class="input-group">
                                <label class="control-label">Event Time :</label>
                                <input type="Time" class="form-control input-eventtime" placeholder="Event Time" name="time" id="time" value="{{ old('time') }}" required min='07:00:00' max='21:00:00'>
                            </div>

                            <div class="input-group">
                                <label class="control-label">Event type :</label>
                                <select id="event_type" name="event_type" class="form-control select-eventType">
                                    <option disabled>Select event type</option>
                                    @foreach($types as $type)
                                        <option value="{{ $type->id }}">{{ $type->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="input-group">
                                <label class="control-label">Event Location :</label>
                                <input type="text" class="form-control input-eventLocation" placeholder="Event Location" name="location" id="location" value="{{ old('location') }}" required>
                            </div>

                            <div class="input-group">
                                <label class="control-label">Number of People :</label>
                                <input type="number" class="form-control input-numberPeople" placeholder="Number of People" name="guest" id="guest" min="75" max="900" value="{{ old('guest') }}">
                            </div>

                            <div class="input-group">
                                <label class="control-label">Select food package :</label>
                                <select id="package" name="package" class="form-control select-foodPackage">
                                    @foreach($packages as $package)
                                        <option value="{{ $package->id }}">{{ $package->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <!-- <div class="input-group">
                                <label class="control-label"> Select Service Type : </label>
                                <select id="service" class="form-control select-serviceType">
                                    <option vlaue="">Service Type</option>
                                </select>
                            </div> -->

                            <div class="d-flex justify-content-center">
                                <button type="submit" name="view-package" value="{{ csrf_token() }}" class="btn btn-info icon icon-eye-open packview"> View Package</button>                            
                            </div>
                        </div>    
                    </div>

        </div> <!--div row -->

                    <!--- Hello motherfucker -->

                  <div class="col-md-12 bg-color">
                        <div class="header-size">
                            <h2>Food Package</h2>
                        </div>
                    <div class="container-new">

                    @if( Session::has('categories') && Session::has('othercat') )

                        <div class="col-sm-4">
                            <h1> Appetizer </h1>
                            <h4 class="h4-color"> Choose 1 </h4>
                            @foreach(Session::get('othercat') as $othercat)
                                    
            
                                    @if($othercat -> type_name == "Appetizer" )
                                        <input type="hidden" id="limits1" value="{{ $othercat -> amount}}">
                                        <label><input class="limitOne" type="checkbox" value="{{ $othercat -> food_id }}" name="food[]" id="{{ $othercat -> food_id }}">  {{ $othercat -> food_name }}  </label>  <br>
                                    @endif


                            @endforeach
                        </div>

                        <div class="col-sm-4">
                            <h1> Soup </h1>
                            <h4 class="h4-color"> Choose 1 </h4>
                            @foreach(Session::get('othercat') as $othercat)
                                    
            
                                    @if($othercat -> type_name == "Soup" )
                                        <input type="hidden" id="limits8" value="{{ $othercat -> amount}}">
                                        <label><input class="limitEight" type="checkbox" value="{{ $othercat -> food_id }}" name="food[]" id="{{ $othercat -> food_id }}">  {{ $othercat -> food_name }}  </label>  <br>
                                    @endif


                            @endforeach
                        </div>

                        <div class="col-sm-4">
                            <h1> Salad </h1>
                            <h4 class="h4-color"> Choose 1 </h4>
                            @foreach(Session::get('othercat') as $othercat)
                                    
            
                                    @if($othercat -> type_name == "Salad" )
                                        <input type="hidden" id="limits9" value="{{ $othercat -> amount}}">
                                        <label><input class="limitNine" type="checkbox" value="{{ $othercat -> food_id }}" name="food[]" id="{{ $othercat -> food_id }}">  {{ $othercat -> food_name }}  </label>  <br>
                                    @endif


                            @endforeach
                        </div>

                        <div class="col-sm-4">
                            <h1> Dessert </h1>
                            <h4 class="h4-color"> Choose 1 </h4>
                            @foreach(Session::get('othercat') as $othercat)
                                    
            
                                    @if($othercat -> type_name == "Dessert" )
                                        <input type="hidden" id="limits10" value="{{ $othercat -> amount}}">
                                        <label><input class="limitTen" type="checkbox" value="{{ $othercat -> food_id }}" name="food[]" id="{{ $othercat -> food_id }}">  {{ $othercat -> food_name }}  </label>  <br>
                                    @endif


                            @endforeach
                        </div>

                        <div class="col-sm-4">
                            <h1> Drinks </h1>
                            <h4 class="h4-color"> Choose 1 </h4>
                            @foreach(Session::get('othercat') as $othercat)
                                    
            
                                    @if($othercat -> type_name == "Drinks" )
                                        <input type="hidden" id="limits11" value="{{ $othercat -> amount}}">
                                        <label><input class="limitEl" type="checkbox" value="{{ $othercat -> food_id }}" name="food[]" id="{{ $othercat -> food_id }}">  {{ $othercat -> food_name }}  </label>  <br>
                                    @endif


                            @endforeach
                        </div>

                        <div class="col-sm-4">
                        <h1> Pasta </h1>
                        <h4 class="h4-color"> Choose 1 </h4>
                        @foreach(Session::get('categories') as $categories)
                                
        
                                @if($categories -> type_id == 1 )
                                    <input type="hidden" id="limits12" value="{{ $categories -> amount}}">
                                    <label><input class="limitTwelve" type="checkbox" value="{{ $categories -> food_id }}" name="food[]" id="{{ $categories -> food_id }}">  {{ $categories -> food_name }}  </label>  <br>
                                @endif


                        @endforeach
                        </div>

                        <div class="col-sm-4">
                        <h1> Beef </h1>
                        <h4 class="h4-color"> Choose 1 </h4>
                        @foreach(Session::get('categories') as $categories)
                                
        
                                @if($categories -> type_id == 2 )

                                    <input type="hidden" id="limits2" value="{{ $categories -> amount}}">
                                    <label><input class="limitTwo" type="checkbox" value="{{ $categories -> food_id }}" name="food[]" id="checkbox">  {{ $categories -> food_name }}  </label>  <br>
                                @endif


                        @endforeach
                    </div>
                         <div class="col-sm-4">
                        <h1> Chicken </h1>
                        <h4 class="h4-color"> Choose 1 </h4>
                        @foreach(Session::get('categories') as $categories)
                                @if($categories -> type_id == 3 )
                                    <input type="hidden" id="limits3" value="{{ $categories -> amount}}">
                                    <label><input class="limitThree" type="checkbox" value="{{ $categories -> food_id }}" name="food[]" id="chickcheck"> : {{ $categories -> food_name }}  </label>  <br>

                                    <label><input class="limitThree" type="checkbox" value="{{ $categories -> food_id }}" name="food[]" id="chickcheck">  {{ $categories -> food_name }}  </label>  <br>
                                @endif


                        @endforeach
                    </div>
                         <div class="col-sm-4">
                        <h1> Fish </h1>
                        <h4 class="h4-color"> Choose 1 </h4>
                        @foreach(Session::get('categories') as $categories)
                                
        
                                @if($categories -> type_id == 4 )
                                    <input type="hidden" id="limits4" value="{{ $categories -> amount}}">
                            
                                    <label><input class="limitFour" type="checkbox" value="{{ $categories -> food_id }}" name="food[]" id="fishcheck">  {{ $categories -> food_name }}  </label>  <br>
                                @endif


                        @endforeach
                        </div>
                         <div class="col-sm-4">
                        <h1> Vegetable </h1>
                        <h4 class="h4-color"> Choose 1 </h4>
                        @foreach(Session::get('categories') as $categories)
                                
        
                                @if($categories -> type_id == 5 )
                                    <input type="hidden" id="limits5" value="{{ $categories -> amount}}">
                                    
                                    <label><input class="limitFive" type="checkbox" value="{{ $categories -> food_id }}" name="food[]" id="vegecheck">  {{ $categories -> food_name }}  </label>  <br>
                                @endif


                        @endforeach
                    </div>
                         <div class="col-sm-4">                       
                        <h1> Rice </h1>
                        <h4 class="h4-color"> Choose 1 </h4>
                        @foreach(Session::get('categories') as $categories)
                                
        
                                @if($categories -> type_id == 6 )
                                <input type="hidden" id="limits6" value="{{ $categories -> amount}}">
                                    
                                    <label><input class="limitSix" type="checkbox" value="{{ $categories -> food_id }}" name="food[]" id="ricecheck">  {{ $categories -> food_name }}  </label>  <br>
                                @endif


                        @endforeach
                        </div>
                         <div class="col-sm-4">
                        <h1> Pork </h1>
                        <h4 class="h4-color"> Choose 1 </h4>
                        @foreach(Session::get('categories') as $categories)
                                
        
                                @if($categories -> type_id == 7 )
                                <input type="hidden" id="limits7" value="{{ $categories -> amount}}">
                                    
                                    <label><input class="limitSeven" type="checkbox" value="{{ $categories -> food_id }}" name="food[]" id="porkcheck"> {{ $categories -> food_name }}  </label>  <br>
                                @endif


                        @endforeach
                        
                        </div>

                        @endif <!-- Session if -->


                     </div>

                  </div>

                </div>    
            </div>
             <!-- END OF ROW -->

                <div id="btn-forCreate" class="d-flex justify-content-center">
                    <button type="submit" class="btn btn-large btn-success justify-content-end" style="width: 200px;">Create</button>
                </div>  
            </form> 
        </div>
        </div>

@endsection

  
@section('after_scripts')
<script type="text/javascript">
    $(document).ready(function () {
            $('#calendar').fullCalendar({
                dayClick: function() {
                    alert('a day has been clicked!');
                },
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,basicWeek,basicDay'
                },
                defaultDate: new Date(),
                navLinks: true, // can click day/week names to navigate views
                editable: false,
                eventLimit: true, // allow "more " link when too many events
                events: {{ json_encode($events_data) }},
            });
        })
</script>
<script type="text/javascript">
    // $('.packview').click(function(){
    //     var package = document.getElementById("package");
    //     var packView = package.options[package.selectedIndex].value;
    //     window.location.href = '/packView/' + packView;
    // }); 

</script>   

<script type="text/javascript">
$(document).ready(function() {
    $("#contact").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    
    $limits = $('#limits1').val()


   $('.limitOne').on('change', function(){
    var noChecked = 0;
    $.each($('.limitOne'), function(){
        if($(this).is(':checked')){
            noChecked++;    
        }
    });
    if(noChecked >= $limits){
           
  
        $.each($('.limitOne'), function(){
            if($(this).not(':checked').length == 1){
                $(this).attr('disabled','disabled');  
            }
              
        });
        alert('The Limits is Exceeded');
       }else{
        $('.limitOne').removeAttr('disabled');    
    };
});


    $limits2 = $('#limits2').val()


   $('.limitTwo').on('change', function(){
    var noChecked = 0;
    $.each($('.limitTwo'), function(){
        if($(this).is(':checked')){
            noChecked++;    
        }
    });
    if(noChecked >= $limits2){
        $.each($('.limitTwo'), function(){
            if($(this).not(':checked').length == 1){
                $(this).attr('disabled','disabled');  
            }
              
        });
        
        alert('The Limits is Exceeded');
    }else{
        $('.limitTwo').removeAttr('disabled');    
    };
});


    $limits3 = $('#limits3').val()


$('.limitThree').on('change', function(){
 var noChecked = 0;
 $.each($('.limitThree'), function(){
     if($(this).is(':checked')){
         noChecked++;    
     }
 });
 if(noChecked >= $limits3){
     $.each($('.limitThree'), function(){
         if($(this).not(':checked').length == 1){
             $(this).attr('disabled','disabled');  
         }
           
     });
     
     alert('The Limits is Exceeded');
 }else{
     $('.limitThree').removeAttr('disabled');    
 };
});


    $limits4 = $('#limits4').val()


$('.limitFour').on('change', function(){
 var noChecked = 0;
 $.each($('.limitFour'), function(){
     if($(this).is(':checked')){
         noChecked++;    
     }
 });
 if(noChecked >= $limits4){
     $.each($('.limitFour'), function(){
         if($(this).not(':checked').length == 1){
             $(this).attr('disabled','disabled');  
         }
           
     });
     
     alert('The Limits is Exceeded');
 }else{
     $('.limitFour').removeAttr('disabled');    
 };
});


    $limits5 = $('#limits5').val()


$('.limitFive').on('change', function(){
 var noChecked = 0;
 $.each($('.limitFive'), function(){
     if($(this).is(':checked')){
         noChecked++;    
     }
 });
 if(noChecked >= $limits5){
     $.each($('.limitFive'), function(){
         if($(this).not(':checked').length == 1){
             $(this).attr('disabled','disabled');  
         }
           
     });
     
     alert('The Limits is Exceeded');
 }else{
     $('.limitFive').removeAttr('disabled');    
 };
});


    $limits6 = $('#limits6').val()


$('.limitSix').on('change', function(){
 var noChecked = 0;
 $.each($('.limitSix'), function(){
     if($(this).is(':checked')){
         noChecked++;    
     }
 });
 if(noChecked >= $limits6){
     $.each($('.limitSix'), function(){
         if($(this).not(':checked').length == 1){
             $(this).attr('disabled','disabled');  
         }
           
     });
     
     alert('The Limits is Exceeded');
 }else{
     $('.limitSix').removeAttr('disabled');    
 };
});


    $limits7 = $('#limits7').val()


$('.limitSeven').on('change', function(){
 var noChecked = 0;
 $.each($('.limitSeven'), function(){
     if($(this).is(':checked')){
         noChecked++;    
     }
 });
 if(noChecked >= $limits7){
     $.each($('.limitSeven'), function(){
         if($(this).not(':checked').length == 1){
             $(this).attr('disabled','disabled');  
         }
           
     });
     
     alert('The Limits is Exceeded');
 }else{
     $('.limitSeven').removeAttr('disabled');    
 };
});


$limits8 = $('#limits8').val()


$('.limitEight').on('change', function(){
 var noChecked = 0;
 $.each($('.limitEight'), function(){
     if($(this).is(':checked')){
         noChecked++;    
     }
 });
 if(noChecked >= $limits8){
        

     $.each($('.limitEight'), function(){
         if($(this).not(':checked').length == 1){
             $(this).attr('disabled','disabled');  
         }
           
     });
     alert('The Limits is Exceeded');
    }else{
     $('.limitEight').removeAttr('disabled');    
 };
});

$limits9 = $('#limits9').val()


$('.limitNine').on('change', function(){
 var noChecked = 0;
 $.each($('.limitNine'), function(){
     if($(this).is(':checked')){
         noChecked++;    
     }
 });
 if(noChecked >= $limits9){
        

     $.each($('.limitNine'), function(){
         if($(this).not(':checked').length == 1){
             $(this).attr('disabled','disabled');  
         }
           
     });
     alert('The Limits is Exceeded');
    }else{
     $('.limitNine').removeAttr('disabled');    
 };
});

$limits10 = $('#limits10').val()


$('.limitTen').on('change', function(){
 var noChecked = 0;
 $.each($('.limitTen'), function(){
     if($(this).is(':checked')){
         noChecked++;    
     }
 });
 if(noChecked >= $limits10){
        

     $.each($('.limitTen'), function(){
         if($(this).not(':checked').length == 1){
             $(this).attr('disabled','disabled');  
         }
           
     });
     alert('The Limits is Exceeded');
    }else{
     $('.limitTen').removeAttr('disabled');    
 };
});

$limits11 = $('#limits11').val()


$('.limitEl').on('change', function(){
 var noChecked = 0;
 $.each($('.limitEl'), function(){
     if($(this).is(':checked')){
         noChecked++;    
     }
 });
 if(noChecked >= $limits11){
        

     $.each($('.limitEl'), function(){
         if($(this).not(':checked').length == 1){
             $(this).attr('disabled','disabled');  
         }
           
     });
     alert('The Limits is Exceeded');
    }else{
     $('.limitEl').removeAttr('disabled');    
 };
});

$limits12 = $('#limits12').val()


$('.limitTwelve').on('change', function(){
 var noChecked = 0;
 $.each($('.limitTwelve'), function(){
     if($(this).is(':checked')){
         noChecked++;    
     }
 });
 if(noChecked >= $limits11){
        

     $.each($('.limits12'), function(){
         if($(this).not(':checked').length == 1){
             $(this).attr('disabled','disabled');  
         }
           
     });
     alert('The Limits is Exceeded');
    }else{
     $('.limitTwelve').removeAttr('disabled');    
 };
});



$(function(){
    var dtToday = new Date();
    
    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();
    
    var maxDate = year + '-' + month + '-' + day;
    $('#reservedate').attr('min', maxDate);
});



});


</script>

<script type="text/javascript">
$(document).ready(function() {
    
    var navListItems = $('ul.setup-panel li a'),
        allWells = $('.setup-content');

    allWells.hide();

    navListItems.click(function(e)
    {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this).closest('li');
        
        if (!$item.hasClass('disabled')) {
            navListItems.closest('li').removeClass('active');
            $item.addClass('active');
            allWells.hide();
            $target.show();
        }
    });
    
    $('ul.setup-panel li.active a').trigger('click');
    
    $('#activate-step-2').on('click', function(e) {
        $('ul.setup-panel li:eq(1)').removeClass('disabled');
        $('ul.setup-panel li a[href="#step-2"]').trigger('click');
        $(this).remove();
    })    
});

</script> 

<script type="text/javascript">

    var accordions = $('.btn-1');

    for (var i = 0; 1 < accordions.length; i++) {
        accordions[i].onclick = function() {
            this.classList.toggle('is-open')
            var content = this.nextElementSibling;

            if (content.style.maxHeight) {
                content.style.maxHeight = null;
            } else {
                content.style.maxHeight = content.scrollHeight + "px";
            }
        }
    }
</script>

<script type="text/javascript">
    $("#event_type").change(function() {
        var now = new Date();

        if ($(this).find(":selected").text() == "Conference") {
            now.setMonth(now.getMonth() + 1);
            console.log("Month has been updated: " + now);
        }
        else if ($(this).find(":selected").text() == "Wedding") {
            now.setDate(now.getDate() + 14);
        }
        else {
            now.setDate(now.getDate() + 7);
        }
        
        console.log("Blockage: " + now.getFullYear() + "-" + now.getMonth() + "-" + now.getDate());
        console.log("minimum date: " + $("#reservedate").attr('min'));
        $("#reservedate").attr("min", now.getFullYear() + "-" + now.getMonth() + "-" + now.getDate());
        /*
        $("#reservedate").datepicker({
            onRender: function(date) {
                return date.valueOf() < now.valueOf() ? 'disabled' : '';
            }
        });
        */
        //$("#reservedate").datePicker('option', 'minDate', now.getFullYear() + "-" + now.getMonth() + "-" + now.getDate());
    });
</script>
@endsection