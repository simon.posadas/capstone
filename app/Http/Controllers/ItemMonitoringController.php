<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use DB;

class ItemMonitoringController extends Controller
{
    public function index () {
        $reservations = App\Reservation::with('customer')
        // ->where(DB::raw("DAY(NOW()) - DAY(event_date)"), '>=', '0')
        ->get();
        return view('admin.item-monitoring', ['reservations' => $reservations]);
    }

    public function getReservationItems ($reservation_id) {
        $reservations = App\Reservation::with('customer', 'equipments')->where('id', $reservation_id)->first();
        // $equips = App\Reservation::with('equipments')->where('id', $reservation_id)->get();
        return view('admin.item-monitoring_reservation', ['reservations' => $reservations]);
    }

    public function getItems ($reservation_id) {
        $reservations = App\Reservation::with('customer')->where('id', $reservation_id)->first();
        $items = App\Equipment::with('equipment_type')->get();
        // return $reservations;
        return view('admin.item-monitoring_tagging', ['reservations' => $reservations, 'items' => $items]);
    }

    public function tagItem (Request $request) {
        if($request->isMethod('POST')) {
            $data = $request->json()->all();

            try {
                $reservation_equipment = App\ReservationEquipment::where([
                    ['reserve_id', '=', $data['reservation_id']],
                    ['equipment_id', '=', $data['item_id']]
                ])->first();

                $reservation_equipment = App\ReservationEquipment::where('reserve_id', '=', $data['reservation_id'])
                    ->where('equipment_id', '=', $data['item_id'])
                    ->first();

                if($reservation_equipment === null) {
                    $reservation_equipment = new App\ReservationEquipment;
                    $reservation_equipment->fill([
                        'reserve_id' => $data['reservation_id'],
                        'equipment_id' => $data['item_id'],
                        'quantity' => $data['quantity'],
                        'reserved' => 0,
                        'release' => 0,
                        'returned' => 0
                    ]);
                }else {
                    $reservation_equipment->quantity += $data['quantity'];
                }

                $equipment = App\Equipment::find($data['item_id']);
                $equipment->quantity -= $data['quantity'];
                $equipment->save();

                $result['status'] = $reservation_equipment->save();
            } catch (\Illuminate\Database\QueryException $e) {
                $result['status'] = false;
                $result['error'] = $e->getMessage();
            }

            return $result;
        }
    }

    public function releaseItem(Request $request) {
        if($request->isMethod('POST')) {
            $data = $request->json()->all();

            try {
                $reservation_equipment = App\ReservationEquipment::where([
                    ['reserve_id', '=', $data['reservation_id']],
                    ['equipment_id', '=', $data['item_id']]
                ])->first();

                if($reservation_equipment->release == 0) {
                    $reservation_equipment->release = 1;
                }else {
                    $reservation_equipment->release = 0;
                }

                $result['status'] = $reservation_equipment->save();
            } catch (\Illuminate\Database\QueryException $e) {
                $result['status'] = false;
                $result['error'] = $e->getMessage();
            }

            return $result;
        }
    }

    public function returnItem(Request $request) {
        if($request->isMethod('POST')) {
            $data = $request->json()->all();

            try {
                $reservation_equipment = App\ReservationEquipment::where([
                    ['reserve_id', '=', $data['reservation_id']],
                    ['equipment_id', '=', $data['item_id']]
                ])->first();
                
                $reservation = App\Reservation::find($data['reservation_id']);
                $equipment = App\Equipment::find($data['item_id']);
                
                $price = $equipment->price;
                $return_quantity = $data['quantity'];
                $required_return = $reservation_equipment->quantity;

                if($reservation_equipment->returned == 0) {
                    if($return_quantity == $required_return){

                        $equipment->quantity += $return_quantity;

                        $reservation_equipment->quantity_returned = $return_quantity;
                        $reservation_equipment->returned = 1;

                        $equipment->save();
                        $result['status'] = $reservation_equipment->save();

                        alert()->success('Successfully returned an item', 'Success')->persistent('Close');
                    
                    } elseif($return_quantity > $required_return) {

                        $result['status'] = false;
                        alert()->error('Returned quantity is greater than released quantity', 'Error')->persistent('Close');
                    
                    } elseif($return_quantity < $required_return) {
                        $id = $data['reservation_id'];
                        $kulang = $required_return - $return_quantity;
                        $bill = $kulang * $price;

                        $equipment->quantity += $return_quantity;

                        $reservation_equipment->quantity_returned = $return_quantity;
                        $reservation_equipment->returned = 1;

                        $billing = new App\Billing;
                        $billing->code = $billing->generateCode();
                        $billing->status = 0;
                        $billing->description = "Item lost for Reservation $reservation->code";
                        $billing->billing_type = 1;
                        $billing->amount = $bill;
                        $billing->reservation_id = $id;
                        $billing->save();

                        $equipment->save();
                        $result['status'] = $reservation_equipment->save();

                        alert()->success('Successfully returned an item but with insufficiency. Bill was added with a balance of ₱' . $bill, 'Success')
                            ->persistent('Close');
                    }
                    
                }else {
                    $reservation_equipment->returned = 0;
                }

            } catch (\Illuminate\Database\QueryException $e) {
                $result['status'] = false;
                $result['error'] = $e->getMessage();
            }

            return $result;
        }
    }

    public function insert() {
        $reservation_equipment = new App\ReservationEquipment;
        $reservation_equipment->fill(['reserve_id' => 3,'equipment_id' => 3,'quantity' => 3,'reserved' => 3,'release' => 3,'returned' => 3]);
        $status = $reservation_equipment->save();
    }
}
