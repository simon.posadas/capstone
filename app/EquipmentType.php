<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EquipmentType extends Model
{
    /**
     * The table to fetch
     * @var type 
     */
    protected $table = 'equipment_types';
    
     /**
     * Unique ID
     * @var type 
     */
    protected $primaryKey = 'id';
    
    /**
     *
     * @var type Remove automatic generation of timestamp
     */
    public $timestamps = false;

     /**
     * Mass variable assignment
     * @var type 
     */
    protected $fillable = ['type_description'];
}