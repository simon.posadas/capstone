@extends('layouts.sidebar') 

@section('title', 'Customer') 

@section('content')

<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="fa fa-home"></i> Home</a>
            <a href="#" class="current">Customers</a>
        </div>
        <h1>Customer</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Customer Name</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            @foreach($cust as $cust)
                            <tbody>
                                <tr>
                                    <td>{{$cust->cust_fname . " " . $cust->cust_lname}}</td>
                                    <td>
                                        <!-- <button class="btn fa fa-pencil btn-primary edittype" data-id="{{ $cust->cust_id }}"> Edit</button>
                                        <button class="btn btn-danger fa fa-trash deletetype" data-id="{{ $cust->cust_id }}"> Delete</button> -->
                                        WIP
                                    </td>
                                </tr>
                            </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection 

@section('modal')
@endsection 

@section('script')

@endsection