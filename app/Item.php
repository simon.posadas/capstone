<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    //
        /**
     * The table to fetch
     * @var type 
     */
    protected $table = 'equipments';
    
     /**
     * Unique ID
     * @var type 
     */
    protected $primaryKey = 'id';

    /**
     * Variable mass assignement
     * @var type 
     */
    protected $fillable = ['name', 'quantity', 'status', 'type_id', 'price'];
}
