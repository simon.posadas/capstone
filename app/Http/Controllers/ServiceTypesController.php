<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App;
use DB;

class ServiceTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = App\ServiceType::orderBy('name')
            ->get();

        return view('service.type.index')
                ->with('types', $types);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = $request->get('name');

        $type = new App\ServiceType;

        $validator = Validator::make([
            'name' => $name
        ], $type->rules());

        if($validator->fails())
        {
            return back()->withInput()->withErrors($validator);
        }

        $type->code = $type->generateCode();
        $type->name = $name;
        $type->save();

        $request->session()->flash('success-message', 'Service Type Added');
        return redirect('service/type');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name = $request->get('name');

        $type = App\ServiceType::find($id);

        $validator = Validator::make([
            'name' => $name
        ], $type->updateRules());

        if($validator->fails())
        {
            return back()->withInput()->withErrors($validator);
        }

        $type->name = $name;
        $type->save();

        $request->session()->flash('success-message', 'Service Type Updated');
        return redirect('service/type');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $type = new App\ServiceType;

        if(count($type) <= 0) $type = new App\ServiceType;

        $validator = Validator::make([
            'Service Type' => $id 
        ], $type->deleteRules());

        if($validator->fails())
        {
            return back()->withErrors($validator);
        }

        $type = App\ServiceType::find($id);
        $type->delete();

        $request->session()->flash('success-message', 'Service Type Removed');
        return redirect('service/type');
    }
}
