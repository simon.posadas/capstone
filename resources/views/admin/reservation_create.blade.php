@extends('layouts.sidebar')

@section('title', 'Create Reservation')

@section('content')
<!--main-container-part-->
<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="fa fa-home"></i> Home</a>
            <a href="/admin/reservation">Reservations</a>
            <a href="#" class="current">Create Reservation</a>
        </div>
        <h1>Create Reservation</h1>
    </div>
    <div class="container-fluid">
        <hr>

        <div class="span8">
            <!-- calendar -->
            <div class="span8 row">
                <div class="span7">
                    <div class="widget-box widget-calendar">
                        <div class="widget-title">
                            <span class="fa fa">
                                <i class="fa fa-calendar pt-1"></i>
                            </span>
                            <h5>Calendar</h5>
                        </div>
                        <div class="widget-content">
                            <div id="fullcalendar"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end calendar -->
        </div>


        <div class="container span8" id="createReservation">
            <div class="row">
            <form action="/create_reservation" method="post">
                        {{csrf_field()}}
                            <!-- <div class="input-forms-container span8"> -->

                <div class="span4">
                    <div class="pb-2">
                            <div class="column">
                                <h4 class="pb-2">Customer Details</h4>
                                    <div class="input-group">
                                        <label class="control-label">First Name :</label>
                                        <input type="text" class="form-control" placeholder="First name" name="fname" id="fname" value="{{ old('fname') }}" required/>
                                    </div>

                                    <div class="input-group">
                                        <label class="control-label">Last Name :</label>
                                        <input type="text" class="form-control" placeholder="Last name" name="lname" id="lname" required/>
                                    </div>

                                    <div class="input-group">
                                        <label class="control-label">Email :</label>
                                        <input type="email" class="form-control" placeholder="Email" name="email" id="email" required/>
                                    </div>

                                    <div class="input-group">
                                        <label class="control-label">Address :</label>
                                        <input type="text" class="form-control" placeholder="Address" name="address" id="address" required/>
                                    </div>

                                    <div class="input-group">
                                    <label class="control-label">Gender</label>
                                        <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="gender" value="M">
                                            Male
                                        </label>
                                        </div>

                                        <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="gender" value="F">
                                            Female
                                        </label>
                                        </div>
                                    </div>

                                    <div class="input-group pt-1">
                                        <label class="control-label">Contact Number :</label>
                                        <input type="number" class="form-control" placeholder="Contact Number" name="contno" id="contno" required/>
                                    </div>
                                <!-- END OF FORM -->
                            </div>
                    </div>
                </div>
<<<<<<< HEAD
            
=======
                <!--end of CUSTOMER DETAILS  -->

>>>>>>> 044e0248754daed7d8a65844c0d2197458df52df
                <!-- EVENT DETAILS -->
                <div class="span4">
                    <div class="">
                        <div class="pb-2">
                            <h4 class="pb-2">Event Details</h4>
                            <div class="input-group">
                                <label class="control-label">Reservation Date (mm-dd-yyyy)</label>
                                <div data-date="12-02-2012" class="input-append date datepicker">
                                    <input name="reservedate" class="form-control" id="reservedate" type="date" value="12-02-2012" data-date-format="dd-mm-yyyy" class="span11">
                                </div>
                            </div>

                            <div class="input-group">
                                <label class="control-label">Event Time :</label>
                                <input type="Time" class="form-control" placeholder="Event Time" name="time" id="time" required min='07:00:00' max='21:00:00'>
                            </div>

                            <div class="input-group">
                                <label class="control-label">Event type :</label>
                                <select name="event_type" class="form-control">
                                    <option disabled>Select event type</option>
                                    @foreach($types as $type)
                                        <option value="{{$type->type_id}}">{{$type->type_name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="input-group">
                                <label class="control-label">Event Location :</label>
                                <input type="text" class="form-control" placeholder="Event Location" name="location" id="location" required>
                            </div>

                            <div class="input-group">
                                <label class="control-label">Number of People :</label>
                                <input type="number" class="form-control" placeholder="Number of People" name="pipno" id="pipno" min="75" max="900">
                            </div>

                            <div class="input-group">
                                <label class="control-label">Select food type :</label>
                                <select id="package" name="package" class="form-control">
                                    <option disabled>Select package</option>
                                    @foreach($packages as $package)
                                        <option value="{{$package->package_id}}">{{$package->package_name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="d-flex justify-content-center">
                                <button type="button" class="btn btn-info fa fa-eye packview"> View Package</button>                            
                            </div>
                        </div>    
                    </div>
                </div>    
            </div> <!-- END OF ROW -->

                <div class="d-flex justify-content-center">
                    <button type="submit"class="btn btn-large btn-success justify-content-end" style="width: 200px;">Create Reservation</button>
                </div>  
            </form> 
        </div>
    

<script src="{{ asset('fullcalendar/lib/moment.min.js') }}"></script>
<script src="{{ asset('fullcalendar/lib/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/matrix.calendar.js') }}"></script>
<script src="{{ asset('js/jquery.ui.custom.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/matrix.js') }}"></script>
<script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('js/masked.js') }}"></script>
<script src="{{ asset('js/jquery.peity.min.js') }}"></script>
<script src="{{ asset('js/select2.min.js') }}"></script>
<script src="{{ asset('js/jquery.toggle.buttons.js') }}"></script> 
<script src="{{ asset('js/matrix.calendar.js') }}"></script>

<script type="text/javascript">
$(document).ready(function () {

            $('#calendar').fullCalendar({
                dayClick: function() {
                    alert('a day has been clicked!');
                },
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,basicWeek,basicDay'
                },
                defaultDate: new Date(),
                navLinks: true, // can click day/week names to navigate views
                editable: false,
                eventLimit: true, // allow "more " link when too many events
                events: <?php echo json_encode($events_data) ?>
            });
        });
</script>

@endsection

@section('modal')
<!-- view modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="deleteModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Delete</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered data-table">
                    <thead>
                        <tr>
                            <th>Food Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="inpname"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<!-- end view modal -->
@endsection

@section('script')
<script type="text/javascript">
        

        $('.packview').click(function(){
            var package = document.getElementById("package");
            var packView = package.options[package.selectedIndex].value;
            window.location.href = '/packView/' + packView;
        }); 

</script>   
@endsection