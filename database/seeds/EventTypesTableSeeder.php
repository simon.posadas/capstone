<?php

use Illuminate\Database\Seeder;

class EventTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('event_types')->insert([
        	[
        		'name' => 'Conference',
        		'range' => '30',
        	],

        	[
        		'name' => 'Seminar',
        		'range' => '30',
        	],

			[
        		'name' => 'Anniversary',
        		'range' => '30',
        	],

        	[
        		'name' => 'Wedding',
        		'range' => '14',
        	],

        	[
        		'name' => 'Birthday',
        		'range' => '7',
        	],

        	[
        		'name' => 'Baptismal',
        		'range' => '7',
        	],

        	[
        		'name' => 'Birthday (Debut)',
        		'range' => '14',
        	],

        	[
        		'name' => 'Birthday (Kids Party)',
        		'range' => '7',
        	]
        ]);
    }
}
