@extends('layouts.sidebar')

@section('title', 'Monitoring')

@section('content')

<!--main-container-part-->
<meta name="csrf-token" content="{{ csrf_token() }}">
<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="fa fa-home"></i> Home</a>
            <a href="#" class="current">Monitoring</a>
        </div>
        <h1>Monitoring</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="colspan">
                <div class="widget-box">
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Customer Name</th>
                                    <th>Contact Number</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                            </thead>
                            @foreach($details as $detail)
                            <tbody>
                                <tr>
                                    <td>{{$detail->cust_fname . " " . $detail->cust_lname}}</td>
                                    <td>{{$detail->contNo}}</td>
                                    <td>{{ config()->get('constants.status')[$detail->status] }}</td>
                                    <td class="btn-center">
                                        <button id ="sizes" class="btn btn-info icon-eye-open viewtype" data-id="{{$detail->reserv_id}}"> View More Details</button>
                                        @if($detail->status == 2)
                                            <button id ="sizes" class="btn btn-success icon-plus addcont" data-id="{{$detail->reserv_id}}"> Add contractor</button>
                                        @endif
                                    </td>
                                </tr>
                            </tbody> 
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--main-container-part-->

@endsection 

@section('modal')
<!-- add contractor modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="addModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Assign Contractor</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/addFoodType">
                    {{csrf_field()}}
                <div class="control-group">
                    <label class="control-label">Select contractor to assign</label>
                    <div class="controls select2-container">
                        <select name="food_type">
                            <option disabled>Select contractor</option>
                            @foreach($contractors as $contractor)
                            <option value="{{$contractor->contractor_id}}">{{$contractor->contractor_fname .' '. $contractor->contractor_lname}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- end add contractor modal -->

<!-- view details modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="viewModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">View Details</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Guest Number</label>
                    <input type="text" class="form-control gnumber" name="gname" id="gname">
                </div>
                <div class="form-group">
                    <label>Event Place</label>
                    <input type="text" class="form-control eplace" name="eplace" id="eplace">
                </div>
                <div class="form-group">
                    <label>Event Time</label>
                    <input type="text" class="form-control etime" name="etime" id="etime">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- end view details modal -->
@endsection

@section('script')
<script>
    $('.addcont').click(function(){
        $('#addModal').modal('show');
    });

    $('.viewtype').click(function(){
        $.ajax({
            type: "post",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/getDetails',
            data: {
                id: $(this).data('id')
            },
            dataType: "json",
            success: function (response) {
                response.forEach(function (data) {
                    $('#gnumber').val(data.reserv_guestNo);
                    $('#eplace').val(data.place);
                    $('#etime').val(data.reserv_time);
                })
            },
            error: function () {
                alert('error');
            }
        });
        alert($(this).data('id'));
        $('#viewModal').modal('show');
    });

</script>
@endsection