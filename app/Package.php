<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon;

class Package extends Model
{
    /**
     * Table to connect
     * @var type 
     */
    protected $table = 'packages';
    protected $primaryKey = 'id';
   
    /**
     * Remove automatic timestamp
     * @var type 
     */
    public $timestamps = true;

    public function rules()
    {
        return [
            'name' => 'required|min:2|max:20|unique:packages,name'
        ];
    }

    public function updateRules()
    {
        $name = $this->name;
        return [
            'name' => 'required|min:2|max:20|unique:packages,name,'.$name.',name',
            'type' => '',
            'code' => '',
            'price' => '',
            'status' => ''
        ];
    }

    public function deleteRules()
    {
        return [
            'Package' => 'required|exists:packages,id'
        ];
    }

    public function categoryRules()
    {
        return [
            'package' => 'required|exists:packages,id',
            'category' => 'required|exists:categories,id',
            'type' => 'nullable|exists:food_types,id',
            'limit' => 'nullable|integer'
        ];
    }

    public function limitRules()
    {
        return [
            'package' => 'required|exists:packages,id',
            'category' => 'required|exists:categories,id',
            'type' => 'nullable|exists:food_types,id',
            'limit' => 'nullable|integer'
        ];
    }

    public function updateLimitRules()
    {
        return [
            'package' => 'required|exists:packages,id',
            'category' => 'required|exists:categories,id',
            'type' => 'nullable|exists:food_types,id',
            'limit' => 'nullable|integer'
        ];
    }

    public function deleteLimitRules()
    {
        return [
            'package' => 'required|exists:packages,id',
            'category' => 'required|exists:categories,id',
            'type' => 'nullable|exists:food_types,id'
        ];
    }

    public function assignFoodRules()
    {
        return [
            'food' => 'required|exists:foods,id'
        ];
    }

    public function deleteFoodFromPackageRules()
    {
        return [
            'food' => 'required|exists:foods,id',
            'package' => 'required|exists:packages,id'
        ];
    }

    public function generateCode()
    {
        $count = 1;
        $date = Carbon\Carbon::now();
        $constant = 'PKG-' . $date->format('y') . '-' . $date->format('m') . '-';
        $id = Package::orderBy('id', 'desc')->pluck('id')->first() + 1;

        if(!isset($id) || $id == null || $id == '') $id = 0;

        return $constant . $id;
    }

    public function categories()
    {
        return $this->belongsToMany('App\FoodCategory', 'package_limits', 'package_id', 'category_id')
                ->withPivot([ 'limit', 'type_id']);
    }

    public function foods()
    {
        return $this->belongsToMany('App\Food', 'package_food', 'package_id', 'food_id')
                    ->withTimestamps();;
    }
}
