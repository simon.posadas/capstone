<?php

use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([

        	[
        		'firstname' => 'Darryl',
        		'Lastname' => 'Milan',
        		'contact_number' => '09312731238',
        		'email' => 'afskh@aklfj.com',
        		'address' => 'Binangonan, Rizal',	
        	],

        	[
        		'firstname' => 'Emrech',
        		'Lastname' => 'Buban',
        		'contact_number' => '09123473821',
        		'email' => 'aslkdj@a;ksjf.com',
        		'address' => 'Marikina City',	
        	],

        	[
        		'firstname' => 'Simon',
        		'Lastname' => 'Posadas',
        		'contact_number' => '09124736281',
        		'email' => 'akjsfh@alksf.com',
        		'address' => 'Pampanga',     		
        	],

        	[
        		'firstname' => 'Gabriel jay',
        		'Lastname' => 'Huerte',
        		'contact_number' => '0912345782934',
        		'email' => 'asflk@gmail.com',
        		'address' => 'De Castro, Pasig',
        	]
        ]);
    }
}
