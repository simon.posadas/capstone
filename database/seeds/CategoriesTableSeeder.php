<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// categories table
        App\FoodCategory::insert([
        	[
        		'name' => 'Appetizer'
        	],

        	[
        		'name' => 'Soup'
        	],

        	[
        		'name' => 'Salad'
        	],

        	[
        		'name' => 'Main Entree'
        	],

        	[
        		'name' => 'Dessert'
        	],

        	[
        		'name' => 'Drinks'
        	]
        ]);
    }
}
