@extends('layouts.template')

@section('content')
<!--main-container-part-->
<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="icon-home"></i> Home</a>
                <a href="#" class="current">Queries</a>
        </div>
        <h1>Queries</h1>
    </div>
    <!--End-breadcrumbs-->
    <div class="container-fluid">
        <hr>


              <div class="container-tab-pill">
                  <ul class="nav nav-pills nav-justified">
                      <li class="active"><a class="address-color" href="#tab_a" data-toggle="pill">Reservation Status</a></li>
                      <li><a class="address-color" href="#tab_b" data-toggle="pill">Package</a></li>
                      <li><a class="address-color" href="#tab_c" data-toggle="pill">Foods</a></li>
                  </ul>
               <div class="tab-content">
                 <div class="tab-pane active" id="tab_a">
                     <div class="widget-box">
                      <div class="widget-title">
                         <h5>Reservation Status</h5>
                     </div>
                     <div class="row-fluid">
                     <div class="span12">
                        <div class="widget-content nopadding">

                                            <table class="table table-bordered data-table" id = "queries">
                                                <thead>
                                                    <tr>
                                                        <th style="width:75%;">Status</th>
                                                        <th style="width:25%;"># of Client</th>

                                                    </tr>
                                                </thead>

                                                <tbody>
                                                  @foreach($res as $res)
                                                    <tr>
                                                     <td><?php if ($res->Stat == 0): ?>
                                                             Cancelled
                                                         <?php elseif($res->Stat == 6): ?>
                                                             Completed
                                                     <?php endif; ?></td>
                                                    <td>{{$res->Count}}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>

                                            </table>

                        </div>
                    </div>
                  </div>
                 </div>
                 </div>
                 <div class="tab-pane" id="tab_b">
                    <div class="widget-box">
                     <div class="widget-title">
                         <h5>Most Picked Packages</h5>
                     </div>
                     <div class="row-fluid">
                     <div class="span12">
                             <div class="widget-content nopadding">
                                 <table class="table table-bordered data-table">
                                     <thead>
                                         <tr>
                                             <th style="width:75%;">Package Name</th>
                                             <th style="width:25%;"># of Times Ordered</th>
                                         </tr>
                                     </thead>

                                     <tbody>

                                          @foreach($pack as $pack)
                                            <tr>
                                            <td>{{$pack->name}}</td>
                                            <td>{{$pack->Count}}</td>
                                            </tr>
                                            @endforeach


                                     </tbody>

                                 </table>
                             </div>

                     </div>
                 </div>
                 </div>
                 </div>
                 <div class="tab-pane" id="tab_c">
                    <div class="widget-box">
                     <div class="widget-title">
                         <h5>Most Ordered Foods</h5>
                     </div>
                     <div class="row-fluid">
                     <div class="span12">
                             <div class="widget-content nopadding">
                                 <table class="table table-bordered data-table">
                                     <thead>
                                         <tr>
                                             <th style="width:75%;">Food Name</th>
                                             <th style="width:25%;"># of Times Ordered</th>
                                         </tr>
                                     </thead>

                                     <tbody>

                                          @foreach ($food as $food)
                                          <tr>
                                          <td>{{$food->name}}</td>
                                          <td>{{$food->Count}}</td>
                                          <tr>
                                          @endforeach

                                     </tbody>

                                 </table>
                             </div>

                     </div>
                 </div>
                 </div>
                 </div>
               </div> <!-- end of content -->
               </div> <!-- end of container -->
    </div>
</div>
<!--end-main-container-part-->

@endsection

@section('scripts_include')
<script type="text/javascript">

  $(document).ready(function(){

  $('#queries').dataTable({
  searching   :   false
  pagination : false
  });


});


</script>
@endsection
