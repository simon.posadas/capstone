<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     
        $this->call(UsersTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(FoodTypesTableSeeder::class);
        $this->call(PackagesTableSeeder::class);
        $this->call(EquipmentTypesTableSeeder::class);
        $this->call(EquipmentsTableSeeder::class);
        $this->call(FoodsTableSeeder::class);
        $this->call(EventTypesTableSeeder::class);
        $this->call(ServiceTypesSeeder::class);
        $this->call(ContractorTableSeeder::class);
        $this->call(CustomersTableSeeder::class);
        $this->call(PackageFoodTableSeeder::class);
        $this->call(FoodCategoriesTableSeeder::class);
        //$this->call(PackageLimitsSeeder::class);
        
    }
}
