@extends('layouts.sidebar')

@section('title', '5')

@section('content')
<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="fa fa-home"></i> Home</a>
            <a href="#" class="current">Refund</a>
        </div>
        <h1>Refund</h1>
        <hr>
    </div>
 	<div class="container-fluid"> 	
        <hr>
         <div class="pull-right">
             <button class="btn btn-info btn-large Add_Contractor">Create Refund Form</button>
        </div>
        <div class="pull-left">   
             <div class="input-group custom-search-form">
                <input type="text" name="searchFood" class="form-control" placeholder="Search" id="search_bar">
                <button class="btn btn-info" id="adjust-height-right" type="submit"><i class="fa fa-search"></i> Search</button>
             </div>
        </div>
    </div>
	<div class="row-fluid">
        <div class="colspan">
                <div class="widget-box">
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Refund ID(?)</th>
                                    <th>Refund Date</th>
                                    <th>Reservation ID</th>
                                    <th>Customer Name</th>
                                    <th>Total Price</th>
                                    <th>Paid Price</th>
                                    <th>Refund Price</th>
                                    <th>View</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>sample data</td>
                                    <td>sample data</td>
                                    <td>sample data</td>
                                    <td>sample data</td>
                                    <td>sample data</td>
                                    <td>sample data</td>
                                    <td>sample data</td>
                                    <td width="9%" class="btn-center">
                                        <button id ="sizes" class="btn btn-primary fa fa-eye-open view">View</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
    </div>
</div>
@endsection