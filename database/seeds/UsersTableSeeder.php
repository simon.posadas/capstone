<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::insert([
        	array(
	        	'username' => 'eli',
	        	'password' => Hash::make('12345678')
        	),
        	array(
	        	'username' => 'arich',
	        	'password' => Hash::make('12345678')
        	),
        	array(
	        	'username' => 'emrechXD',
	        	'password' => Hash::make('12345678')
        	),
        ]);
    }
}
