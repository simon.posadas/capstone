<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Model\ReservationDetail;
use App\Model\FoodDetail;
use App\Model\FoodType;
use App\Model\PackageDetail;
use App\Model\Worker;
use App\Model\WorkerRole;
use App\Http\Requests\Reservation\ReservationIdRequest;
use Notification;
use App\Notifications\ReservationApprove;
use App\Notifications\ReservationDisapprove;
use App\Http\Controllers\Controller;

/**
 * 
 */
class AdminController extends Controller {
    /**
     * View directory
     * @var type 
     */
    protected $view = 'admin.';

    /**
     * Populate dashboard page
     * @return type
     */
    public function dashboard() {
        $type = ReservationDetail::where('status', 0)
        ->join('customer_info', 'customer_info.cust_id', '=', 'reservation_details.cust_id')
        ->join('event_details', 'event_details.event_id', '=', 'reservation_details.event_id')
        ->get();

        return view('/dashboard', ['type' => $type]);
    }
    //END DASHBOARD


    /**
     * Approve the reservation of customer
     * 
     * @param ReservationIdRequest $reservationIdRequest
     * @return type
     */
    public function approve(ReservationIdRequest $reservationIdRequest) {
        $reservation_detail = ReservationDetail::find($reservationIdRequest->reserv_id);
        $customer_info = $reservation_detail->customer_info;
        
        $budget_food = $_POST["budget_food"];
        $budget_equip = $_POST["budget_equip"];
        $budget_work = $_POST["budget_work"];

        $total = $budget_food + $budget_equip + $budget_work;

        /**
         * mark the reservation as approve
         */
        if ($reservation_detail->update(['status' => 1, 'total_pay' => $total, 'bud_food' => $reservationIdRequest->budget_food, 'bud_equip' => $reservationIdRequest->budget_equip, 'bud_worker' => $reservationIdRequest->budget_work])) {
            /*Notification::send($reservation_detail->customer_info, new ReservationApprove($customer_info, $reservationIdRequest->reserv_id));*/
        alert()->success('Successfully approved a reservation. Please wait for the confirmation.', 'Success');
            return redirect()->back();
        }
        /**
         * approving of reservation failed
         */
        alert()->error('Something went wrong in approving the reservation', 'Failed...');
        return redirect()->back();
    }
    
    /**
     * Disapproves the reservation of customer
     * @param ReservationIdRequest $reservationIdRequest
     * @return type
     */
    public function disapprove(ReservationIdRequest $reservationIdRequest) {
        $reservation_detail = ReservationDetail::find($reservationIdRequest->reserv_id);
        $customer_info = $reservation_detail->customer_info;

        /**
         * mark the reservation as approve
         */
        if ($reservation_detail->update(['status' => 2, 'disapprove_reason' => $reservationIdRequest->disapprove_reason])) {
            /*Notification::send($reservation_detail->customer_info, new ReservationDisapprove($customer_info, $reservationIdRequest->reserv_id, $reservationIdRequest->disapprove_reason));*/
            alert()->success('Successfully disapproved the reservation', 'Success');
            return redirect()->back();
        }
        /**
         * approving of reservation failed
         */
        alert()->error('Something went wrong in disapproving the reservation', 'Failed...');
        return redirect()->back();
    }

    /**
     * Get reservation details
     * @param Request $req
     * @return type
     */
    public function getreserve(Request $req) {
        $reservation = ReservationDetail::find($req->id);
        $reservation->event_detail;
        $reservation->customer_info;
        $package_detail = $reservation->package_detail;
        $package_detail->package_type;
        return response()->json($reservation);
    }


    public function customer() {
        $type = DB::table('customer_info')->join('reservation_details', 'reservation_details.cust_id', '=', 'customer_info.cust_id')
        ->get();
        return view('/admin/customer',['type' => $type ]);
    }

    public function getcustomer(Request $req){
        $type = DB::table('customer_info')->where('cust_id',$req->id)->get();
        return response()->json($type);
    }
}
