@extends('layouts.template') 

@section('content')

@include('modal.service.type.add')
@include('modal.service.type.edit')
@include('modal.service.type.delete')

<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="icon icon-home"></i> Home</a>
            <a href="/admin/service">
                <i class="icon icon-briefcase"></i>Service</a>
            <a href="#" class="current">Type</a>
        </div>
        <h1>Service Types</h1>
    </div>
    <div class="container-fluid">
        <hr>

        @if(session()->has('success-message'))
        <div class="alert alert-success">
            <ul>
                {{ session()->get('success-message') }}
            </ul>
        </div>
        @endif


        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="float-left mb-2">
            <button class="btn btn-success btn-large icon-plus addServiceType" data-toggle="modal" data-target="#addModal"> Add Service Type</button>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-content nopadding">

                        <table class="table table-bordered data-table" id="serviceTypeTbl">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            @foreach($types as $type)
                            <tbody>
                                <tr>
                                    <td>{{ $type->id }}</td>
                                    <td>{{ $type->name }}</td>
                                    <td class="btn-center" width="30%">
                                        <button id ="sizes" type="button" class="btn icon-pencil btn-primary edittype" data-toggle="modal" data-target="#editModal" data-id="{{ $type->id }}" data-name="{{ $type->name }}"> Edit</button>
                                        <button id ="sizes" type="button" class="btn btn-danger icon-trash deletetype" data-toggle="modal" data-target="#deleteModal" data-name="{{ $type->name }}" data-id="{{ $type->id }}"> Delete</button>
                                    </td>
                                </tr>
                            </tbody>
                            @endforeach
                        </table>
                   </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection 

@section('after_scripts')

<script type="text/javascript">

    $(document).ready(function(){

        $('#serviceTypeTbl').on('click', '.edittype', function (event) {
            name = $(this).data('name')
            id = $(this).data('id')
            $('#editModal').find('input[name=name]').val(name);

            $('#editModal').find('#btn-edit').on('click', function () {

                $('#editForm').attr('action', function(i, value) {
                    return "{{ url('service/type') }}/" + id ;
                });

                $('#editForm').submit();
            });

        })

        $('#serviceTypeTbl').on('click', '.deletetype', function (event) {
            id = $(this).data('id')

            $('#deleteModal').find('#btn-delete').on('click', function () {

                $('#deleteForm').attr('action', function(i, value) {
                    return "{{ url('service/type') }}/" + id ;
                });

                $('#deleteForm').submit();
            });

        })
    })
</script>

@endsection   