<?php

use Illuminate\Database\Seeder;

class FoodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Food::insert([
        	[
                'code' => 'PST1801',
        		'name' => 'Country style cheesy Lasagna',
        		'details' => '',
        		'price' => '120',
        		'type_id' => '1',
        	],

            [
                'code' => 'PST1802',
                'name' => 'Seafood Fettuccine Alfredo',
                'details' => '',
                'price' => '120',
                'type_id' => '1',
            ],

            [
                'code' => 'PST1803',
                'name' => 'Classic Ham and Crispy Bacon Carbonarra',
                'details' => '',
                'price' => '120',
                'type_id' => '1',
            ],

            [
                'code' => 'PST1804',
                'name' => 'Baked Macaroni with Anchovies',
                'details' => '',
                'price' => '120',
                'type_id' => '1',
            ],

            [
                'code' => 'PST1805',
                'name' => 'Old Style Italian Sauce Spaghetti',
                'details' => '',
                'price' => '120',
                'type_id' => '1',
            ],

            [
                'code' => 'BEF1801',
                'name' => 'Ox Tail Kare-Kare',
                'details' => '',
                'price' => '140',
                'type_id' => '2',
            ],

            [
                'code' => 'BEF1802',
                'name' => 'Beef Stroganoff',
                'details' => '',
                'price' => '140',
                'type_id' => '2',
            ],

            [
                'code' => 'BEF1803',
                'name' => 'Holiday Beef with Olives',
                'details' => '',
                'price' => '140',
                'type_id' => '2',
            ],

            [
                'code' => 'BEF1804',
                'name' => 'Beef Caldereta',
                'details' => '',
                'price' => '140',
                'type_id' => '2',
            ],

            [
                'code' => 'BEF1805',
                'name' => 'Beef Mechado',
                'details' => '',
                'price' => '140',
                'type_id' => '2',
            ],

        	[
                'code' => 'PRK1801',
        		'name' => 'Pork Tenderlion Teriyaki',
        		'details' => '',
        		'price' => '120',
        		'type_id' => '7',
        	],

            [
                'code' => 'PRK1802',
                'name' => 'Pork Hamonado',
                'details' => '',
                'price' => '120',
                'type_id' => '7',
            ],

            [
                'code' => 'PRK1803',
                'name' => 'Barbeque Spareribs',
                'details' => '',
                'price' => '120',
                'type_id' => '7',
            ],

            [
                'code' => 'PRK1804',
                'name' => 'Pork Strips in Spicy Sauce',
                'details' => '',
                'price' => '120',
                'type_id' => '7',
            ],

            [
                'code' => 'PRK1805',
                'name' => 'Pork Korean Sesame',
                'details' => '',
                'price' => '120',
                'type_id' => '7',
            ],

        	[
                'code' => 'CHK1801',
        		'name' => 'Chicken Pastel',
        		'details' => '',
        		'price' => '100',
        		'type_id' => '3',
        	],

            [
                'code' => 'CHK1802',
                'name' => 'Chicken Courdon Bleu',
                'details' => '',
                'price' => '100',
                'type_id' => '3',
            ],

            [
                'code' => 'CHK1803',
                'name' => 'Baked Chicken Barbeque',
                'details' => '',
                'price' => '100',
                'type_id' => '3',
            ],

            [
                'code' => 'CHK1804',
                'name' => 'Chicken Hamonado',
                'details' => '',
                'price' => '100',
                'type_id' => '3',
            ],

            [
                'code' => 'CHK1805',
                'name' => 'Chicken Alexander',
                'details' => '',
                'price' => '100',
                'type_id' => '3',
            ],

            [
                'code' => 'CHK1806',
                'name' => 'Chicken Teriyaki',
                'details' => '',
                'price' => '100',
                'type_id' => '3',
            ],

        	[
                'code' => 'FSH1801',
        		'name' => 'Fish Fillet with Homemade Tartar sauce',
        		'details' => '',
        		'price' => '100',
        		'type_id' => '4',
        	],

            [
                'code' => 'FSH1802',
                'name' => 'Fish Fillet with Homemade Garlic sauce',
                'details' => '',
                'price' => '100',
                'type_id' => '4',
            ],

            [
                'code' => 'FSH1803',
                'name' => 'Grilled Fish with Lemon Butter Sauce',
                'details' => '',
                'price' => '100',
                'type_id' => '4',
            ],

            [
                'code' => 'FSH1804',
                'name' => 'Rellenong Bangus',
                'details' => '',
                'price' => '100',
                'type_id' => '4',
            ],

        	[
                'code' => 'VGT1801',
        		'name' => 'Buttered Vegetable with Kernel Corn and Mushroom',
        		'details' => '5',
        		'price' => '100',
        		'type_id' => '5',
        	],

            [
                'code' => 'VGT1802',
                'name' => 'Chapseuy',
                'details' => '5',
                'price' => '100',
                'type_id' => '5',
            ],

            [
                'code' => 'VGT1803',
                'name' => 'Gallacher Brown Creamy Potato',
                'details' => '5',
                'price' => '100',
                'type_id' => '5',
            ],

            [
                'code' => 'VGT1804',
                'name' => 'Mixed Vegetables',
                'details' => '5',
                'price' => '100',
                'type_id' => '5',
            ],

            [
                'code' => 'VGT1805',
                'name' => 'Lumpiang Ubod',
                'details' => '5',
                'price' => '100',
                'type_id' => '5',
            ],

        	[
                'code' => 'RCE1801',
        		'name' => 'Steamed Pandan Rice',
        		'details' => '',
        		'price' => '50',
        		'type_id' => '6',
        	],

            [
                'code' => '',
                'name' => 'Canapes (Ham, Cucumber, and Cheese)',
                'details' => '',
                'price' => '50',
                'type_id' => '6',
            ],

            [
                'code' => '',
                'name' => 'Nachos with Salsa Dip',
                'details' => '',
                'price' => '50',
                'type_id' => '6',
            ],

            [
                'code' => '',
                'name' => 'Pumpkin Soup',
                'details' => '',
                'price' => '50',
                'type_id' => '6',
            ],

            [
                'code' => '',
                'name' => 'Mushroom Soup',
                'details' => '',
                'price' => '50',
                'type_id' => '6',
            ],

            [
                'code' => '',
                'name' => 'Nido Soup',
                'details' => '',
                'price' => '50',
                'type_id' => '6',
            ],

            [
                'code' => '',
                'name' => ' Creamy Vegetable Soup',
                'details' => '',
                'price' => '50',
                'type_id' => '6',
            ],

            [
                'code' => '',
                'name' => 'Salad Bar garden salad (Iceberg Lettuce, Cucumber, Tomatoes, White Onions, Carrots, Kernel Corn) Dressings: Caesar and Thousand Island',
                'details' => '',
                'price' => '50',
                'type_id' => '6',
            ],

            [
                'code' => '',
                'name' => 'Waldorf Salad',
                'details' => '',
                'price' => '50',
                'type_id' => '6',
            ],

            [
                'code' => '',
                'name' => 'Hawaiian Maacaroni Salad',
                'details' => '',
                'price' => '50',
                'type_id' => '6',
            ],

            [
                'code' => '',
                'name' => 'Fresh fruits',
                'details' => '',
                'price' => '50',
                'type_id' => '6',
            ],

            [
                'code' => '',
                'name' => 'White Salad',
                'details' => '',
                'price' => '50',
                'type_id' => '6',
            ],

            [
                'code' => '',
                'name' => 'Buco Pandan',
                'details' => '',
                'price' => '50',
                'type_id' => '6',
            ],

            [
                'code' => '',
                'name' => 'Fruit Salad',
                'details' => '',
                'price' => '50',
                'type_id' => '6',
            ],

            [
                'code' => '',
                'name' => 'Leche Plan',
                'details' => '',
                'price' => '50',
                'type_id' => '6',
            ],

            [
                'code' => '',
                'name' => 'Brownies and Blondies',
                'details' => '',
                'price' => '50',
                'type_id' => '6',
            ],

            [
                'code' => '',
                'name' => 'Blue Lemonade',
                'details' => '',
                'price' => '50',
                'type_id' => '6',
            ],

            [
                'code' => '',
                'name' => 'Four Season',
                'details' => '',
                'price' => '50',
                'type_id' => '6',
            ],

            [
                'code' => '',
                'name' => 'Black Gulaman',
                'details' => '',
                'price' => '50',
                'type_id' => '6',
            ]

        ]);
    }
}
