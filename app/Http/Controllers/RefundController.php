<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Carbon;
use Validator;
use DB;

class RefundController extends Controller
{
    public function refund()
    {
        $reservations = App\Reservation::whereNotIn('status', [ '0', '1', '6'])->get();
        $fifthDayAfterCurrentDay = Carbon\Carbon::now() ->addDays(5)->format('Y-m-d');
        return view('collection.refund')
            ->with('reservations', $reservations)
            ->with('days2', $fifthDayAfterCurrentDay);
    }

    public function cancellationWithRefund(Request $request) {

        $receiver = $request->get('ror');
        $id = $request->get('id');
        $reservation = App\Reservation::find($id);
        $payment = App\Payment::where('reservation_id', $reservation->id)
            ->get();
        // dd($reservation->total);
        // dd($reservation->payments->sum('amount'));
        
        $refund = new App\Refund;
        $refund->code = $refund->generateCode();
        $refund->reservation_id = $reservation->id;
        $refund->received = ($reservation->payments->sum('amount') * 0.5) * -1;
        $refund->details = "Refund";
        $refund->name = $receiver;
        $refund->save();

        $reservation->status = 4;
        $reservation->save();

        alert()->success('Successfully cancelled the reservation with refund', 'Success')->persistent('Close');
        return redirect('collection/refund');
    }

    public function cancellationWithoutRefund(Request $request) 
    {

        $id = $request->get('id');
        $reservation =  App\Reservation::find($id);
        $reservation->status = 5;
        $reservation->save();

        alert()->success('Successfully cancelled the reservation', 'Success')->persistent('Close');
        return redirect('collection/refund');
    }

    public function cancel_charge(Request $request) 
    {
        $id = $request->get('id');
        $receiver = $request->get('ror');
        $reservation = App\Reservation::find($id);
        $amount = $reservation->payments->sum('amount') / 1.1;

        $reservation->status = 7;
        $reservation->save();

        $refund = new App\Refund;
        $refund->code = $refund->generateCode();
        $refund->reservation_id = $reservation->id;
        $refund->received = ($amount) * -1;
        $refund->details = "Refund";
        $refund->receiver = $receiver;
        $refund->save();

        alert()->success('Successfully cancelled the reservation with service charge', 'Success')->persistent('Close');
        return redirect('collection/refund');
    }

    public function refundform($id, $id2)
    {

        // dd($id, $id2);
        $reservation = app\Reservation::find($id);
        $customer = App\Person::find($id2);
        $package = App\Package::find($reservation->package_id);
        $food = DB::table('reservation_food')->where('reservation_id', '=', $id)->pluck('food_id');
        $food  = App\Food::whereIn('id', $food)->get();
        $payments = App\Payment::where('reservation_id', '=', $id)->sum('amount');

        // dd($reservation->event_date);
        // dd(Carbon\Carbon::parse($reservation->event_date)->toDayDateTimeString  ());

        

        $service_charge = $reservation->total;
        $percentage = $service_charge * 0.1 ;
        $grand_total = $service_charge * 1.1;
        $refund = $payments * 0.5;
        $total_refund = $refund * 0.5;

        $data = [
            'reservation' => $reservation,
            'payments' => $payments,
            'customer' => $customer,
            'packages' => $package,
            'payments' => $payments,
            'service_charge' => $service_charge,
            'percentage' => $percentage,
            'grand_total' => $grand_total,
            'refund' => $refund,
            'total_refund' => $total_refund,


        ];

        $filename = "Receipt-".\Carbon\Carbon::now()->format('mdYHm').".pdf";
        $view = "billing.print.refund";

        return view($view)
        ->with($data)
        ->with($filename);

    }

}
