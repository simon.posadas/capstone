<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventType extends Model
{
    protected $table = 'event_types';
    protected $primaryKey = 'id';
    public $timestamps = true;
    protected $fillable = [
    	'name',
    	'range'
    ];

    public function rules()
    {
    	return [
    		'name' => 'required|min:2|max:40|unique:event_types,name',
    		'range' => 'required|integer'
    	];
    }

    public function updateRules()
    {
    	$name = $this->name;
    	return [
    		'name' => 'required|min:2|max:40|unique:event_types,name,'.$name.',name',
    		'range' => 'required|integer'
    	];
    }

    public function deleteRules()
    {
        return [
            'id' => 'required|exists:event_types,id'
        ];
    }
}
