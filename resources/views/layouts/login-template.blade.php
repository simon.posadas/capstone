<!DOCTYPE html>
<html lang="eng">
	<head>

		<meta charset="UTF-8" />
    	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    	<meta name="csrf-token" content="{{ csrf_token() }}">
    	
		<title>{{ isset($title) ? $title : config('app.name') }}</title>
		@yield('styles')

		<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('css/bootstrap-responsive.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('css/matrix-login.css') }}" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
		<link href="{{ asset('css/sweetalert.css') }}" rel="stylesheet">
		@yield('styles_include')

		@yield('after_styles')
	</head>

	<body>
		@yield('content')

		<script src="{{ asset('js/jquery.min.js') }}"></script>
		<script src="{{ asset('js/matrix.login.js') }}"></script>
		<script src="{{ asset('js/sweetalert.min.js') }}"></script>

		@yield('scripts_include')
		@yield('script')
		@include('sweet::alert')
		

		@yield('after_scripts')

	</body>
</html>