@extends('layouts.template') 

@section('content')
<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="icon icon-home"></i> Home</a>
            <a href="/monitoring">Item Monitoring</a>
            <a href="#" class="current">Item List</a>
        </div>
        <h1>Reservation Items</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="pull-right">
            <a href=" {{ url("monitoring/items/tagging/$reservations->id") }}" class="btn btn-info btn-large">Tag Items</a>
        </div>
        <div class="pull-left">
            <div class="input-group custom-search-form">
                <label>Customer: {{ $reservations->customer->firstname }} {{ $reservations->customer->lastname }}</label>
                <input id="ReservationID" type="hidden" value="{{ $reservations['id'] }}" />
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="colspan">
            <div class="widget-box">
                <div class="widget-content nopadding">
                    <table class="table table-bordered data-table" id="reserveItem">
                        <thead>
                            <tr>
                                <th>Item ID</th>
                                <th style="width: 30%">Item Name</th>
                                <th>Quantity</th>
                                <th>Quantity Returned</th>
                                <th style="width: 10%;">Released</th>
                                <th style="width: 10%;">Returned</th>
                                <th style="width: 15%;">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($reservations->equipments as $items)
                                <tr>
                                    <td>{{ $items->id }}</td>
                                    <td>{{ $items->name }}</td>
                                    <td style="text-align:right;">{{ $items->pivot->quantity }}</td>
                                    <td style="text-align:right;">{{ $items->pivot->quantity_returned }}</td>
                                    @if ($items->pivot->release == 0)
                                    <td bgcolor="#ffb3b3" style="text-align: center;">X</td>
                                    @else
                                    <td bgcolor="#99ff99" style="text-align: center;"><i class="icon icon-check"></td>
                                    @endif
                                    @if ($items->pivot->returned == 0)
                                    <td bgcolor="#ffb3b3" style="text-align: center;">X</td>
                                    @else
                                    <td bgcolor="#99ff99" style="text-align: center;"><i class="icon icon-check"></td>
                                    @endif
                                    <td style="text-align: center;">
                                        @if($items->pivot->release == 0)
                                        <button class="btn btn-info releaseItem" type="button">
                                            <i class="icon icon-truck"></i>
                                            <span>Release</span>
                                        </button>
                                        @else
                                        <button class="btn btn-info releaseItem" type="button" disabled>
                                            <i class="icon icon-truck"></i>
                                            <span>Release</span>
                                        </button>
                                        @endif

                                        @if($items->pivot->release == 1 and $items->pivot->returned == 0)
                                            <button class="btn btn-inverse returnItem" type="button"
                                            data-id="{{ $items->id }}"
                                            data-name="{{ $items->name }}"
                                            data-price="{{ $items->price }}"
                                            data-quantity="{{ $items->pivot->quantity }}">
                                                <i class="icon icon-retweet"></i>
                                                <span>Return</span>
                                            </button>
                                        @else
                                        <button class="btn btn-inverse returnItem" type="button" disabled>
                                            <i class="icon icon-retweet"></i>
                                            <span>Return</span>
                                        </button>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
<div class="modal fade modal-size-s1" tabindex="-1" role="dialog" id="returnModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Returned Item</h4>
            </div>
            <div class="modal-body">
                <input type="text" name="id" id="id">
                <div class="form-group">
                    <label class="input-margin2">Name : <span id="name"></span></label>
                </div>
                <div class="form-group">
                    <label class="label-margin1">Quantity Returned:</label>
                    <input id="quantity" type="number" class="form-control inquan" name="quantity">
                </div>
                <div class="form-group div-margin4">
                    <label class="label-margin1">Price per Piece : &#8369; <span id="price"></span></label>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary confirmReturnItem" id="btn-edit">Confirm Return</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts_include')
    <script type="text/javascript">
        $('.releaseItem').click(function () {
            var $row = $(this).closest("tr");
            ItemID = $row.find("td:nth-child(1)");

            var data = {
                reservation_id: $('#ReservationID').val(),
                item_id: ItemID.text()
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: '/monitoring/items/release',
                data: JSON.stringify(data),
                success: function (result) {
                    if (result.status) {
                        location.reload();
                    } else {
                        alert('Something went wrong, please try again.');
                        console.log(result.error);
                    }
                },
                error: function (jqXHR, exception) {
                    var msg = '';
                    if (jqXHR.status === 0) {
                        msg = 'Not connect.\n Verify Network.';
                    } else if (jqXHR.status == 404) {
                        msg = 'Requested page not found. [404]';
                    } else if (jqXHR.status == 500) {
                        msg = 'Internal Server Error [500].';
                    } else if (exception === 'parsererror') {
                        msg = 'Requested JSON parse failed.';
                    } else if (exception === 'timeout') {
                        msg = 'Time out error.';
                    } else if (exception === 'abort') {
                        msg = 'Ajax request aborted.';
                    } else {
                        msg = 'Uncaught Error.\n' + jqXHR.responseText;
                    }
                    alert(msg);
                }
            });
        });



    $('.returnItem').click(function () {
        id = $(this).data('id')
        name = $(this).data('name')
        price = $(this).data('price')
        quantity = $(this).data('quantity')
            
        $('#returnModal').find('#id').val(id);
        $('#returnModal').find('#name').text(name);
        $('#returnModal').find('#price').text(price);
        $('#returnModal').find('.inquan').val(quantity);

        $('#returnModal').modal('show');
    });

        $('.confirmReturnItem').click(function (){
            ItemID = $('#returnModal').find('#id').val();
            quantity = $('#returnModal').find('.inquan').val();

            var data = {
                reservation_id: $('#ReservationID').val(),
                item_id: ItemID,
                quantity: quantity
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: '/monitoring/items/return',
                data: JSON.stringify(data),
                success: function (result) {
                    if (result.status) {
                        location.reload();
                    } else {
                        location.reload();
                    }
                },
                error: function (jqXHR, exception) {
                    var msg = '';
                    if (jqXHR.status === 0) {
                        msg = 'Not connect.\n Verify Network.';
                    } else if (jqXHR.status == 404) {
                        msg = 'Requested page not found. [404]';
                    } else if (jqXHR.status == 500) {
                        msg = 'Internal Server Error [500].';
                    } else if (exception === 'parsererror') {
                        msg = 'Requested JSON parse failed.';
                    } else if (exception === 'timeout') {
                        msg = 'Time out error.';
                    } else if (exception === 'abort') {
                        msg = 'Ajax request aborted.';
                    } else {
                        msg = 'Uncaught Error.\n' + jqXHR.responseText;
                    }
                    alert(msg);
                }
            });
        });

        $(document).ready(function(){
            $('#reserveItem').dataTable({
                searching   :   false
            });
        });
    </script>
@endsection
