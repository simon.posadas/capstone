<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use DB;
use Validator;

class EventTypesController extends Controller
{
    public function index(Request $request)
    {
        $types = App\EventType::orderBy('name')
            ->get();
        
    	return view('event.type.index')
    			->with('types', $types);
    }

    public function create(Request $request)
    {
        // return view('event.type.create');
    }

    public function store(Request $request)
    {
        $name = $request->get('name');
        $range = $request->get('range');

        $type = new App\EventType;

        $validator = Validator::make([
            'name' => $name,
            'range' => $range
        ], $type->rules());

        if($validator->fails())
        {
            return back()->withInput()->withErrors($validator);
        }

        $type->name = $name;
        $type->range = $range;
        $type->save();

        $request->session()->flash('success-message', 'Event Type Added');
        return redirect('event/type');
    }

    public function edit(Request $request, $id)
    {

    }

    public function update(Request $request, $id)
    {
        $name = $request->get('name');
        $range = $request->get('range');

        $type = App\EventType::find($id);

        $validator = Validator::make([
            'name' => $name,
            'range' => $range
        ], $type->updateRules());

        if($validator->fails())
        {
            return back()->withInput()->withErrors($validator);
        }

        $type->name = $name;
        $type->range = $range;
        $type->save();

        $request->session()->flash('success-message', 'Event Type Updated');
        return redirect('event/type');

    }

    public function destroy(Request $request, $id)
    {
        $type = App\EventType::find($id);

        $validator = Validator::make([
            'id' => $id
        ], $type->deleteRules());

        if($validator->fails())
        {
            return back()->withInput()->withErrors($validator);
        }

        $type->delete();

        $request->session()->flash('success-message', 'Event Type Deleted');
        return redirect('event/type');
    }
}



