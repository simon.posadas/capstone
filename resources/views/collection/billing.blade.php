@extends('layouts.template') 

@section('content')

<!--main-container-part-->
<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="icon icon-home"></i> Home</a>
            <a href="/collection">Collection</a>
            <a href="#" class="current">Billing</a>
        </div>
        <h1>Billings</h1>
    </div>
    <!--End-breadcrumbs-->
    <div class="container-fluid">
        <hr>
        
        @if(session()->has('success-message'))
        <div class="alert alert-success">
            <ul>
                {{ session()->get('success-message') }}
            </ul>
        </div>
        @endif


        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <i class="fa fa-th fa-sm pt-1"></i>
                        <h5>Billing</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Code</th>
                                    <th>description</th>
                                    <th>Amount</th>
                                    <th>Billing Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($billings as $billing)
                                <tr>
                                    <td>{{ $billing->id }}</td>
                                    <td>{{ $billing->code }}</td>
                                    <td>{{ $billing->description }}</td>
                                    <td>{{ number_format($billing->amount, 2) }}</td>
                                    <td>{{ Carbon\Carbon::parse($billing->created_at)->toDayDateTimeString  () }}</td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan=5>Empty</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end-main-container-part-->

@endsection 

@section('scripts_include')

<script type="text/javascript">

</script>

@endsection