@extends('layouts.sidebar') 

@section('title', 'Food per Package')

@section('content')

<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="fa fa-home"></i> Home</a>
                <a href="/admin/packages">Packages</a>
            <a href="#" class="current">Food for Package</a>
        </div>
        <h1>Food</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="pull-left">
            <button class="btn btn-success btn-large fa fa-plus addMain" data-id="{{ $pid }}"> Add Main Entree</button>
        </div>
        <div class="row-fluid">
            <div class="colspan">
                <div class="widget-box">
                    <div class="widget-title">
                            <span class="fa fa">
                                <i class="fa fa-th mt-1 mt-1"></i>
                            </span>
                            <h5>Main Entree</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table" id="main_table">
                            <thead>
                                <tr>
                                    <th>Package ID</th>
                                    <th>Package Name</th>
                                    <th>Food Name</th>
                                </tr>
                            </thead>
                            @foreach($mains as $main)
                            <tbody>
                                <tr>
                                    <td>{{$main->pid}}</td>
                                    <td>{{$main->pname}}</td>
                                    <td>{{$main->name}}</td>
                                </tr>
                            </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <hr>
        <div class="pull-left">
            <button class="btn btn-success btn-large fa fa-plus addSoup" data-id="{{ $pid }}"> Add Soup</button>
        </div>
        <div class="row-fluid">
            <div class="colspan">
                <div class="widget-box">
                <div class="widget-title">
                            <span class="fa fa">
                                <i class="fa fa-th mt-1"></i>
                            </span>
                            <h5>Soup</h5>
                        </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Package Name</th>
                                    <th>Food Name</th>
                                </tr>
                            </thead>
                            @foreach($soups as $soup)
                            <tbody>
                                <tr>
                                    <td>{{$soup->pname}}</td>
                                    <td>{{$soup->name}}</td>
                                </tr>
                            </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <hr>
        <div class="pull-left">
            <button class="btn btn-success btn-large fa fa-plus addAppetizer" data-id="{{ $pid }}"> Add Appetizer</button>
        </div>
        <div class="row-fluid">
            <div class="colspan">
                <div class="widget-box">
                <div class="widget-title">
                            <span class="fa fa">
                                <i class="fa fa-th mt-1"></i>
                            </span>
                            <h5>Appetizers</h5>
                        </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Package Name</th>
                                    <th>Food Name</th>
                                </tr>
                            </thead>
                            @foreach($appetizers as $appetizer)
                            <tbody>
                                <tr>
                                    <td>{{$appetizer->pname}}</td>
                                    <td>{{$appetizer->name}}</td>
                                </tr>
                            </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <hr>
        <div class="pull-left">
            <button class="btn btn-success btn-large fa fa-plus addSalad" data-id="{{ $pid }}"> Add Salad</button>
        </div>
        <div class="row-fluid">
            <div class="colspan">
                <div class="widget-box">
                <div class="widget-title">
                            <span class="fa fa">
                                <i class="fa fa-th mt-1"></i>
                            </span>
                            <h5>Salad</h5>
                        </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Package Name</th>
                                    <th>Food Name</th>
                                </tr>
                            </thead>
                            @foreach($salads as $salad)
                            <tbody>
                                <tr>
                                    <td>{{$salad->pname}}</td>
                                    <td>{{$salad->name}}</td>
                                </tr>
                            </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('modal')
<!-- add main -->
<div class="modal fade" tabindex="-1" role="dialog" id="addMains">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add Food</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/addMain">
                    {{csrf_field()}}
                    <div class="control-group">
                        <label class="control-label">Select food to add</label>
                        <div class="controls select2-container">
                            <input type="hidden" class="id" name="id" id="packID">
                            <select name="food_main">
                                <option disabled>Select food</option>
                                @foreach($mains2 as $main)
                                <option value="{{$main->food_id}}">{{$main->food_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Add</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end add main -->

<!-- add soup -->
<div class="modal fade" tabindex="-1" role="dialog" id="addSoup">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add Food</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/addSoup">
                    {{csrf_field()}}
                    <div class="control-group">
                        <label class="control-label">Select food to add</label>
                        <div class="controls select2-container">
                            <input type="hidden" class="id" name="id" id="soup_packID">
                            <select name="food_soup">
                                <option disabled>Select food</option>
                                @foreach($soups2 as $soup)
                                <option value="{{$soup->food_id}}">{{$soup->food_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Add</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end add soup -->

<!-- add appetizer -->
<div class="modal fade" tabindex="-1" role="dialog" id="addApp">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add Food</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/addApp">
                    {{csrf_field()}}
                    <div class="control-group">
                        <label class="control-label">Select food to add</label>
                        <div class="controls select2-container">
                            <input type="hidden" class="id" name="id" id="app_packID">
                            <select name="food_app">
                                <option disabled>Select food</option>
                                @foreach($appetizers2 as $appetizer)
                                <option value="{{$appetizer->food_id}}">{{$appetizer->food_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Add</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end add appetizer -->

<!-- add salad -->
<div class="modal fade" tabindex="-1" role="dialog" id="addSalad">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add Food</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/addSalad">
                    {{csrf_field()}}
                    <div class="control-group">
                        <label class="control-label">Select food to add</label>
                        <div class="controls select2-container">
                            <input type="hidden" class="id" name="id" id="salad_packID">
                            <select name="food_salad">
                                <option disabled>Select food</option>
                                @foreach($salads2 as $salad)
                                <option value="{{$salad->food_id}}">{{$salad->food_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Add</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end add salad -->
@endsection

@section('script')
<script type="text/javascript">

        $('.addMain').click(function () {
            $('#packID').val($(this).data('id'));
            $('#addMains').modal('show');
        });

        $('.addSoup').click(function () {
            $('#soup_packID').val($(this).data('id'));
            $('#addSoup').modal('show');
        });

        $('.addAppetizer').click(function () {
            $('#app_packID').val($(this).data('id'));
            $('#addApp').modal('show');
        });

        $('.addSalad').click(function () {
            $('#salad_packID').val($(this).data('id'));
            $('#addSalad').modal('show');
        });
</script>
@endsection