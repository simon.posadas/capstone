@extends('layouts.template') 

@section('content')

@include('modal.food.type.add')
@include('modal.food.type.edit')
@include('modal.food.type.delete')

<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="icon icon-home"></i> Home</a>
            <a href="{{ url('food') }}">Food</a>
            <a href="#" class="current">Type</a>
        </div>
        <h1>Food Types</h1>
    </div>
    <div class="container-fluid">
        <hr>

        @if(session()->has('success-message'))
        <div class="alert alert-success">
            <ul>
                {{ session()->get('success-message') }}
            </ul>
        </div>
        @endif


        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="pull-left">
            <button class="btn btn-success btn-large icon-plus addFoodType" data-toggle="modal" data-target="#addModal"> Add Food Type</button>
        </div>

        <div class="pull-right">
            <button class="btn btn-info btn-large" onClick="goBack()">Back</button>
        </div>

        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-content nopadding">

                        <table class="table table-bordered data-table" id="foodTypeTbl">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($types as $type)
                                <tr>
                                    <td>{{ $type->id }}</td>
                                    <td>{{ $type->name }}</td>
                                    <td class="btn-center">
                                        <button id ="sizes" type="button" class="btn icon-pencil btn-primary edittype" data-toggle="modal" data-target="#editModal" data-id="{{ $type->id }}" data-name="{{ $type->name }}"> Edit</button>
                                        <button id ="sizes"type="button" class="btn btn-danger icon-trash deletetype" data-toggle="modal" data-target="#deleteModal" data-name="{{ $type->name }}" data-id="{{ $type->id }}"> Delete</button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                   </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection 

@section('after_scripts')

<script type="text/javascript">

    $(document).ready(function(){

        $('#foodTypeTbl').on('click', '.edittype', function (event) {
            name = $(this).data('name')
            id = $(this).data('id')
            $('#editModal').find('input[name=name]').val(name);

            $('#editModal').find('#btn-edit').on('click', function () {

                $('#editForm').attr('action', function(i, value) {
                    return "{{ url('food/type') }}/" + id ;
                });

                $('#editForm').submit();
            });

        })

        $('#foodTypeTbl').on('click', '.deletetype', function (event) {
            id = $(this).data('id')

            $('#deleteModal').find('#btn-delete').on('click', function () {

                $('#deleteForm').attr('action', function(i, value) {
                    return "{{ url('food/type') }}/" + id ;
                });

                $('#deleteForm').submit();
            });

        })
    })

    function goBack(){
        window.location.href = "/food";
    }

    $('#foodTypeTbl').dataTable({
        searching   :   false
    });
</script>

@endsection   