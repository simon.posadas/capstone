<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            create view package_lists_v
            as
            select 
            packages.name as package_name,
            packages.id as package_id,
            packages.type as package_type,
            packages.price as package_price,
            packages.code as package_code,
            foods.id as food_id,
            foods.code as food_code,
            foods.name as food_name,
            foods.details as food_details,
            food_types.id as type_id,
            food_types.name as food_type
            from packages
            join package_food
            on packages.id = package_food.package_id
            join foods
            on foods.id = package_food.food_id
            join food_types
            on food_types.id = foods.type_id

        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("drop view package_lists_v");
    }
}
