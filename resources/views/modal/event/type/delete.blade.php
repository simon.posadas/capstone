<!-- delete modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="deleteModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Delete</h4>
            </div>
            <form method="post" action="{{ url('event/type') }}" id="deleteForm">
                <div class="modal-body">
                        <input type="hidden" name="_method" value="DELETE" />
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <input type="hidden" class="id" name="id" id="deleteID">
                        <div class="form-group">
                            <h4>Delete event type?</h4>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btn-delete">Confirm</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end delete modal -->