@extends('layouts.sidebar')

@section('title', '4')

@section('content')
<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="fa fa-home"></i> Home</a>
            <a href="#" class="current">Contractor Monitoring</a>
        </div>
        <h1>Contractor Monitoring</h1>
    </div>
    <div class="container-fluid"> 	
        <hr>
         <div class="pull-right">
             <button class="btn btn-info btn-large Add_Contractor">Add Contractor</button>
        </div>
        <div class="pull-right">
             <button id="rightt" class="btn btn-info btn-large Tag_Contractor">Tag Contractor</button>
        </div>
        <div class="pull-left">   
             <div class="input-group custom-search-form">
                <input type="text" name="searchFood" class="form-control" placeholder="Search" id="search_bar">
                <button class="btn btn-info" id="adjust-height-right" type="submit"><i class="fa fa-search"></i> Search</button>
             </div>
        </div>
      </div>
        <div class="row-fluid">
            <div class="colspan">
                <div class="widget-box">
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Contractor Name</th>
                                    <th>Contact No.</th>
                                    <th>Status</th>
                                    <th>Action</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td>sample data</td>
                                    <td>sample data</td>
                                    <td width="30%" class="btn-center">
                                        <button id ="sizes" class="btn btn-primary icon-eye-open Edit">Edit</button>
                                        <button id ="sizes" class="btn btn-primary icon-eye-open Delete">Delete</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

 @section('modal')
    <!-- view modal -->
    <div class="modal fade modal-size2" tabindex="-1" role="dialog" id="viewModal">
        <div class="modal-dialog modal-size2" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Tag Contractor</h4>
                </div>
                <div class="modal-body">
                    <h4>Contractor List</h4> 
                    <table class="table table-bordered data-table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Contact No</th>
                                <th></th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>sample data</td>
                                <td>sample data</td>
                                <td class="btn-center">
                                    <input type="checkbox">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                	<button type="button" class="btn btn-success" data-dismiss="modal">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection 

@section('script')
    <script type="text/javascript">
        $('.Tag_Contractor').click(function () {
            $('#viewModal').modal('show');
        });
    </script>

@endsection 