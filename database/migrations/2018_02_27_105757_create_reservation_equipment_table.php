<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationEquipmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation_equipment', function (Blueprint $table) {
            $table->integer('reservation_id')->unsinged();
            $table->integer('equipment_id')->unsigned();
            $table->integer('quantity');
            $table->integer('reserved');
            $table->integer('release');
            $table->integer('returned');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservation_equipment');
    }
}
