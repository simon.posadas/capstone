@extends('layouts.template')

@section('content')

<!--main-container-part-->
<div id="content">

    @include('modal.collection.pay')

    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="icon icon-home"></i> Home</a>
            <a href="#" class="current">Collection</a>
        </div>
        <h1>Collections</h1>
    </div>
    <!--End-breadcrumbs-->
    <div class="container-fluid">
        <hr>

        @if(session()->has('success-message'))
        <div class="alert alert-success">
            <ul>
                {{ session()->get('success-message') }}
            </ul>
        </div>
        @endif


        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <i class="fa fa-th fa-sm pt-1"></i>
                        <h5>Collection</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table" id="collectionsTable">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Code</th>
                                    <th>Total Balance</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            @foreach($reservations as $reservation)
                            <tbody>
                                <tr>
                                    <td>{{ $reservation->id }}</td>
                                    <td>{{ $reservation->code }}</td>
                                    <td style="text-align: right">&#8369; {{ number_format($reservation->total,2)}} + &#8369; {{ number_format(($reservation->billings->sum('amount') - $reservation->total),2) }}</td>
                                    <td>{{ $reservation->status_name }}</td>
                                   <td class="btn-center">

                                    @if($reservation->status == 0)

                                        <button type="button" data-id="{{ $reservation->id }}" data-amount="{{ $reservation->total }}" class="btn btn-primary show-payment-modal" data-toggle="modal" data-target="#reservationPaymentModal"> Pay</button></br>

                                    @elseif($reservation->status != 1)

                                        <form method="get" action="{{ url("collection/$reservation->id/billing") }}">
                                            <button type="submit" name="pay" value="{{ csrf_token() }}" id ="sizes" class="btn btn-primary fa fa-trash">&nbsp;Billing</button>
                                        </form>

                                        <form method="get" action="{{ url("collection/$reservation->id/payment") }}">
                                            <button type="submit" name="pay" value="{{ csrf_token() }}" id ="sizes" class="btn btn-success fa fa-trash">&nbsp;Payment</button>
                                        </form>
                                        <!-- <button id="print" onclick="print()">Print</button> -->

                                    @endif

                                    </td>
                                </tr>
                            </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end-main-container-part-->

@endsection

@section('script')

<script type="text/javascript">

$(document).ready(function(){



        $('#collectionsTable').on('click', '.show-payment-modal', function (event) {
            id = $(this).data('id')
            amount = $(this).data('amount')

            $('#reservationPaymentModal').find('#reservation-id').val(id);
            $('#reservationPaymentModal').find('#total-amount').text(amount);
            $('#reservationPaymentModal').find('#half-amount').text(amount*0.5);

            $('#reservationPaymentModal').find('#payment-confirm').on('click', function () {

                var id = $('#reservation-id').val()
                $('#payReservationForm').attr('action', function(i, value) {
                    return "{{ url('reservation') }}/" + id + '/pay' ;
                });

                $('#payment-confirm').submit();
            });

        });
    });


</script>
@endsection
