<!-- edit modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="editModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edit Food</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="#" id="editForm">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="_method" value="PUT" />
                    <div class="control-group">
                        <label class="control-label">Select food type</label>
                        <div class="controls select2-container">
                            <select name="type" id="type" disabled>
                                @foreach($types as $type)
                                <option value="{{ $type->id }}">{{ $type->name }}</option>
                                @endforeach
                            </select>

                            <input type="hidden" name="type" id="type" class="type" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Select food category</label>
                        <div class="controls select2-container">
                            <select name="category" id="category" disabled>
                                @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                            <input type="hidden" name="category" id="category" class="category" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="label-limit">Limit :</label>
                        <input id="input-limit" type="number" class="form-control" placeholder="Limit" name="limit">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn-edit">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- end edit modal -->
