<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon;

class Payment extends Model
{
    /**
     * Table to connect
     * @var type 
     */
    protected $table = 'payments';

    protected $primaryKey = 'id';
   
    /**
     * Remove automatic timestamp
     * @var type 
     */
    public $timestamps = true;

    protected $fillable = [
        'code',
        'created_at',
        'amount',
        'details',
        'reservation_id'  
    ];


    /**
     * filter all the payments from the clients
     * to use this function you need to include it in eloquent
     * model of payments
     * e.g. App\Payment::scopeFilterAllReceivedPayments
     * this will return list of all amount
     */
    public function scopeFilterAllReceivedPayments($query)
    {   
        $query->where('amount', '>', 0);
    }

    /**
     * filter all the payments returned to the user
     * commonly called as refunds
     * to use this function you need to include it in eloquent
     * model of payments
     * e.g. App\Payment::scopeFilterAllReturnedPayments
     * this will return list of all returned amount
     * Note: convert the amount from negative to positive
     */
    public function scopeFilterAllReturnedPayments($query)
    {
        $query->where('amount', '<', 0);
    }

    public function generateCode()
    {
        $count = 1;
        $date = Carbon\Carbon::now();
        $constant = $date->format('y') . '-' . $date->format('m') . '-';
        $id = Payment::orderBy('id', 'desc')->pluck('id')->first() + 1;

        if(!isset($id) || $id == null || $id == '') $id = 0;

        return $constant . $id;
    }

    public function getPayeeAttribute($value)
    {
        if($value == null || $value == '') 
        {
            return 'None';
        }

        return $value;
    }
}
