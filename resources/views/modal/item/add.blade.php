<!-- add modal -->
<div class="modal fade modal-size-s3" tabindex="-1" role="dialog" id="addModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add Item</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/item/add">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="form-group div-margin1">
                        <label class="label-addItem">Name :</label>
                        <input id="input-addItemName" type="text" class="form-control" placeholder="Item Name" name="name" required>
                    </div>
                    <div class="form-group div-margin1">
                        <label class="label-addItem1">Quantity :</label>
                        <input id="input-addItemQuantity" type="number" class="form-control" placeholder="Quantity" name="quantity" required>
                    </div>
                    <div class="form-group div-margin1">
                        <label class="label-addItem1">Price per Piece :</label>
                        <input id="input-addItemPrice" type="number" class="form-control" placeholder="Price" name="price" required>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Add</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end add modal -->