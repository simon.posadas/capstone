<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon;

class Refund extends Model
{
    protected $table = 'refunds';
    /**
     * Uniq identifier
     * @var type 
     */
    protected $primaryKey = 'id';
    
    /**
     * Remove automatic timestamp
     * @var type 
     */
    public $timestamps = true;
    
    /**
     * Variable mass assignement
     * @var type 
     */
    protected $fillable = [
        'name', 
        'date', 
        'received',
        'reservation_id',
        'details'
    ];

    public function generateCode()
    {
        $count = 1;
        $date = Carbon\Carbon::now();
        $constant = $date->format('y') . '-' . $date->format('m') . '-';
        $id = Payment::orderBy('id', 'desc')->pluck('id')->first() + 1;

        if(!isset($id) || $id == null || $id == '') $id = 0;

        return $constant . $id;
    }
}
