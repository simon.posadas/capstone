<!-- add modal -->
<div class="modal fade modal-size-s3" tabindex="-1" role="dialog" id="addModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add Food</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="{{ url('food') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="form-group div-margin1">
                        <label>Food Name :</label>
                        <input id ="input-addFoodName" type="text" class="form-control" placeholder="Food Name" name="name" required>
                    </div>
                    <div class="control-group row-flex div-margin3">
                        <label class="control-label label-selectFoodCat">Select food category :</label>
                        <div class="controls select2-container select2-size">
                            <select class="select-category" name="category" id="category" onchange="enableCat()">    
                                @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="control-group row-flex div-margin2">
                        <label class="control-label label-selectFood">Select food type :</label>
                        <div class="controls select2-container select2-size">
                            <select class="select-type" name="type" id="type">
                                @foreach($types as $type)
                                <option value="{{ $type->id }}">{{ $type->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group div-margin4">
                        <label>Price :</label>
                        <input id ="input-addFoodPrice2" type="text" class="form-control" placeholder="Price" name="price" required>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Add</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end add modal -->