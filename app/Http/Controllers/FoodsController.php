<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use DB;
use Validator;

class FoodsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $foods = App\Food::orderBy('name')
            ->get();
        $types = App\FoodType::all();
        $categories = App\FoodCategory::all();

        //return $categories;

        return view('food.index')
                ->with('title', 'Foods')
                ->with('foods', $foods)
                ->with('types', $types)
                ->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = $request->get('name');
        $price = $request->get('price');
        $type_id = $request->get('type');
        $category_id = $request->get('category');


        $food = new App\Food;

        $validator = Validator::make([
            'name' => $name,
            'price' => $price,
            'type' => $type_id,
            'category' => $category_id
        ], $food->rules());

        if($validator->fails())
        {
            return back()->withInput()->withErrors($validator);
        }

        DB::beginTransaction();

        $food->code = $food->generateCode();
        $food->name = $name;
        $food->price = $price;
        $food->type_id = $type_id;
        $food->save();

        $food->categories()->attach($category_id);

        DB::commit();

        $request->session()->flash('success-message', 'Food Created');
        return redirect('food');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name = $request->get('name');
        $price = $request->get('price');
        $type_id = $request->get('type');

        $food = App\Food::find($id);

        $validator = Validator::make([
            'name' => $name,
            'price' => $price,
            'type' => $type_id
        ], $food->updateRules());

        if($validator->fails())
        {
            return back()->withInput()->withErrors($validator);
        }

        DB::beginTransaction();

        $food->name = $name;
        $food->price = $price;
        $food->type_id = $type_id;
        $food->save();

        DB::commit();

        $request->session()->flash('success-message', 'Food Updated');
        return redirect('food');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $food = App\Food::find($id);

        if(count($food) <= 0) $food = new App\Food;

        $validator = Validator::make([
            'Food' => $id 
        ], $food->deleteRules());

        if($validator->fails())
        {
            return back()->withErrors($validator);
        }

        $food = App\Food::find($id);
        $food->delete();

        $request->session()->flash('success-message', 'Food Removed');
        return redirect('food');
    }
}
