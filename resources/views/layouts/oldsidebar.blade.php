

<!--sidebar-menu-->
<div id="sidebar">
    <!-- <a href="#" class="visible-phone">
        <i class="fa fa-home"></i> Dashboard
    </a> -->
    <ul class="widthz">
        <div class="panel panel-default">
            <div class="panel-heading">
            <div class="panel-title" style="height:60px;">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" role="tab">
                    <i class="fa fa-th mr-1"></i>
                    <span>Reservation</span>
                    <span style="margin-right:120px;"></span>
                    <i class="fa fa-chevron-down"></i>
                </a>
            </div>
            </div>
            <div id="collapseOne" class="panel-collapse collapse inactive">
            <div class="panel-body">
                <li>
                    <a href="/admin/reservation">Reservation Management</a>
                </li>
                <li>
                    <a href="/admin/downpayment">Downpayment</a>
                </li>
                <li>
                    <a href="/admin/cancelled">Cancelled Reservation</a>
                </li>
                <li>
                    <a href="/admin/refund">Refund</a>
                </li>
                <li>
                    <a href="/admin/equipment">Collections</a>
                </li>
            </div>
            </div>
        </div>

        <li>
            <a href="/admin/1">
                <i class="fa fa-table"></i>
                <span>Billing</span>
            </a>
        </li>
        <!-- blank pages -->
        <li>
            <a href="/admin/4">
                <i class="fa fa-table"></i>
                <span>Contractor Monitoring</span>
            </a>
        </li>
        
        <!-- end blank pages -->
        <li>

            <a href="/admin/wip">

                <i class="icon icon-group"></i>
                <span>BudgetForm WIP : Emrech</span>

                <i class="fa fa-group"></i>
                <span>WIP</span>

            </a>
        </li>
        <li>
            <a href="/admin/budget">
                <i class="icon icon-group"></i>
                <span>BudgetForm2 WIP : Emrech</span>

            <a href="/admin/budget">
                <i class="fa fa-group"></i>
                <span>BudgetForm2 WIP : Emrech(hindi dapat dito)</span>

            </a>
        </li>

         <li>
            <a href="/admin/item-monitoring">
                <i class="fa fa-group"></i>
                <span>Item Monitoring</span>
            </a>
        </li>

        <li>
            <a href="/admin/customer">
                <i class="fa fa-group"></i>
                <span>Queries</span>
            </a>
        </li>

        <div class="panel panel-default">
            <div class="panel-heading">
            <div class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                    <i class="fa fa-th mr-1"></i>
                    <span>Maintenance</span>
                    <span style="margin-right:120px;"></span>
                    <i class="fa fa-chevron-down"></i>
                </a>
            </div>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
            <div class="panel-body">
                <li>
                    <a href="/admin/food">Food</a>
                </li>
                <li>
                    <a href="/admin/packages">Package</a>
                </li>
                <li>
                    <a href="/admin/contractor">Contractor</a>
                </li>
                <li>
                    <a href="/admin/equipment">Item</a>
                </li>
            </div>
            </div>
        </div>


          <div class="panel panel-default">
            <div class="panel-heading">
            <div class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                    <i class="fa fa-th mr-1"></i>
                    <span>Reports</span>
                    <span style="margin-right:120px;"></span>
                    <i class="fa fa-chevron-down"></i>
                </a>
            </div>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
            <div class="panel-body">
                <li>
                    <a href="/admin/collection-report">Collection</a>
                </li>
                <li>
                    <a href="/admin/reserve_reports">Reservation</a>
                </li>
            </div>
            </div>
        </div>

    </ul>
</div>
<!--sidebar-menu-->