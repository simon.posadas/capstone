@extends('layouts.template') 

@section('content')

@include('modal.food.add')
@include('modal.food.edit')
@include('modal.food.delete')

<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="icon icon-home"></i> Home</a>
            <a href="#" class="current">Food</a>
        </div>
        <h1>Food</h1>
    </div>
    <div class="container-fluid">
        <hr>

        @if(session()->has('success-message'))
        <div class="alert alert-success">
            <ul>
                {{ session()->get('success-message') }}
            </ul>
        </div>
        @endif


        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="pull-left">
            <button style="border:none; margin:none; margin-top:0px; padding-bottom:10px;"class="btn btn-success btn-large icon icon-plus" data-toggle="modal" data-target="#addModal" type="button"> Add Food</button>
        </div>
        <div class="pull-left">
            <a href="{{ url('food/category/') }}" style="border:none;" class="btn btn-info btn-large" type="button">
                <i class="icon icon-plus"></i>
                <span>Add Food Category</span>
            </a>
        </div>
        <div class="pull-left">
            <a href="{{ url('food/type/') }}" style="border:none;" class="btn btn-info btn-large" type="button">
                <i class="icon icon-plus"></i>
                <span>Add Food type</span>
            </a>
        </div>
        <form method="get" action="/searchFood" role="search">
            <div class="pull-right">   
                <div class="input-group custom-search-form">
                    <input type="text" name="searchFood" class="form-control" placeholder="Search" id="search_bar">
                    <button class="btn btn-info" id="adjust-height-right4" type="submit"><i class="icon icon-search"></i> Search</button>
                </div>
            </div>
        </form>
        <div class="row-fluid">
            <div id="table-margin" class="span12">
                <div class="widget-box">
                    <div class="widget-content nopadding">
                        <table class="table table-bordered" id="foodsTable">
                            <thead>
                                <tr>
                                    <th>Food ID</th>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($foods as $food)
                                <tr>
                                    <td>{{ $food->id }}</td>
                                    <td>{{ $food->name }}</td>
                                    <td>{{ $food->type_name }}</td>
                                    <td class="btn-center">
                                        <button id ="sizes" class="btn icon icon-pencil btn-primary edittype" data-id="{{ $food->id }}" data-name="{{ $food->name }}" data-type="{{ $food->type_id }}" data-toggle="modal" data-price="{{ $food->price }}" data-target="#editModal"> Edit</button>
                                        <button id ="sizes" class="btn icon icon-trash btn-danger deletetype" data-id="{{ $food->id }}" data-toggle="modal" data-target="#deleteModal"> Delete</button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection 

@section('scripts_include')

<script type="text/javascript">

    $(document).ready(function(){
        document.getElementById("type").disabled=true;

        $('#foodsTable').on('click', '.edittype', function (event) {
            name = $(this).data('name')
            id = $(this).data('id')
            type = $(this).data('type')
            price = $(this).data('price')

            $('#editModal').find('input[name=name]').val(name);
            $('#editModal').find('#type').val(type).change();
            $('#editModal').find('input[name=price]').val(price);

            $('#editModal').find('#btn-edit').on('click', function () {

                $('#editForm').attr('action', function(i, value) {
                    return "{{ url('food') }}/" + id ;
                });

                $('#editForm').submit();
            });

        })

        $('#foodsTable').on('click', '.deletetype', function (event) {
            id = $(this).data('id')

            $('#deleteModal').find('#btn-delete').on('click', function () {

                $('#deleteForm').attr('action', function(i, value) {
                    return "{{ url('food') }}/" + id ;
                });

                $('#deleteForm').submit();
            });

        })

        $('#foodsTable').dataTable({
            searching   :   false
        });

    })

    function enableCat(){
        var t = document.getElementById("category");
        var food_cat = t.options[t.selectedIndex].value;

        if(food_cat == 4){
            document.getElementById("type").disabled=false;
        } else {
            document.getElementById("type").disabled=true;
        }
    }

</script>

@endsection