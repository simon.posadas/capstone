<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FoodType extends Model
{
    /**
     * The table to fetch
     * @var type 
     */
    protected $table = 'food_types';
    
     /**
     * Unique ID
     * @var type 
     */
    protected $primaryKey = 'id';
    
    /**
     *
     * @var type Remove automatic generation of timestamp
     */
    public $timestamps = true;

    public function rules()
    {
        return [
            'name' => 'required|min:2|max:20|unique:food_types,name'
        ];
    }

    public function updateRules()
    {
        $name = $this->name;
        return [
            'name' => 'required|min:2|max:20|unique:food_types,name,'.$name.',name'
        ];
    }

    public function deleteRules()
    {
        return [
            'Food Type' => 'required|exists:food_types,id'
        ];
    }
}