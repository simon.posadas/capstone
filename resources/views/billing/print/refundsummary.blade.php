@extends('layouts.printables.report')

@section('content')
  <style>
      th , tbody{
        text-align: center;
      }
 
      #content{
        font-family: "Times New Roman";
      }
 
      @media print {
          tr.page-break  { display: block; page-break-after: always; }
      }   
 
  </style>
  <div id="content" class="col-sm-12">
    <h4 class="text-center"> <small class="pull-right"></small></h4>
    <table class="table table-striped table-bordered table-condensed" id="inventoryTable" width="100%" cellspacing="0">
      <thead>
          @php
            $division = 3;
          @endphp
          <tr rowspan="2">
              <th class="text-left" colspan="{{ $division }}">
                <span style="font-weight:normal"></span> 
              </th>
              <th class="text-left" colspan="{{ $division }}">
                <span style="font-weight:normal"></span>
              </th>
          </tr>
          <tr>
            <th>Customer Name</th>
            <th>Refund Date</th>
            <th>Total Amount</th>
            <th>Paid Amount</th>
            <th>Refunded Amount</th>
          </tr> 
      </thead> 
      <tbody> 
 
      </tbody> 
    </table> 
  </div> 
 
   
@endsection