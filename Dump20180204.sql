CREATE DATABASE  IF NOT EXISTS `icc_new` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `icc_new`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: icc_new
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.25-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `budget_form`
--

DROP TABLE IF EXISTS `budget_form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `budget_form` (
  `budget_id` int(11) NOT NULL AUTO_INCREMENT,
  `reserve_id` int(11) NOT NULL,
  `description` varchar(45) NOT NULL,
  `price` double NOT NULL,
  PRIMARY KEY (`budget_id`),
  KEY `budget_fk1_idx` (`reserve_id`),
  CONSTRAINT `budget_fk1` FOREIGN KEY (`reserve_id`) REFERENCES `reservation_details` (`reserv_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `budget_form`
--

LOCK TABLES `budget_form` WRITE;
/*!40000 ALTER TABLE `budget_form` DISABLE KEYS */;
/*!40000 ALTER TABLE `budget_form` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contractor`
--

DROP TABLE IF EXISTS `contractor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contractor` (
  `contractor_id` int(11) NOT NULL AUTO_INCREMENT,
  `contractor_lname` varchar(20) NOT NULL,
  `contractor_fname` varchar(20) NOT NULL,
  `contractor_mname` varchar(20) NOT NULL,
  `contractor_age` int(3) NOT NULL,
  `contactNo` bigint(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`contractor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contractor`
--

LOCK TABLES `contractor` WRITE;
/*!40000 ALTER TABLE `contractor` DISABLE KEYS */;
INSERT INTO `contractor` VALUES (1,'Rutherford','Irma','Koss',34,0,1),(8,'Hoppe','Maxine','Okuneva',29,0,0),(9,'Hudson','Daphnee','Hackett',33,0,0),(10,'Stamm','Stephania','Ratke',20,0,0),(11,'Pagac','Trever','Lang',32,0,0),(12,'Mraz','Raleigh','Altenwerth',21,0,0),(13,'Schmeler','Alvera','Collins',24,0,0),(14,'Lesch','Palma','Johns',31,0,0),(15,'Klocko','Francis','Schumm',21,0,0),(16,'Bosco','Nelson','Volkman',23,0,0),(17,'Hahn','Lambert','Kutch',27,0,0),(18,'Schamberger','Hilbert','Shields',31,0,0),(19,'Cole','Antonietta','Prohaska',19,0,0),(20,'Rempel','Afton','Kuhn',29,0,0),(21,'Gleason','Darren','Dietrich',20,0,0),(22,'Okuneva','Ludwig','Flatley',33,0,0),(23,'Effertz','Maybell','Friesen',23,0,0),(24,'Heathcote','Margaretta','Wehner',34,0,0),(25,'Brakus','Pete','Nolan',34,0,0),(26,'Doyle','Reyes','Schimmel',32,0,0),(27,'Ernser','Thaddeus','Anderson',28,0,0),(28,'Brekke','Brycen','Fay',35,0,0),(29,'Ruecker','Doris','Oberbrunner',28,0,0),(30,'Dach','Elian','Dietrich',25,0,0),(31,'Erdman','Tyrique','Hills',29,0,0),(32,'Swift','August','Greenholt',27,0,0),(33,'Greenholt','Aylin','Schroeder',35,0,0),(34,'Herzog','Una','Toy',24,0,0),(35,'Stark','Danial','Legros',23,0,0),(36,'Kuphal','Lonnie','Rolfson',24,0,0),(37,'Wunsch','Yoshiko','Nader',32,0,0),(38,'Reichel','Durward','Nader',25,0,0),(39,'Towne','Francisco','Friesen',22,0,0),(40,'Blick','Genesis','Ledner',20,0,0),(41,'Stehr','Erika','Lueilwitz',32,0,0),(42,'Veum','Clarissa','Nitzsche',24,0,0),(43,'Terry','Ernestine','Towne',22,0,0),(44,'Padberg','Bessie','Russel',29,0,0),(45,'Ryan','Camilla','Nitzsche',31,0,0),(46,'Kling','Freddy','Senger',34,0,0),(47,'Crona','Blanche','Funk',18,0,0),(48,'Roberts','Dorthy','West',30,0,0),(49,'Windler','Dedric','Senger',24,0,0),(50,'Howe','Orrin','Kuphal',33,0,0),(51,'Milan','Aian Darryl','Cerafica',31,0,0),(53,'Buban','Emrech','Vacban',50,0,0),(54,'asdfasf','fasf','asdfasf',50,9478158929,0);
/*!40000 ALTER TABLE `contractor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cost_price`
--

DROP TABLE IF EXISTS `cost_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cost_price` (
  `cost_ID` int(1) NOT NULL,
  `price` double DEFAULT NULL,
  `price_date` datetime NOT NULL,
  PRIMARY KEY (`cost_ID`,`price_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cost_price`
--

LOCK TABLES `cost_price` WRITE;
/*!40000 ALTER TABLE `cost_price` DISABLE KEYS */;
/*!40000 ALTER TABLE `cost_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_info`
--

DROP TABLE IF EXISTS `customer_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_info` (
  `cust_id` int(11) NOT NULL AUTO_INCREMENT,
  `cust_fname` varchar(45) NOT NULL,
  `cust_lname` varchar(45) NOT NULL,
  `gender` char(1) DEFAULT NULL,
  `contNo` varchar(15) NOT NULL,
  `email` varchar(30) DEFAULT NULL,
  `reserve_id` int(11) DEFAULT NULL,
  `address` varchar(191) NOT NULL,
  PRIMARY KEY (`cust_id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_info`
--

LOCK TABLES `customer_info` WRITE;
/*!40000 ALTER TABLE `customer_info` DISABLE KEYS */;
INSERT INTO `customer_info` VALUES (48,'Darryl','Milan','M','09490090214','darryl.milan0401@gmail.com',NULL,'406 L. Cerrero street, Macamot, Binangonan, Rizal'),(49,'Darryl','Milan','M','09490090214','darryl.milan0401@gmail.com',NULL,'406 L. Cerrero street, Macamot, Binangonan, Rizal'),(50,'Darryl','Milan','M','09490090214','darryl.milan0401@gmail.com',NULL,'406 L. Cerrero street, Macamot, Binangonan, Rizal'),(51,'Emrech','Presa','M','09235859322','darryl.milan0401@gmail.com',NULL,'406 L. Cerrero street, Macamot, Binangonan, Rizal'),(78,'Test','Test','F','0909099909','Test@yahoo.com',NULL,'Test'),(79,'eli','elnar','F','09123456789','eli_elnar@gmail.com',NULL,'manila'),(80,'Test','tes','M','5000000','Test@yahoo.com',NULL,'Test'),(81,'Test','tes','M','5000000','Test@yahoo.com',NULL,'Test'),(82,'Test','tes','M','5000000','Test@yahoo.com',NULL,'Test'),(83,'Test','tes','M','5000000','Test@yahoo.com',NULL,'Test'),(84,'Test','tes','M','5000000','Test@yahoo.com',NULL,'Test'),(85,'Test','tes','M','5000000','Test@yahoo.com',NULL,'Test'),(86,'Test','tes','M','5000000','Test@yahoo.com',NULL,'Test'),(87,'fasfasdf','fasdfas','M','216546464','fasdfsadf@gmail.com',NULL,'sdfasf'),(88,'Simon','Posadas','M','09478158929','simonposadas1412@gmail.com',NULL,'Sta. Mesa, Manila'),(89,'Aian ','Milan','M','09478158929','aianmilan@gmail.com',NULL,'Binangonan, Rizal'),(90,'aian','milan','M','9165465464','lajsfl@gmail.com',NULL,'flsakjflsdkj'),(91,'Emrech','Buban','M','091234567','fdasfsf@gmail.com',NULL,'Marikina'),(92,'fasfdasdf','fasdfsa','M','092613546549','fasdfsadf@gmail.com',NULL,'fasdf');
/*!40000 ALTER TABLE `customer_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipment`
--

DROP TABLE IF EXISTS `equipment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipment` (
  `equipment_id` int(11) NOT NULL AUTO_INCREMENT,
  `equipment_name` varchar(50) NOT NULL,
  `quantity` int(5) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `type_id` int(11) DEFAULT NULL,
  `cost_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`equipment_id`),
  KEY `type_fk_idx` (`type_id`),
  KEY `cost_fk_idx` (`cost_ID`),
  CONSTRAINT `cost_fk` FOREIGN KEY (`cost_ID`) REFERENCES `cost_price` (`cost_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `type_fk` FOREIGN KEY (`type_id`) REFERENCES `equipment_type` (`type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipment`
--

LOCK TABLES `equipment` WRITE;
/*!40000 ALTER TABLE `equipment` DISABLE KEYS */;
INSERT INTO `equipment` VALUES (20,'Chair',1000,0,1,NULL),(21,'Round Table',500,0,5,NULL);
/*!40000 ALTER TABLE `equipment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipment_type`
--

DROP TABLE IF EXISTS `equipment_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipment_type` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_description` varchar(45) NOT NULL,
  PRIMARY KEY (`type_id`),
  UNIQUE KEY `type_id_UNIQUE` (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipment_type`
--

LOCK TABLES `equipment_type` WRITE;
/*!40000 ALTER TABLE `equipment_type` DISABLE KEYS */;
INSERT INTO `equipment_type` VALUES (1,'Utensils'),(2,'Tools'),(3,'Decorations'),(4,'Equipment'),(5,'Table');
/*!40000 ALTER TABLE `equipment_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_details`
--

DROP TABLE IF EXISTS `event_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_details` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` int(11) DEFAULT NULL,
  `place` varchar(191) NOT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_details`
--

LOCK TABLES `event_details` WRITE;
/*!40000 ALTER TABLE `event_details` DISABLE KEYS */;
INSERT INTO `event_details` VALUES (1,1,'bahay'),(2,1,'Manila'),(3,1,'fasdff');
/*!40000 ALTER TABLE `event_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_type`
--

DROP TABLE IF EXISTS `event_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_type` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(45) NOT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_type`
--

LOCK TABLES `event_type` WRITE;
/*!40000 ALTER TABLE `event_type` DISABLE KEYS */;
INSERT INTO `event_type` VALUES (1,'Wedding');
/*!40000 ALTER TABLE `event_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `food_category`
--

DROP TABLE IF EXISTS `food_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(45) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food_category`
--

LOCK TABLES `food_category` WRITE;
/*!40000 ALTER TABLE `food_category` DISABLE KEYS */;
INSERT INTO `food_category` VALUES (2,'Vegetable'),(3,'Fish'),(4,'Chicken'),(5,'Pork');
/*!40000 ALTER TABLE `food_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `food_details`
--

DROP TABLE IF EXISTS `food_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_details` (
  `food_id` int(11) NOT NULL AUTO_INCREMENT,
  `food_name` varchar(20) NOT NULL,
  `food_type_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `recipe_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) DEFAULT '0',
  `cost_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`food_id`),
  KEY `food_type_id` (`food_type_id`),
  KEY `food_details_recipefk_idx` (`recipe_id`),
  KEY `food_recipe_fk_idx` (`recipe_id`),
  KEY `fk_food_details_1_idx` (`category_id`),
  CONSTRAINT `fk_food_details_1` FOREIGN KEY (`food_type_id`) REFERENCES `food_type` (`food_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food_details`
--

LOCK TABLES `food_details` WRITE;
/*!40000 ALTER TABLE `food_details` DISABLE KEYS */;
INSERT INTO `food_details` VALUES (7,'Adobo',10,1,0,0,NULL),(8,'Trial',10,1,0,0,NULL),(9,'Adobo',10,1,0,0,NULL),(10,'Adobo',10,1,0,0,NULL),(11,'Sinigang',10,0,0,0,NULL),(12,'Bicol Express',10,0,0,0,NULL),(26,'Mushroom soup',2,0,0,0,NULL),(27,'Adobo',10,0,0,0,NULL),(28,'Buko Salad',2,1,0,0,NULL),(29,'Buko Salad',12,1,0,0,NULL),(30,'Buko Salad',12,1,0,0,NULL),(31,'Buko Salad',12,1,0,0,NULL),(32,'Buko Salad',12,1,0,0,NULL),(33,'Pork Nilaga',10,0,0,0,NULL),(34,'Pork Nilaga',10,1,0,0,NULL),(35,'Menudo',10,0,0,0,NULL),(36,'Chicken Soup',2,0,0,0,NULL),(37,'Chicken Soup',2,1,0,0,NULL),(38,'Fruit Salad',12,0,0,0,NULL),(39,'Macapuno',12,0,0,0,NULL),(41,'Mushroom Soup',2,0,0,0,NULL),(42,'Cordon Blue',10,0,0,4,NULL);
/*!40000 ALTER TABLE `food_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `food_type`
--

DROP TABLE IF EXISTS `food_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_type` (
  `food_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `food_type_name` varchar(20) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`food_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food_type`
--

LOCK TABLES `food_type` WRITE;
/*!40000 ALTER TABLE `food_type` DISABLE KEYS */;
INSERT INTO `food_type` VALUES (2,'Soup',0),(10,'Main Entree',0),(11,'Appetizer',0),(12,'Salad',0),(14,'Fuk',0);
/*!40000 ALTER TABLE `food_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice_table`
--

DROP TABLE IF EXISTS `invoice_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice_table` (
  `invoice_ID` int(11) NOT NULL,
  `total_Amount` double NOT NULL,
  `vat` double NOT NULL,
  `employee_budget` double NOT NULL,
  `equipment_budget` double NOT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`invoice_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice_table`
--

LOCK TABLES `invoice_table` WRITE;
/*!40000 ALTER TABLE `invoice_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoice_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `multi_worker`
--

DROP TABLE IF EXISTS `multi_worker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `multi_worker` (
  `worker_id` int(11) NOT NULL,
  `reserve_id` int(11) NOT NULL,
  KEY `multi_fk1_idx` (`worker_id`),
  KEY `multi_workerfk2_idx` (`reserve_id`),
  CONSTRAINT `multi_workerfk1` FOREIGN KEY (`worker_id`) REFERENCES `contractor` (`contractor_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `multi_workerfk2` FOREIGN KEY (`reserve_id`) REFERENCES `reservation_details` (`reserv_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `multi_worker`
--

LOCK TABLES `multi_worker` WRITE;
/*!40000 ALTER TABLE `multi_worker` DISABLE KEYS */;
/*!40000 ALTER TABLE `multi_worker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_details`
--

DROP TABLE IF EXISTS `order_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_details` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `reserv_id` int(11) NOT NULL,
  `food_id` int(11) NOT NULL,
  PRIMARY KEY (`order_id`),
  KEY `order_fk1_idx` (`reserv_id`),
  KEY `order_fk2_idx` (`food_id`),
  CONSTRAINT `order_fk1` FOREIGN KEY (`reserv_id`) REFERENCES `reservation_details` (`reserv_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `order_fk2` FOREIGN KEY (`food_id`) REFERENCES `food_details` (`food_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_details`
--

LOCK TABLES `order_details` WRITE;
/*!40000 ALTER TABLE `order_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `package_details`
--

DROP TABLE IF EXISTS `package_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `package_details` (
  `package_id` int(11) NOT NULL AUTO_INCREMENT,
  `package_name` varchar(50) NOT NULL,
  `package_price` double NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `package_type_id` int(10) DEFAULT NULL COMMENT 'Package type foreign key',
  PRIMARY KEY (`package_id`),
  UNIQUE KEY `package_name_UNIQUE` (`package_name`),
  KEY `package_details_package_type_id_foreign` (`package_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `package_details`
--

LOCK TABLES `package_details` WRITE;
/*!40000 ALTER TABLE `package_details` DISABLE KEYS */;
INSERT INTO `package_details` VALUES (2,'Package 1',500,0,NULL),(3,'Package 2',400,0,NULL);
/*!40000 ALTER TABLE `package_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `package_food`
--

DROP TABLE IF EXISTS `package_food`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `package_food` (
  `package_id` int(11) NOT NULL,
  `food_id` int(11) NOT NULL,
  KEY `fk_package_food_1_idx` (`food_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `package_food`
--

LOCK TABLES `package_food` WRITE;
/*!40000 ALTER TABLE `package_food` DISABLE KEYS */;
INSERT INTO `package_food` VALUES (2,7),(2,26);
/*!40000 ALTER TABLE `package_food` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `package_type`
--

DROP TABLE IF EXISTS `package_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `package_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `desc` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `package_type`
--

LOCK TABLES `package_type` WRITE;
/*!40000 ALTER TABLE `package_type` DISABLE KEYS */;
INSERT INTO `package_type` VALUES (1,'Wedding Buffet Menu'),(2,'Merienda Menu');
/*!40000 ALTER TABLE `package_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `packages_view`
--

DROP TABLE IF EXISTS `packages_view`;
/*!50001 DROP VIEW IF EXISTS `packages_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `packages_view` AS SELECT 
 1 AS `package_id`,
 1 AS `package_name`,
 1 AS `package_price`,
 1 AS `status`,
 1 AS `package_type_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `payment_settlement`
--

DROP TABLE IF EXISTS `payment_settlement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_settlement` (
  `invoice_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `amount_used` double NOT NULL,
  `change` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`invoice_id`,`payment_id`),
  CONSTRAINT `invoice_ID_FK` FOREIGN KEY (`invoice_id`) REFERENCES `reservation_details` (`reserv_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_settlement`
--

LOCK TABLES `payment_settlement` WRITE;
/*!40000 ALTER TABLE `payment_settlement` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_settlement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_tbl`
--

DROP TABLE IF EXISTS `payment_tbl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_tbl` (
  `payment_ID` int(11) NOT NULL AUTO_INCREMENT,
  `first_pay` double NOT NULL,
  `second_pay` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`payment_ID`),
  UNIQUE KEY `payment_ID_UNIQUE` (`payment_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_tbl`
--

LOCK TABLES `payment_tbl` WRITE;
/*!40000 ALTER TABLE `payment_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservation_details`
--

DROP TABLE IF EXISTS `reservation_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservation_details` (
  `reserv_id` int(11) NOT NULL AUTO_INCREMENT,
  `reserv_guestNo` varchar(45) NOT NULL,
  `cust_budget` int(11) DEFAULT NULL,
  `bud_food` int(11) DEFAULT NULL,
  `bud_equip` int(11) DEFAULT NULL,
  `bud_worker` int(11) DEFAULT NULL,
  `cust_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `addOn_id` int(11) DEFAULT NULL,
  `reserv_date` date NOT NULL,
  `reserv_time` time DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `package_id` int(11) NOT NULL,
  `disapprove_reason` text COMMENT 'The reason of admin for disapproving the reservation',
  `receipt_no` varchar(45) DEFAULT NULL,
  `payment_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`reserv_id`),
  KEY `reserve_fk5_idx` (`cust_id`),
  KEY `reservation_details_event_id_foreign` (`event_id`),
  KEY `reservation_details_package_id_foreign` (`package_id`),
  CONSTRAINT `reservation_details_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `event_details` (`event_id`) ON DELETE CASCADE,
  CONSTRAINT `reservation_details_package_id_foreign` FOREIGN KEY (`package_id`) REFERENCES `package_details` (`package_id`) ON DELETE CASCADE,
  CONSTRAINT `reserve_fk5` FOREIGN KEY (`cust_id`) REFERENCES `customer_info` (`cust_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservation_details`
--

LOCK TABLES `reservation_details` WRITE;
/*!40000 ALTER TABLE `reservation_details` DISABLE KEYS */;
INSERT INTO `reservation_details` VALUES (6,'500',50000,NULL,NULL,NULL,87,NULL,NULL,'2018-01-01','12:12:00',0,2,NULL,NULL,NULL),(7,'500',NULL,NULL,NULL,NULL,90,1,NULL,'2018-01-31','12:12:00',0,2,NULL,NULL,7),(8,'500',NULL,NULL,NULL,NULL,91,2,NULL,'2018-01-31','12:12:00',0,2,NULL,NULL,NULL),(9,'5000',NULL,NULL,NULL,NULL,92,3,NULL,'2018-01-08','12:12:00',0,2,NULL,NULL,NULL);
/*!40000 ALTER TABLE `reservation_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservation_equipments`
--

DROP TABLE IF EXISTS `reservation_equipments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservation_equipments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `reserv_id` int(11) NOT NULL COMMENT 'Reservation foreign key',
  `equipment_id` int(11) NOT NULL COMMENT 'Equipment foreign key',
  `quantity` int(11) NOT NULL COMMENT 'the number of quantity assigned in a event',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `reservation_equipments_reserv_id_foreign` (`reserv_id`),
  KEY `reservation_equipments_equipment_id_foreign` (`equipment_id`),
  CONSTRAINT `reservation_equipments_equipment_id_foreign` FOREIGN KEY (`equipment_id`) REFERENCES `equipment` (`equipment_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `reservation_equipments_reserv_id_foreign` FOREIGN KEY (`reserv_id`) REFERENCES `reservation_details` (`reserv_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservation_equipments`
--

LOCK TABLES `reservation_equipments` WRITE;
/*!40000 ALTER TABLE `reservation_equipments` DISABLE KEYS */;
/*!40000 ALTER TABLE `reservation_equipments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservation_workers`
--

DROP TABLE IF EXISTS `reservation_workers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservation_workers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `reserv_id` int(11) NOT NULL COMMENT 'Reservation foreign key',
  `worker_id` int(11) NOT NULL COMMENT 'Worker foreign key',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `reservation_workers_reserv_id_foreign` (`reserv_id`),
  KEY `reservation_workers_worker_id_foreign` (`worker_id`),
  CONSTRAINT `reservation_workers_reserv_id_foreign` FOREIGN KEY (`reserv_id`) REFERENCES `reservation_details` (`reserv_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `reservation_workers_worker_id_foreign` FOREIGN KEY (`worker_id`) REFERENCES `contractor` (`contractor_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservation_workers`
--

LOCK TABLES `reservation_workers` WRITE;
/*!40000 ALTER TABLE `reservation_workers` DISABLE KEYS */;
/*!40000 ALTER TABLE `reservation_workers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','admin');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `worker_role`
--

DROP TABLE IF EXISTS `worker_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `worker_role` (
  `worker_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `worker_role_description` varchar(20) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`worker_role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `worker_role`
--

LOCK TABLES `worker_role` WRITE;
/*!40000 ALTER TABLE `worker_role` DISABLE KEYS */;
INSERT INTO `worker_role` VALUES (1,'Waiter',1),(2,'Waiter II',0),(3,'Chef',0),(4,'Chef II',0),(5,'Waiter III',0),(6,'Head waiter',0);
/*!40000 ALTER TABLE `worker_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `packages_view`
--

/*!50001 DROP VIEW IF EXISTS `packages_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `packages_view` AS select `package_details`.`package_id` AS `package_id`,`package_details`.`package_name` AS `package_name`,`package_details`.`package_price` AS `package_price`,`package_details`.`status` AS `status`,`package_details`.`package_type_id` AS `package_type_id` from `package_details` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-04 12:06:30
