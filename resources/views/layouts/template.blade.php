<!DOCTYPE html>
<html lang="eng">
	<head>

		<meta charset="UTF-8" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="csrf-token" content="{{ csrf_token() }}">
		
	    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" />
	    <link href="{{ asset('css/jquery.dataTables.min.css') }}" rel='stylesheet' >
	    <link href="{{ asset('css/dataTables.bootstrap.min.css') }}" rel='stylesheet' >
	    <!-- <link rel="stylesheet" href="{{ asset('css/bootstrap-v4.min.css') }}" /> -->
	    <link rel="stylesheet" href="{{ asset('css/bootstrap-responsive.min.css') }}" />
	    <link rel="stylesheet" href="{{ asset('fullcalendar/fullcalendar.css') }}" />
	    <!-- <link rel="stylesheet" href="{{ asset('css/uniform.css') }}" /> -->
	    <link rel="stylesheet" href="{{ asset('css/colorpicker.css') }}" />
	    <link rel="stylesheet" href="{{ asset('css/datepicker.css') }}" />
	    <link rel="stylesheet" href="{{ asset('css/select2.css') }}" />
	    <link rel="stylesheet" href="{{ asset('css/matrix-style.css') }}" />
	    <link rel="stylesheet" href="{{ asset('css/matrix-media.css') }}" />
	    <!-- <link rel="stylesheet" href="{{ asset('css/bootstrap-wysihtml5.css') }}" /> -->
	    <link href="{{ asset('css/sweetalert.css') }}" rel="stylesheet">
	    <!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'/> -->
	    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.4.2/sweetalert.min.css" /> -->

	    
	    <link href="{{ asset('css/font-awesome.css') }}" rel='stylesheet' >
	    



		<title>{{ isset($title) ? $title . "::" . config('app.name') : config('app.name') }}</title>
		@yield('styles')

		@yield('styles_include')

		@yield('after_styles')

		<script>
        function startTime(){
            var today = new Date();
            var h = today.getHours();
            var m = today.getMinutes();
            var s = today.getSeconds();
            m = checkTime(m);
            s = checkTime(s);
            document.getElementById('txt').innerHTML =
            h + ":" + m + ":" + s;
            var t = setTimeout(startTime, 500);
        }
            function checkTime(i) {
            if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
            return i;
        }
    

		</script>
	</head>

	<body onload="startTime()">
	
		@include('layouts.nav')
		<div id="txt" class="time"></div>
		@include('layouts.sidebar')

		@yield('content')
		@yield('modal')
	    <script src="{{ asset('js/excanvas.min.js') }}"></script>
	    <script src="{{ asset('js/jquery.min.js') }}"></script>
	    <script src="{{ asset('js/jquery.ui.custom.js') }}"></script>
	    	
	    <script src="{{ asset('js/moment.min.js') }}"></script>
	    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
	    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
	    <script src="{{ asset('js/jquery.peity.min.js') }}"></script>
	    <script src="{{ asset('fullcalendar/fullcalendar.min.js') }}"></script>
	    <script src="{{ asset('fullcalendar/lib/moment.min.js') }}"></script>
	    <script src="{{ asset('js/matrix.calendar.js') }}"></script>
	    <script src="{{ asset('js/jquery.validate.js') }}"></script>
	    <script src="{{ asset('js/jquery.wizard.js') }}"></script>
	    <script src="{{ asset('js/select2.min.js') }}"></script>
	    

	    <script src="{{ asset('js/bootstrap-wysihtml5.js') }}"></script>

	    <script src="{{ asset('js/wysihtml5-0.3.0.js') }}"></script>
	    <script src="{{ asset('js/bootstrap-colorpicker.js') }}"></script>
	    <script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
	    <script src="{{ asset('js/sweetalert.min.js') }}"></script>

		<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
		@yield('scripts_include')
		@yield('script')
		@include('sweet::alert')

	    <script type="text/javascript">
	        // This function is called from the pop-up menus to transfer to
	        // a different page. Ignore if the value returned is a null string:
	        function goPage(newURL) {

	            // if url is empty, skip the menu dividers and reset the menu selection to default
	            if (newURL != "") {

	                // if url is "-", it is this page -- reset the menu:
	                if (newURL == "-") {
	                    resetMenu();
	                }
	                // else, send page to designated URL            
	                else {
	                    document.location.href = newURL;
	                }
	            }
	        }

	        // resets the menu selection upon entry to this page:
	        function resetMenu() {
	            document.gomenu.selector.selectedIndex = 2;
	        }
	    </script>

	    @yield('after_scripts')

	</body>
</html>