<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Validator;
use DB;
use Carbon;

class PackageListsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($package)
    {
        $id = $package;
        $package = App\Package::find($package);
        $foods = App\Food::all();
        $package_lists = App\CategoryFood::join('foods as f', 'category_food.food_id', 'f.id')
            ->join('categories as c', 'c.id', 'category_food.category_id')
            ->join('package_food as pf', 'pf.food_id', 'f.id')
            ->join('packages as p', 'p.id', 'pf.package_id')
            ->select('c.name as cat_name','f.name as food_name')
            ->where('p.id', $id)
            ->get();

        

        

        // return $package_lists;

        return view('package.list.index')
                ->with('package_lists', $package_lists)
                ->with('package', $package)
                ->with('foods', $foods);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $package)
    {

        $food = $request->get('food');

        $list = DB::table('package_food')->where('package_id', '=', $package)->where('food_id', '=', $food)->get();

        if(count($list) > 0)
        {
            return back()->withInput()->withErrors([ 'Food already exists in the package']);
        }

        $package = App\Package::find($package);

        $validator = Validator::make([
            'food' => $food
        ], $package->assignFoodRules());

        if($validator->fails())
        {
            return back()->withInput()->withErrors($validator);
        }

        $package->foods()->attach( $food );

        $request->session()->flash('success-message', 'Food added to the package');
        return redirect("food/package/$package->id/list");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $package, $food)
    {
        $packages = new App\Package;
        dd('asdad');
        $validator = Validator::make([
            'food' => $food,
            'package' => $package
        ], $packages->deleteFoodFromPackageRules());

        if($validator->fails())
        {
            return back()->withInput()->withErrors($validator);
        }

        DB::table('package_food')
                ->where('package_id', '=', $package)
                ->where('food_id', '=', $food)
                ->delete();

        $request->session()->flash('success-message', 'Food Removed from Package');
        return redirect("food/package/$package/list");
    }
}
