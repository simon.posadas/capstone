@extends('layouts.sidebar') 

@section('title', 'Items') 

@section('content')

<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="fa fa-home"></i> Home</a>
            <a href="#" class="current">Items</a>
        </div>
        <h1>Item</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="pull-left">
            <button class="btn btn-success btn-large fa fa-plus addItem"> Add Item</button>
        </div>
        <div class="pull-right">
            <a href="/admin/equipmenttype">
                <button class="btn btn-info btn-large">Item Type</button>
            </a>
        </div>
        <form method="get" action="/searchEquip" role="search">
            <div class="pull-right">   
                <div class="input-group custom-search-form">
                    <input type="text" name="searchEquip" class="form-control" placeholder="Search" id="search_bar">
                    <button class="btn btn-info" id="adjust-height-right" type="submit"><i class="fa fa-search"></i> Search</button>
                </div>
            </div>
        </form>

        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table" id="equipmentTbl">
                            <thead>
                                <tr>
                                    <th>Item</th>
                                    <th>Quantity</th>
                                    <th>Type</th>
                                    <th>Price</th>
                                    <th>Supplied?</th>
                                    <th width="20%">Actions</th>
                                </tr>
                            </thead>
                            @foreach($equip as $equip)
                            <tbody>
                                <tr>
                                    <td>{{$equip->equipment_name}}</td>
                                    <td>{{$equip->quantity}} pcs</td>
                                    <td>{{$equip->type_description}}</td>
                                    <td class="currency">₱{{$equip->cost}}.00</td>
                                    @if($equip->status == 0)
                                    <td>No</td>
                                    @endif
                                    @if($equip->status != 0)
                                    <td>Yes</td>
                                    @endif
                                    <td class="btn-center">
                                        <button id ="sizes" class="btn fa fa-pencil btn-primary edittype" data-id="{{ $equip->equipment_id }}"> Edit</button>
                                        <button id ="sizes" class="btn btn-danger fa fa-trash deletetype" data-id="{{ $equip->equipment_id }}"> Delete</button>
                                    </td>
                                </tr>
                            </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection 

@section('modal')
<!-- add modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="addModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add Item</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/addEquip">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label>Item Name</label>
                        <input type="text" class="form-control" placeholder="Item Name" name="eqname" required>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Select input</label>
                        <div class="controls select2-container">
                            <select name="equip_type">
                                <option disabled>Select item type</option>
                                @foreach($type as $type)
                                <option value="{{$type->type_id}}">{{$type->type_description}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Price</label>
                        <input type="number" class="form-control" placeholder="Price" name="price" required>
                    </div>
                    <div class="form-group">
                        <label>Quantity</label>
                        <input type="number" class="form-control" placeholder="Quantity" name="quantity" required>
                    </div>

                    <label class="control-label">From supplier?</label>
                    <div class="controls">
                        <label>
                            <input type="radio" name="status" value="1" /> Yes
                        </label>
                        <label>
                            <input type="radio" name="status" value="0" /> No
                        </label>
                    </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Add</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end add modal -->

<!-- edit modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="editModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edit Item</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/editEquip">
                    {{csrf_field()}}
                    <input type="hidden" name="id" id="editID">
                    <div class="form-group">
                        <label>Item Name</label>
                        <input type="text" class="form-control inpname" name="eqname">
                    </div>
                    <div class="form-group">
                        <label>Price</label>
                        <input type="number" class="form-control inprice" name="price">
                    </div>
                    <div class="form-group">
                        <label>Quantity</label>
                        <input type="number" class="form-control inpquan" name="quantity">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end edit modal -->

<!-- delete modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="deleteModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Delete</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/deleteEquip">
                    {{csrf_field()}}
                    <input type="hidden" class="id" name="id" id="deleteID">
                    <div class="form-group">
                        <h4>Delete item?</h4>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Confirm</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end delete modal -->
@endsection 

@section('script')

<script type="text/javascript">
    $('.addItem').click(function () {
        $('#addModal').modal('show');
    });


    $('#editModal').on('show.bs.modal', function () {
        $(this).find('#btn-primary').on('click', function () {
            $('#editModal').find('form').submit();
        });
    })

    $('.edittype').click(function () {
        $.ajax({
            type: "get",
            url: '/getEquip',
            data: {
                id: $(this).data('id')
            },
            dataType: "json",
            success: function (response) {
                response.forEach(function (data) {
                    $('#editModal .id').val($(this).data('id'));
                    $('#editModal .inpname').val(data.equipment_name);
                    $('#editModal .inprice').val(data.cost);
                    $('#editModal .inpquan').val(data.quantity);
                })
            }
        });
        $('#editModal').modal('show');
        $('#editID').val($(this).data('id'));
    });


    $('.deletetype').click(function () {
        $('#deleteID').val($(this).data('id'));
        $('#deleteModal').modal('show');
    });
</script>

@endsection