<?php

use Illuminate\Database\Seeder;

class PackageFoodTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('package_food')->insert([
            [
                'package_id'=> '1',
                'food_id' => '1',
            ],

            [
                'package_id'=> '1',
                'food_id' => '2',
            ],

            [
                'package_id'=> '1',
                'food_id' => '3',
            ],

            [
                'package_id'=> '1',
                'food_id' => '4',
            ],

            [
                'package_id'=> '1',
                'food_id' => '5',
            ],

            [
                'package_id'=> '1',
                'food_id' => '6',
            ],

            [
                'package_id'=> '1',
                'food_id' => '7',
            ],

            [
                'package_id'=> '1',
                'food_id' => '8',
            ],

            [
                'package_id'=> '1',
                'food_id' => '9',
            ],

            [
                'package_id'=> '1',
                'food_id' => '10',
            ],

            [
                'package_id'=> '1',
                'food_id' => '11',
            ],

            [
                'package_id'=> '1',
                'food_id' => '12',
            ],

            [
                'package_id'=> '1',
                'food_id' => '13',
            ],

            [
                'package_id'=> '1',
                'food_id' => '14',
            ],

            [
                'package_id'=> '1',
                'food_id' => '15',
            ],

            [
                'package_id'=> '1',
                'food_id' => '16',
            ],

            [
                'package_id'=> '1',
                'food_id' => '17',
            ],

            [
                'package_id'=> '1',
                'food_id' => '18',
            ],

            [
                'package_id'=> '1',
                'food_id' => '19',
            ],

            [
                'package_id'=> '1',
                'food_id' => '20',
            ],

            [
                'package_id'=> '1',
                'food_id' => '21',
            ],

            [
                'package_id'=> '1',
                'food_id' => '22',
            ],

            [
                'package_id'=> '1',
                'food_id' => '23',
            ],

            [
                'package_id'=> '1',
                'food_id' => '24',
            ],

            [
                'package_id'=> '1',
                'food_id' => '25',
            ],

            [
                'package_id'=> '1',
                'food_id' => '26',
            ],

            [
                'package_id'=> '1',
                'food_id' => '27',
            ],

            [
                'package_id'=> '1',
                'food_id' => '28',
            ],

            [
                'package_id'=> '1',
                'food_id' => '29',
            ],

            [
                'package_id'=> '1',
                'food_id' => '30',
            ],

            [
                'package_id'=> '1',
                'food_id' => '31',
            ],

            [
                'package_id'=> '1',
                'food_id' => '32',
            ],

            [
                'package_id'=> '1',
                'food_id' => '33',
            ],

            [
                'package_id'=> '1',
                'food_id' => '34',
            ],

            [
                'package_id'=> '1',
                'food_id' => '35',
            ],

            [
                'package_id'=> '1',
                'food_id' => '36',
            ],

            [
                'package_id'=> '1',
                'food_id' => '37',
            ],

            [
                'package_id'=> '1',
                'food_id' => '38',
            ],

            [
                'package_id'=> '1',
                'food_id' => '39',
            ],

            [
                'package_id'=> '1',
                'food_id' => '40',
            ],

            [
                'package_id'=> '1',
                'food_id' => '41',
            ],

            [
                'package_id'=> '1',
                'food_id' => '42',
            ],

            [
                'package_id'=> '1',
                'food_id' => '43',
            ],

            [
                'package_id'=> '1',
                'food_id' => '44',
            ],

            [
                'package_id'=> '1',
                'food_id' => '45',
            ],

            [
                'package_id'=> '1',
                'food_id' => '46',
            ],

            [
                'package_id'=> '1',
                'food_id' => '47',
            ],

            [
                'package_id'=> '1',
                'food_id' => '48',
            ],



            [
                'package_id'=> '2',
                'food_id' => '1',
            ],

            [
                'package_id'=> '2',
                'food_id' => '2',
            ],

            [
                'package_id'=> '2',
                'food_id' => '3',
            ],

            [
                'package_id'=> '2',
                'food_id' => '4',
            ],

            [
                'package_id'=> '2',
                'food_id' => '5',
            ],

            [
                'package_id'=> '2',
                'food_id' => '6',
            ],

            [
                'package_id'=> '2',
                'food_id' => '7',
            ],

            [
                'package_id'=> '2',
                'food_id' => '8',
            ],

            [
                'package_id'=> '2',
                'food_id' => '9',
            ],

            [
                'package_id'=> '2',
                'food_id' => '10',
            ],

            [
                'package_id'=> '2',
                'food_id' => '11',
            ],

            [
                'package_id'=> '2',
                'food_id' => '12',
            ],

            [
                'package_id'=> '2',
                'food_id' => '13',
            ],

            [
                'package_id'=> '2',
                'food_id' => '14',
            ],

            [
                'package_id'=> '2',
                'food_id' => '15',
            ],

            [
                'package_id'=> '2',
                'food_id' => '16',
            ],

            [
                'package_id'=> '2',
                'food_id' => '17',
            ],

            [
                'package_id'=> '2',
                'food_id' => '18',
            ],

            [
                'package_id'=> '2',
                'food_id' => '19',
            ],

            [
                'package_id'=> '2',
                'food_id' => '20',
            ],

            [
                'package_id'=> '2',
                'food_id' => '21',
            ],

            [
                'package_id'=> '2',
                'food_id' => '22',
            ],

            [
                'package_id'=> '2',
                'food_id' => '23',
            ],

            [
                'package_id'=> '2',
                'food_id' => '24',
            ],

            [
                'package_id'=> '2',
                'food_id' => '25',
            ],

            [
                'package_id'=> '2',
                'food_id' => '26',
            ],

            [
                'package_id'=> '2',
                'food_id' => '27',
            ],

            [
                'package_id'=> '2',
                'food_id' => '28',
            ],

            [
                'package_id'=> '2',
                'food_id' => '29',
            ],

            [
                'package_id'=> '2',
                'food_id' => '30',
            ],

            [
                'package_id'=> '2',
                'food_id' => '31',
            ],

            [
                'package_id'=> '2',
                'food_id' => '32',
            ],

            [
                'package_id'=> '2',
                'food_id' => '33',
            ],

            [
                'package_id'=> '2',
                'food_id' => '34',
            ],

            [
                'package_id'=> '2',
                'food_id' => '35',
            ],

            [
                'package_id'=> '2',
                'food_id' => '36',
            ],

            [
                'package_id'=> '2',
                'food_id' => '37',
            ],

            [
                'package_id'=> '2',
                'food_id' => '38',
            ],

            [
                'package_id'=> '2',
                'food_id' => '39',
            ],

            [
                'package_id'=> '2',
                'food_id' => '40',
            ],

            [
                'package_id'=> '2',
                'food_id' => '41',
            ],

            [
                'package_id'=> '2',
                'food_id' => '42',
            ],

            [
                'package_id'=> '2',
                'food_id' => '43',
            ],

            [
                'package_id'=> '2',
                'food_id' => '44',
            ],

            [
                'package_id'=> '2',
                'food_id' => '45',
            ],

            [
                'package_id'=> '2',
                'food_id' => '46',
            ],

            [
                'package_id'=> '2',
                'food_id' => '47',
            ],

            [
                'package_id'=> '2',
                'food_id' => '48',
            ],

            [
                'package_id'=> '3',
                'food_id' => '1',
            ],

            [
                'package_id'=> '3',
                'food_id' => '2',
            ],

            [
                'package_id'=> '3',
                'food_id' => '3',
            ],

            [
                'package_id'=> '3',
                'food_id' => '4',
            ],

            [
                'package_id'=> '3',
                'food_id' => '5',
            ],

            [
                'package_id'=> '3',
                'food_id' => '6',
            ],

            [
                'package_id'=> '3',
                'food_id' => '7',
            ],

            [
                'package_id'=> '3',
                'food_id' => '8',
            ],

            [
                'package_id'=> '3',
                'food_id' => '9',
            ],

            [
                'package_id'=> '3',
                'food_id' => '10',
            ],

            [
                'package_id'=> '3',
                'food_id' => '11',
            ],

            [
                'package_id'=> '3',
                'food_id' => '12',
            ],

            [
                'package_id'=> '3',
                'food_id' => '13',
            ],

            [
                'package_id'=> '3',
                'food_id' => '14',
            ],

            [
                'package_id'=> '3',
                'food_id' => '15',
            ],

            [
                'package_id'=> '3',
                'food_id' => '16',
            ],

            [
                'package_id'=> '3',
                'food_id' => '17',
            ],

            [
                'package_id'=> '3',
                'food_id' => '18',
            ],

            [
                'package_id'=> '3',
                'food_id' => '19',
            ],

            [
                'package_id'=> '3',
                'food_id' => '20',
            ],

            [
                'package_id'=> '3',
                'food_id' => '21',
            ],

            [
                'package_id'=> '3',
                'food_id' => '22',
            ],

            [
                'package_id'=> '3',
                'food_id' => '23',
            ],

            [
                'package_id'=> '3',
                'food_id' => '24',
            ],

            [
                'package_id'=> '3',
                'food_id' => '25',
            ],

            [
                'package_id'=> '3',
                'food_id' => '26',
            ],

            [
                'package_id'=> '3',
                'food_id' => '27',
            ],

            [
                'package_id'=> '3',
                'food_id' => '28',
            ],

            [
                'package_id'=> '3',
                'food_id' => '29',
            ],

            [
                'package_id'=> '3',
                'food_id' => '30',
            ],

            [
                'package_id'=> '3',
                'food_id' => '31',
            ],

            [
                'package_id'=> '3',
                'food_id' => '32',
            ],

            [
                'package_id'=> '3',
                'food_id' => '33',
            ],

            [
                'package_id'=> '3',
                'food_id' => '34',
            ],

            [
                'package_id'=> '3',
                'food_id' => '35',
            ],

            [
                'package_id'=> '3',
                'food_id' => '36',
            ],

            [
                'package_id'=> '3',
                'food_id' => '37',
            ],

            [
                'package_id'=> '3',
                'food_id' => '38',
            ],

            [
                'package_id'=> '3',
                'food_id' => '39',
            ],

            [
                'package_id'=> '3',
                'food_id' => '40',
            ],

            [
                'package_id'=> '3',
                'food_id' => '41',
            ],

            [
                'package_id'=> '3',
                'food_id' => '42',
            ],

            [
                'package_id'=> '3',
                'food_id' => '43',
            ],

            [
                'package_id'=> '3',
                'food_id' => '44',
            ],

            [
                'package_id'=> '3',
                'food_id' => '45',
            ],

            [
                'package_id'=> '3',
                'food_id' => '46',
            ],

            [
                'package_id'=> '3',
                'food_id' => '47',
            ],

            [
                'package_id'=> '3',
                'food_id' => '48',
            ],

        ]);
    }
}
