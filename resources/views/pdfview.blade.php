<!DOCTYPE html>
<html>
<head>
	<title>Statement of Account - PDF</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
	<div class="container">
		<br/>
		<span style="font-weight: bold;font-size:20px">Statement of Account:</span><br/>
		<span style="font-weight: bold;">Name: </span><span>{{$customer->firstname}} {{$customer->lastname}}</span>
		<br/>
		<span style="font-weight: bold;">Address:</span><span> {{$customer->address}}</span>
		<br/><br/>
		<table class="table table-bordered" style="">
			<thead>
				<th style="width: 15%;text-align: center;">Date</th>
				<th style="width: 15%;text-align: center;">Code</th>
				<th style="width: 55%;text-align: center;">Description</th>
				<th style="width: 15%;text-align: center;">Amount(Php)</th>

			</thead>
			<tbody>
				<?php $total = 0; ?>
				@foreach ($soa as $value)
				<?php $total = $total + $value->amount; $type = $value->billing_type; $status = $value->status;
				if($type == 0){
					$refund = $value->amount/2;
				}
				?>
				<tr>
					<td style="width: 15%;text-align: center;">{{ $value->created_at }} {{$status}}</td>
					<td style="width: 15%;text-align: center;">{{ $value->code }}</td>
					<td style="width: 55%;">{{ $value->description }}</td>
					<td style="width: 15%;text-align: right">{{ $value->amount }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		<br/>

		<table class="table table-bordered" style="">
			<tbody>
				<td>Total:<span style="float:right;">P {{ $total }}</span></td>
			</tbody>
		</table>

		<table class="table table-bordered" style="">
			<tbody>
				<?php $paid=0; ?>
				@foreach($payment as $payment)
				<?php $paid = $paid + $payment->amount ?>
				<tr>
					<td>{{ $payment->details }}:<span style="float:right;color:red">P {{ $payment->amount }}</span></td>
				</tr>
				@endforeach
			</tbody>
		</table>

		<table class="table table-bordered" style="">
			<tbody>
				<td>Remaining:<span style="float:right;">P {{ $total-$paid }}</span></td>
			</tbody>
		</table>

		<br/><br/><br/>

		@if($status == 4)
		<table class="table table-bordered" style="">
			<tbody>
				<td>Refund:<span style="float:right;">P {{ $refund }}</span></td>
			</tbody>
		</table>

		@endif
	</div>
</body>
</html>