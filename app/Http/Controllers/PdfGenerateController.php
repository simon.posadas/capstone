<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use DB;
use PDF;

class PdfGenerateController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function pdfview(Request $request)
    {
        $id = $_GET['id'];
        $qry = 'SELECT * 
        FROM `billings`as b JOIN reservations as r ON b.reservation_id = r.id JOIN customers as c ON c.id = r.customer_id 
        where r.id ='.$id;

        $qry2 = 'SELECT * 
        FROM payments 
        where reservation_id ='.$id;

        $soa = DB::select($qry); 
        $payment = DB::select($qry2);

        foreach($soa as $soaid){
            $customer_id = $soaid->customer_id;
        }
        
        $customer = DB::table("customers as c")
        ->where('id',$customer_id)->first();

        view()->share('soa',$soa);
        view()->share('customer',$customer);
        view()->share('payment',$payment);

        return view('pdfview');
    }
}