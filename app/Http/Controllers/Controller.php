<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use PDF;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    

	public function printPreview( $view , $data=[] , $filename="Preview.php" )
	{
		$pdf = \PDF::loadView($view,$data);
		
	    $header = view('layouts.printables.header');
	    return $pdf
	        ->setOption('footer-center', 'Page [page] / [toPage]')
	        ->setOption('header-spacing', 5)
	        ->setOption('header-html',$header)
	        ->setOption('margin-top', '35mm')
	        ->setOption('margin-bottom', '15mm')
	        ->setOption('footer-spacing', 4)
	        ->setOption('footer-font-size','7')
	        // ->setOption('footer-html', $footer)
    		->stream( $filename , array('Attachment'=>0) );

	}
}
