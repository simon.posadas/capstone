@extends('layouts.sidebar') 

@section('title', 'View Package')

@section('content')

<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="fa fa-home"></i> Home</a>
                <a href="/admin/reservation">Reservations</a>
            <a href="/admin/reservation_create">Create Reservation</a>
            <a href="#" class="current">View Package</a>
        </div>
        <h1>Packages</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <!-- <div class="pull-left">
            <button class="btn btn-success btn-large icon-circle-arrow-left back-btn"> Back</button>
        </div> -->
        <div class="row-fluid">
            <div class="colspan">
                <div class="widget-box">
                    <div class="widget-title">
                            <span class="icon">
                                <i class="icon-th"></i>
                            </span>
                            <h5>Price of package: ₱{{$price}}.00</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Food Name</th>
                                    <th>Food Type</th>
                                </tr>
                            </thead>
                            @foreach($view as $view)
                            <tbody>
                                <tr>
                                    <td>{{$view->name}}</td>
                                    <td>{{$view->type}}</td>
                                </tr>
                            </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script type="text/javascript">
    $('.back-btn').click(function() {
        window.location.href.back();
    })
</script>
@endsection