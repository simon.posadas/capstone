<!-- edit modal -->
<div class="modal fade modal-size-s4" tabindex="-1" role="dialog" id="editModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edit service Type</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="{{ url('service/type') }}" id="editForm">
                    <input type="hidden" name="_method" value="PUT" />
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="id" id="editID">
                    <div class="form-group">
                        <label class="label-serviceType">Service Type Name :</label>
                        <input id="input-serviceType" type="text" class="form-control inpname" name="name">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn-edit">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- end edit modal -->