<?php

use Illuminate\Database\Seeder;

class FoodTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\FoodType::insert([

        	[
        		'name' => 'Pasta'
        	],

        	[
        		'name' => 'Beef'
        	],

        	[
        		'name' => 'Chicken'
        	],

        	[
        		'name' => 'Fish'
        	],

        	[
        		'name' => 'Vegetable'
        	],

        	[
        		'name' => 'Rice'
        	],

            [
                'name' => 'Pork'
            ]
        ]);
    }
}
