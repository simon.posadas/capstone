@extends('layouts.printables.report')

@section('content')

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="container">
    <div id="header">
        <div class="col-sm-12">
        </div>
        <div class="col-sm-12" style="color: #800000;">
            <div class="clearfix"></div>
            <div>
                <img src="{{ asset('images/logo.png') }}" class="img img-responsive img-circle pull-left" style="height: 80px;width:auto;" />
            </div>
            <div style="margin-left: 5em;">
              <div style="font-size:18pt; margin-left: 20%;">IRMALYN'S CAKE AND CATERING  </div>
              <div style="font-size:13pt; margin-left: 23%; margin-top: 2%;">#22 Lempira St. Meralco Village Taytay Rizal <span class="pull-right"> {{ Carbon\Carbon::now()->toDayDateTimeString() }} </span></div>
              <div style="font-size:13pt; margin-left: 22%;">Tel no.: (02) 661-0076 / Cel no.: 0922 853 7164 </div>
            </div>
        </div>
        <div class="col-sm-12">
          <hr />
        </div>
    </div>
  </body>
</html>


	<style>
    hr.style8 {
      border-top: 1px solid #8c8b8b;
      border-bottom: 1px solid #fff;
      }
      hr.style8:after {
      content: '';
      display: block;
      margin-top: 2px;
      border-top: 1px solid #8c8b8b;
      border-bottom: 1px solid #fff;
      }
    </style>
    <div>
      <header>
    	     <center><h1>REFUND FORM</h1></center>
      </header>	
    </div>
    
    <div class="container">
      <body>
            <h4>Refund Date: {{ Carbon\Carbon::parse()->toDayDateTimeString  () }}</h4>
            <h4>Reservation Code: {{ $reservation->code }}</h4>
            <h4>Reservation Date: {{ $reservation->created_at }}</h4>
            <h4>Total Price Payed: {{ number_format($payments) }}</h4>
            <h4>Refund Fee(50% of the total payed): {{ number_format($refund) }}</h4>
            <h4>Total Refund: {{ number_format($refund) }}</h4>
            
            
            <hr class="style8">

            
            <h4>Customer Name:  {{$customer->firstname . " " . $customer->lastname}}</h4>
            <h4>Event Date & Time: {{ $reservation->event_date }}</h4>
            

            
            <hr class="style8">

            
            <h3 style = "margin-bottom: 40px">Detailed explanation for refund </h3>


            <div>
          
          <br><h4 style="margin-top: 20px; text-align: right; font-size: 15 px">CUSTOMER SIGNATURE OVER PRINTED NAME</h4>
          <h4 style = "font-size: 25px; margin-top: -35px ">Date: {{ date('m-d-Y' , strtotime(Carbon\Carbon::now()) ) }}</h4>
          
  </div>

      </body>
    </div>  
    
    <div>
      <footer>
      </footer> 
    </div>    

@endsection    