<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Validator;
use DB;

class QueriesController extends Controller
{
    public function Reservation()
    {
       $reservation = DB::table('reservations')
       ->select(DB::raw('status as Stat,count(status) as Count'))
       ->WHERE('status',6)
       ->orWhere('status',0)
       ->groupBy('status')
       ->orderBy('status',"asc")
       ->limit(5)
       ->get();

       $package = DB::table('reservations as r')
       ->select(DB::raw('count(package_id) as Count,p.name'))
       ->join('packages as p','p.id','=','r.package_id')
       ->groupBy('package_id')
       ->orderBy('Count', 'desc')
       ->limit(5)
       ->get();

       $food = DB::table('reservation_food as rf')
       ->select(DB::raw('count(rf.food_id) as Count,f.name'))
       ->join('foods as f','rf.food_id','=','f.id')
       ->groupby('rf.food_id')
       ->orderby('Count','desc')
       ->limit(5)
       ->get();

       return view ('queries.index')
       ->with('res',$reservation)
       ->with('pack',$package)
       ->with('food',$food);
    }
}
