@extends('layouts.sidebar')

@section('title', 'Downpayment')

@section('content')

<!--main-container-part-->
<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="fa fa-home"></i> Home</a>
            <a href="#" class="current">Downpayment</a>
        </div>
        <h1>Downpayment</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <form method="get" action="/searchDown" role="search">
            <div class="pull-right">   
                <div class="input-group custom-search-form">
                    <input type="text" name="searchDown" class="form-control" placeholder="Search" id="search_bar">
                    <button class="btn btn-info" id="adjust-height-right" type="submit"><i class="fa fa-search"></i> Search</button>
                </div>
            </div>
        </form>
        <div class="row-fluid">
            <div class="colspan">
                <div class="widget-box">
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Customer Name</th>
                                    <th>Guest Number</th>
                                    <th>Contact Number</th>
                                    <th>Place</th>
                                    <th>Event Date</th>
                                    <th>Event Time</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                            </thead>
                            @foreach($details as $detail)
                            <tbody>
                                <tr>
                                    <td>{{$detail->cust_fname . " " . $detail->cust_lname}}</td>
                                    <td>{{$detail->reserv_guestNo}}</td>
                                    <td>{{$detail->contNo}}</td>
                                    <td>{{$detail->place}}</td>
                                    <td>{{$detail->reserv_date}}</td>
                                    <td>{{$detail->reserv_time}}</td>
                                    <td>{{ config()->get('constants.status')[$detail->status] }}</td>
                                    <td>
                                        @if($detail->status == 0 )
                                        <button id ="sizes1" class="btn btn-primary fa fa-check-circle paid_down" data-id="{{$detail->reserv_id}}"> Pay downpayment</button></br>
                                        <button id ="sizes1" class="btn btn-danger fa fa-times-circle-o  first_cancel" data-id="{{$detail->reserv_id}}"> Cancel</button>
                                        @elseif($detail->status == 2)
                                        <button id ="sizes1" class="btn btn-primary fa fa-check-circle paid_full" data-id="{{$detail->reserv_id}}"> Pay in Full</button>
                                        <button id ="sizes1" class="btn btn-danger fa fa-times-circle-o  refund_cancel" data-id="{{$detail->reserv_id}}"> Cancel with refund</button>
                                        @elseif($detail->status == 3)
                                        <button id ="sizes1" class="btn btn-success fa fa-check-circle event_done" data-id="{{$detail->reserv_id}}"> Event Done</button>
                                        <button id ="sizes1" class="btn btn-danger fa fa-times-circle-o  cancel_norefund" data-id="{{$detail->reserv_id}}"> Cancel without refund</button>
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--main-container-part-->
<script src="{{ asset('js/jquery.min.js') }}"></script>

@endsection 

@section('modal')
<!-- status 2 -->
<div class="modal fade" tabindex="-1" role="dialog" id="paid_fullModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Pay in Full</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/paid_full">
                    {{csrf_field()}}
                    <input class="id" type="hidden" name="id" id="paid_fullID">
                    <div class="form-group">
                        <h4>Pay in full</h4>
                    </div>
                    <div class="form-group">
                        <label>Total Amount to pay</label>
                        <input type="text" class="form-control total_pay" disabled>
                    </div>
                    <div class="form-group">
                        <label>First Amount Paid</label>
                        <input type="text" class="form-control amount_paid" disabled>
                    </div>
                    <div class="form-group">
                        <label>Amount Paid</label>
                        <input type="number" class="form-control" name="paid" required>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Confirm</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="cancel_refundModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Event Cancellation with Refund</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/refund_cancel">
                    {{csrf_field()}}
                    <input type="hidden" class="id" name="id" id="refund_cancelID">
                    <div class="form-group">
                        <h4>Cancel event?</h4>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Confirm</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end status 2 -->
@endsection

@section('script')
<script type="text/javascript">
    // status 2
    $('.paid_full').click(function () {
        $.ajax({
            type: "get",
            url: '/get_Paid',
            data: {
                id: $(this).data('id')
            },
            dataType: "json",
            success: function (response) {
                response.forEach(function (data) {
                    $('#paid_fullModal .id').val($(this).data('id'));
                    $('#paid_fullModal .total_pay').val(data.total_pay);
                    $('#paid_fullModal .amount_paid').val(data.amount_paid);
                })
            }
        });
        $('#paid_fullID').val($(this).data('id'));
        $('#paid_fullModal').modal('show');
    });

    $('.refund_cancel').click(function () {
        $('#refund_cancelID').val($(this).data('id'));
        $('#cancel_refundModal').modal('show');
    });
    // end status 2
</script>
@endsection