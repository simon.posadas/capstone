<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon;

class Reservation extends Model
{
    protected $table = 'reservations';
    /**
     * Uniq identifier
     * @var type 
     */
    protected $primaryKey = 'id';
    
    /**
     * Remove automatic stamping
     * @var type 
     */
    public $timestamps = true;
    
    /**
     * Variable mass assignement
     * @var type 
     */
    protected $fillable = [
        'code', 
        'guest_count', 
        'event_date', 
        'details', 
        'details', 
        'status', 
        'total'
    ];

    public $status_list = [
        0 => 'Pending',
        1 => 'Cancelled by the Customer',
        2 => 'Paid (Downpayment)',
        3 => 'Paid (Complete)',
        4 => 'Cancel (Refunded)',
        5 => 'Cancelled without Refund',
        6 => 'Done',
        7 => 'Cancelled with Service Charge',
        8 => 'Paid (Downpayment with Partial Payment)'
    ];

    protected $appends = [
        'status_name', 'paid_amount'
    ];

    public function getPaidAmountAttribute()
    {
        return $this->payments()->sum('amount');
    }

    public function getStatusNameAttribute()
    {
        if( isset( $this->status_list[$this->status] ) )
        {
            return $this->status_list[$this->status];
        }

        return $this->status_list[0];
    }

    public function payments()
    {
        return $this->hasMany('App\Payment', 'reservation_id', 'id');
    }
    
    public function foods()
    {
        return $this->belongsToMany('App\Food', 'reservation_food', 'reservation_id', 'food_id');
    }
    
    /**
     * Make a model that connects to event detail
     * @return type
     */
    public function event(){
        return $this->hasMany('App\Event', 'event_id', 'id');
    }

    /**
     * Make a relation with customer info
     * @return type
     */
    public function customer(){
        return $this->belongsTo('App\Person', 'customer_id', 'id');
    }

    public function billings()
    {
        return $this->hasMany('App\Billing', 'reservation_id', 'id');
    }
    
    /**
     * Make a relation with package detail
     * @return type
     */
    public function packages(){
        return $this->belongsTo('App\Package', 'package_id', 'id');
    }
    
    /**
     * Get the list of equipment in reservation
     * @return type
     */
    public function equipments(){
        return $this->belongsToMany('App\Equipment', 'reservation_equipment', 'reserve_id', 'equipment_id')
        ->withPivot('quantity', 'reserved', 'release', 'returned')->withTimestamps();
    }
    
    /**
     * Get the list of worker in reservation
     * @return type
     */
    public function worker(){
        return $this->belongsToMany('App\Worker', 'reservation_worker', 'reservation_id', 'worker_id')
                ->withPivot('id');
    }

    public function rules()
    {
        return [
            'guest' => '',
            'event date' => '',
            'event type' => '',
            'food type' => '',
            'location' => ''
        ];
    }

    public function deleteRules()
    {
        return [
            'reservation' => 'required|exists:reservations,id'
        ];
    }

    public function generateCode()
    {
        $count = 1;
        $date = Carbon\Carbon::now();
        $constant = 'RES-' . $date->format('y') . '-' . $date->format('m') . '-';
        $id = Reservation::orderBy('id', 'desc')->pluck('id')->first() + 1;

        if(!isset($id) || $id == null || $id == '') $id = 0;

        return $constant . $id;
    }
           
}
