<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicePackageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_package', function (Blueprint $table) { 
            $table->integer('service_id')->unsigned(); 
            $table->integer('package_id')->unsigned(); 
            $table->foreign('service_id')
            ->references('id')
            ->on('service_types')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->foreign('package_id')
            ->references('id')
            ->on('packages')
            ->onUpdate('cascade')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_package');
    }
}
