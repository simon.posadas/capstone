<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Hash;
use DB;
use Validator;

class SessionsController extends Controller
{

    public function getHomePage(Request $request)
    {
        if(!Auth::check())
        {
            return view('login');
        }

        return view('dashboard/index');
    }

    public function getLogin(Request $request)
    {

    	if(Auth::check())
		{
			return redirect('/');
		}

    	return view('login');

    }

    public function login(Request $request)
    {

        $username = $request->get('username');
        $password = $request->get('password');

        $array = [
            'username' => $username,
            'password' => $password
        ];

        if(Auth::attempt($array))
        {
            return redirect('/');
        }

        alert()->error('Incorrect username or password', 'Error')->persistent('Close');
        return redirect('login');

    }

    public function getRegistrationForm(Request $request)
    {
        return view('register');
    }

    public function register(Request $request)
    {

        $username = $request->get('username');
        $password = $request->get('password');
        $password_confirm = $request->get('password_confirm');

        $validator = Validator::make($request->all(), [
            'username' => 'required|max:16',
            'password' => 'required|min:8',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        } elseif ($password == $password_confirm) {
            $password = Hash::make($password);

            DB::table('users')->insert([
                'username' => $username,
                'password' => $password
            ]);
            alert()->success('Successfully made an account', 'Success')->persistent('Close');
            return redirect('/');
        } else {
            return back()
                ->withErrors('Passwords did not match')
                ->withInput();
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }
}
