@extends('layouts.sidebar')
@include('/layouts/css')

@section('title', 'Reservation Management')

@section('content')

<!--main-container-part-->
<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="fa fa-home"></i> Home</a>
            <a href="#" class="current">Reservation Management</a>
        </div>
        <h1>Reservation Management</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="pull-left">
            <a href="/admin/reservation_create">
                <button class="btn btn-success btn-large fa fa-plus"> Create Reservation</button>
            </a>
        </div>

        <div class="pull-right">
            <a href="/admin/eventtype">
                <button class="btn btn-info btn-large">Event Type</button>
            </a>
        </div>
        <form method="get" action="/searchReserve" role="search">
            <div class="pull-right">   
                <div class="input-group custom-search-form">
                    <input type="text" name="searchReserve" class="form-control" placeholder="Search" id="search_bar">
                    <button class="btn btn-info" id="adjust-height-right" type="submit"><i class="fa fa-search"></i> Search</button>
                </div>
            </div>
        </form>
        <div class="row-fluid">
            <div class="colspan">
                <div class="widget-box">
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Customer Name</th>
                                    <th>Guest Number</th>
                                    <th>Contact Number</th>
                                    <th>Place</th>
                                    <th>Event Date</th>
                                    <th>Event Time</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                            </thead>
                            @foreach($details as $detail)
                            <tbody>
                                <tr>
                                    <td>{{$detail->cust_fname . " " . $detail->cust_lname}}</td>
                                    <td>{{$detail->reserv_guestNo}}</td>
                                    <td>{{$detail->contNo}}</td>
                                    <td>{{$detail->place}}</td>
                                    <td>{{$detail->reserv_date}}</td>
                                    <td>{{$detail->reserv_time}}</td>
                                    <td>{{ config()->get('constants.status')[$detail->status] }}</td>
                                    <td class="btn-center">
                                        @if($detail->status == 0 )
                                        <button id ="sizes1" class="btn btn-primary fa fa-check-circle paid_down" data-id="{{$detail->reserv_id}}"> Pay downpayment</button></br>
                                        <button id ="sizes1" class="btn btn-danger fa fa-times-circle-o  first_cancel" data-id="{{$detail->reserv_id}}"> Cancel</button>
                                        @elseif($detail->status == 2)
                                        <button id ="sizes1" class="btn btn-primary fa fa-check-circle paid_full" data-id="{{$detail->reserv_id}}"> Pay in Full</button>
                                        <button id ="sizes1" class="btn btn-danger fa fa-times-circle-o  refund_cancel" data-id="{{$detail->reserv_id}}"> Cancel with refund</button>
                                        @elseif($detail->status == 3)
                                        <button id ="sizes1" class="btn btn-success fa fa-check-circle event_done" data-id="{{$detail->reserv_id}}"> Event Done</button>
                                        <button id ="sizes1" class="btn btn-danger fa fa-times-circle-o  cancel_norefund" data-id="{{$detail->reserv_id}}"> Cancel without refund</button>
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--main-container-part-->
<script src="{{ asset('js/jquery.min.js') }}"></script>

@endsection 

@section('modal')
<!-- status 0 -->
<div class="modal fade" tabindex="-1" role="dialog" id="paid_firstModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Downpayment</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/paid_first">
                    {{csrf_field()}}
                    <input type="hidden" class="id" name="id" id="paid_downID" required>
                    <div class="form-group">
                        <h4>Pay downpayment</h4>
                    </div>
                    <div class="form-group">
                        <label>Paid Amount</label>
                        <input type="number" class="form-control inpname" name="paid" required>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Confirm</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="cancel_firstModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Event Cancellation</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/first_cancel">
                    {{csrf_field()}}
                    <input type="hidden" class="id" name="id" id="first_cancelID">
                    <div class="form-group">
                        <h4>Cancel event?</h4>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Confirm</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end status 0 -->

<!-- status 3 -->
<div class="modal fade" tabindex="-1" role="dialog" id="event_doneModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Done</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/event_done">
                    {{csrf_field()}}
                    <input class="id" name="id" id="event_doneID">
                    <div class="form-group">
                        <h4>Event is done?</h4>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Confirm</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="cancel_norefundModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Event Cancellation with Refund</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/norefund_cancel">
                    {{csrf_field()}}
                    <input type="hidden" class="id" name="id" id="norefund_cancelID">
                    <div class="form-group">
                        <h4>Cancel event?</h4>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Confirm</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end status 3 -->
@endsection

@section('script') 
<script type="text/javascript">
    // status 0
    $('.paid_down').click(function () {
        $('#paid_downID').val($(this).data('id'));
        $('#paid_firstModal').modal('show');
    });

    $('.first_cancel').click(function () {
        $('#first_cancelID').val($(this).data('id'));
        $('#cancel_firstModal').modal('show');
    });
    // end status 0

    

    // status 3
    $('.event_done').click(function () {
        $('#event_doneID').val($(this).data('id'));
        $('#event_doneModal').modal('show');
    });

    $('.cancel_norefund').click(function () {
        $('#norefund_cancelID').val($(this).data('id'));
        $('#cancel_norefundModal').modal('show');
    });
    // end status 3
</script>
@endsection