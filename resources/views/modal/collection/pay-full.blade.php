<div class="modal fade" tabindex="-1" role="dialog" id="fullPaymentModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Pay Balance</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/paid_full">
                    {{csrf_field()}}
                    <input class="id" type="hidden" name="id" id="paid_fullID">
                    <div class="form-group">
                        <h4>Pay Balance</h4>
                    </div>
                    <div class="form-group">
                        <label>Total Amount Paid: <span id="amount_paid"></span></label>
                    </div>
                    <div class="form-group">
                        <label>Total Amount to pay: <span id="total_pay"></span></label>
                    </div>
                    <div class="form-group">
                        <label>Payee: </label>
                        <input type="text" class="form-control" name="payee" required>
                    </div>
                    <div class="form-group">
                        <label>Amount Paid: </label>
                        <input type="text" class="form-control" name="paid" required>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Confirm</button>
            </div>
            </form>
        </div>
    </div>
</div>