<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon;

class Billing extends Model
{
    protected $table = 'billings';
    protected $primaryKey = 'id';

    public function rules()
    {
        $billing_types = implode( ',', array_keys(self::$types));
    	return [
    		'description' => 'required',
    		'amount' => 'required',
            'reservation' => 'required|exists:reservations,id',
            'type' => 'required|in:' . $billing_types
    	];
    }

    public static $types = [
        0 => 'reservation',
        1 => 'items',
        2 => 'others'
    ];

    public function deleteRules()
    {
        return [
            'billing' => 'required|exists:billings,id'
        ];
    }

    public $status_list = [
        0 => 'Unpaid',
        1 => 'Paid'
    ];

    protected $appends = [
        'status_name'
    ];

    public function getBillingTypeName()
    {
        $type = isset( self::$types[ $this->billing_type ] ) ? self::$types[ $this->billing_type ] : null ;
        return ucfirst($type);
    }

    public function reservation()
    {
        return $this->belongsTo('App\Reservation', 'reservation_id', 'id');
    }

    public function getStatusNameAttribute()
    {
        if( isset( $this->status_list[$this->status] ) )
        {
            return $this->status_list[$this->status];
        }

        return $this->status_list[0];
    }

    public function generateCode()
    {
        $count = 1;
        $date = Carbon\Carbon::now();
        $constant = 'BILL-' . $date->format('y') . '-' . $date->format('m') . '-';
        $id = Billing::orderBy('id', 'desc')->pluck('id')->first() + 1;

        if(!isset($id) || $id == null || $id == '') $id = 0;

        return $constant . $id;
    }
}
