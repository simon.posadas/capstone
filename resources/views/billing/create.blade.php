@extends('layouts.template')

@section('content')
<!--main-container-part-->
<div id="content">

    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="icon icon-home"></i> Home</a>
            <a href="/billing">Billing</a>
            <a href="#" class="current">Create</a>
        </div>
        <h1>Create Bill</h1>
    </div>

    <div class="container-fluid">
        <hr>
        
        <form method="post" action="{{ url('billing') }}" class="form-horizontal">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Reservation Code</label>
                    <select class="form-control" name="reservation" id="reservation">
                    @forelse($reservations as $key=>$value)
                        <option value="{{ $key }}" @if(old('reservation') == $key ) checked @endif>
                            {{ $value }}
                        </option>
                    @empty
                        <option value="">Empty</option>
                    @endforelse
                    </select>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="form-group">
                    <label>Billing Type</label>
                    <select class="form-control" name="billing_type" id="billing_type">
                    @foreach($billing_types as $key=>$type)
                        <option value="{{ $key }}" @if(old('billing_type') == $key) checked @endif>
                            {{ ucfirst($type) }}
                        </option>
                    @endforeach
                    </select>
                </div>
            </div>
            
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Description</label>
                    <textarea class="form-control" name="description" id="description" value="{{ old("description") }}"></textarea>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="form-group">
                    <label>Amount</label>
                    <input type="text" class="form-control" name="amount" id="amount" value="{{ old("amount") }}" />
                </div>
            </div>

            <div class="col-sm-12">
                <div class="form-group">
                    <button type="submit" name="submit" class="btn btn-primary pull-right">Create</button>
            </div>
        </form>
    </div>
</div>
@endsection
  