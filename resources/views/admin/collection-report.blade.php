@extends('layouts.sidebar')

@section('title', 'WIP')

@section('content')

<!--main-container-part-->
<meta name="csrf-token" content="{{ csrf_token() }}">
<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="fa fa-home"></i> Home</a>
            <a href="#" class="current">Collection Report</a>
        </div>
        <h1>Collection Report</h1>
    </div>
    <div class="container-fluid">
        <form class="form-horizontal">
             <div class="pull-right">
             <button id="right-btn1" class="btn btn-info btn-large Add_Contractor">Export</button>
        </div>
        <div class="pull-right">   
             <div class="input-group custom-search-form">
                <input type="text" name="searchFood" class="form-control" placeholder="Search" id="search_bar3">
                <button class="btn btn-info" id="adjust-height-right3" type="submit"><i class="fa fa-search"></i> Search</button>
             </div>
        </div>
            <div class="control-group">
                    <div class="controls">
                        <select id="dropdownz2" class="span4">
                            <option class="sizee" selected disabled>- Period -</option>
                            <option class="sizee">Daily</option>
                            <option class="sizee">Weekly</option>
                            <option class="sizee">Monthly</option>
                            <option class="sizee">Quarterly</option>
                            <option class="sizee">Anually</option>
                        </select>
                    </div>
               </div>
            </div>
        </form>
        <div class="row-fluid">
            <div class="colspan">
                <div class="widget-box bordered">
                        <table id="dashTable" class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th width="8%" >Reservation ID</th>
                                    <th>Date(?)</th>
                                    <th>Customer Name</th>
                                    <th>No.Of Guest</th>
                                    <th>Package</th>
                                    <th>Total Price</th>
                                    <th>Initial Payment</th>
                                    <th>Balance</th>
                                    <th>Full Payment</th>
                                    <th>Status</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection 