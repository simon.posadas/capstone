@extends('layouts.template') 

@section('content')
<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="icon icon-home"></i> Home</a>
            <a href="/monitoring">Item Monitoring</a>
            <a href="{{ url("/monitoring/items/$reservations->id") }}">Item List</a>
            <a href="#" class="current">Item Tagging</a>
        </div>
        <h1>Reservation Items: Tagging</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="pull-right">
            <a href="{{ url("/monitoring/items/$reservations->id") }}" class="btn btn-info btn-large">Back</a>
        </div>
        <div class="pull-left">
            <div class="input-group custom-search-form">
                <label>Customer: {{ $reservations->customer->firstname }} {{ $reservations->customer->lastname }}</label>
                <input id="ReservationID" type="hidden" value="{{ $reservations->id }}"/>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="colspan">
            <div class="widget-box">
                <div class="widget-content nopadding">
                    <table class="table table-bordered data-table" id="tagTable">
                        <thead>
                            <tr>
                                <th>Item ID</th>
                                <th>Item Name</th>
                                <th>Quantity</th>
                                <th style="width: 10%;">TAG</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($items as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td style="text-align: right;">{{ $item->quantity }}</td>
                                    <td style="text-align: center;">
                                        <button class="btnTagItem" data-toggle="modal" data-target="#modalTagQuantity"><i class="icon icon-tag"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Tag Quantity -->
<div class="modal fade" id="modalTagQuantity" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Quantity</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="control-label col-md-3">How Many:</label>
                    <div class="col-md-3">
                        <input id="quantity" type="number" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="btnSaveTag" class="btn btn-success">Save</button>
                <button id="btnCloseModal" class="btn btn-danger">Cancel</button>
            </div>
        </div>

    </div>
</div>
@endsection

@section('scripts_include')
    <script type="text/javascript">
        var ReservationID;
        var ItemID;
        var TagQuantity;
        var RemainingQuantity;
        $('#btnCloseModal').click(function () {
            $('#modalTagQuantity').modal('hide');
        });

        $('.btnTagItem').click(function () {
            ReservationID = $('#ReservationID').val();
            var $row = $(this).closest("tr");
            ItemID = $row.find("td:nth-child(1)");
            var $row = $(this).closest("tr");
            RemainingQuantity = $row.find("td:nth-child(3)");
        });

        $('#btnSaveTag').click(function () {
            TagQuantity = $('#quantity').val();

            if(parseInt(TagQuantity) > parseInt(RemainingQuantity.text())) {
                alert('Remaining stock is not enough!');
                return false;
            }

            var data = {
                reservation_id: ReservationID,
                item_id: ItemID.text(),
                quantity: TagQuantity
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: '/monitoring/items/tagging',
                data: JSON.stringify(data),
                success: function (result) {
                    if(result.status) {
                        alert('Successfully tagged item.')
                        location.reload();
                    }else {
                        console.log(result.error);
                    }
                },
                error: function (jqXHR, exception) {
                    var msg = '';
                    if (jqXHR.status === 0) {
                        msg = 'Not connect.\n Verify Network.';
                    } else if (jqXHR.status == 404) {
                        msg = 'Requested page not found. [404]';
                    } else if (jqXHR.status == 500) {
                        msg = 'Internal Server Error [500].';
                    } else if (exception === 'parsererror') {
                        msg = 'Requested JSON parse failed.';
                    } else if (exception === 'timeout') {
                        msg = 'Time out error.';
                    } else if (exception === 'abort') {
                        msg = 'Ajax request aborted.';
                    } else {
                        msg = 'Uncaught Error.\n' + jqXHR.responseText;
                    }
                    alert(msg);
                }
            });
        });

        $(document).ready(function(){
            $('#tagTable').dataTable({
                searching   :   false
            });
        });
    </script>
@endsection 