@extends('layouts.template') 

@section('content')

<!--main-container-part-->
<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="icon icon-home"></i> Home</a>
                <a href="/collection">Collection</a>
                <a href="#" class="current">Refund</a>
        </div>
        <h1>Refund</h1>
    </div>
    <!--End-breadcrumbs-->
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Code</th>
                                    <th>Remaining Balance</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                    <th>Generate Refund Form</th>
                                </tr>
                            </thead>
                            @foreach($reservations as $reservation)
                            <tbody>
                                <tr>
                                    <td>{{ $reservation->id }}</td>
                                    <td>{{ $reservation->code }}</td>
                                    <td style="text-align: right">&#8369; {{ $reservation->total - $reservation->payments->sum('amount') }}</td>
                                    <td>{{ $reservation->status_name }}</td>
                                   <td class="btn-center">
                                        @if($reservation->status == 0)

                                        <form method="get" action="{{ url("collection/$reservation->id/billing") }}">
                                            <button type="submit" name="pay" value="{{ csrf_token() }}" id ="sizes" class="btn btn-primary fa fa-trash">&nbsp;Billings</button>
                                        </form>

                                        <form method="get" action="{{ url("collection/$reservation->id/payment") }}">
                                            <button type="submit" name="pay" value="{{ csrf_token() }}" id ="sizes" class="btn btn-success fa fa-trash">&nbsp;Payments</button>
                                        </form>

                                        @elseif($reservation->status == 3)

                                            @if($days2 >= Carbon\Carbon::parse($reservation->event_date)->format('Y-m-d'))
                                            <button id ="sizes1" class="btn btn-danger fa fa-times-circle-o cancel_norefund" data-id="{{$reservation->id}}"> Cancel without refund</button>
                                            @else
                                            <button id ="sizes1" class="btn btn-danger fa fa-times-circle-o charge_cancel" data-id="{{$reservation->id}}"> Cancel with refund</button>
                                            @endif

                                        @elseif($reservation->status == 2)
                                            <button id ="sizes1" class="btn btn-danger fa fa-times-circle-o refund_cancel" data-id="{{$reservation->id}}"> Cancel with refund</button>
                                        @endif
                                    </td>
                                    @if($reservation->status == 4 || $reservation->status == 5)
                                    <td>
                                        <a method="get" id="sizes1" class="btn btn-primary" href="{{ route('Refund', ['id' => $reservation-> id, 'id2' =>$reservation-> customer_id]) }}">Refund Form</a>
                                    </td>
                                    @else
                                    <td>
                                        <a method="get" id="sizes1" class="btn btn-primary" disabled>Refund Form</a>
                                    </td>
                                    @endif
                                </tr>
                            </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end-main-container-part-->

@endsection 

@section('modal')
<!-- status2 -->
<div class="modal fade" tabindex="-1" role="dialog" id="cancel_refundModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Event Cancellation with Refund</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="{{ url('refund_cancel') }}">
                    {{csrf_field()}}
                    <input type="hidden" class="id" name="id" id="refund_cancelID">
                    <div class="form-group">
                        <h4>Cancel event?</h4>
                    </div>
                    <div class="form-group">
                        <label>Receiver of refund: </label>
                        <input type="text" class="form-control" name="ror" required>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Confirm</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end status 2 -->

<!-- status 3 -->
<div class="modal fade" tabindex="-1" role="dialog" id="cancel_norefundModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Event Cancellation without Refund</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="{{ url('norefund_cancel') }}">
                    {{csrf_field()}}
                    <input type="hidden" class="id" name="id" id="norefund_cancelID">
                    <div class="form-group">
                        <h4>Cancel event?</h4>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Confirm</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end status 3 -->

<!-- charge -->
<div class="modal fade" tabindex="-1" role="dialog" id="charge_cancelModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Refund with Service charge</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="{{ url('charge_cancel') }}">
                    {{csrf_field()}}
                    <input type="hidden" class="id" name="id" id="charge_cancelID">
                    <div class="form-group">
                        <h4>Cancel event?</h4>
                    </div>
                    <div class="form-group">
                        <label>Receiver of refund: </label>
                        <input type="text" class="form-control" name="ror" required>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Confirm</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end charge -->
@endsection

@section('script')
<script type="text/javascript">
    // status 2
    $('.refund_cancel').click(function () {
        $('#refund_cancelID').val($(this).data('id'));
        $('#cancel_refundModal').modal('show');
    });
    // end status 2

    // status 3
    $('.cancel_norefund').click(function () {
        $('#norefund_cancelID').val($(this).data('id'));
        $('#cancel_norefundModal').modal('show');
    });
    // end status 3

    // charge 
    $('.charge_cancel').click(function () {
        $('#charge_cancelID').val($(this).data('id'));
        $('#charge_cancelModal').modal('show');
    });
    // end charge
</script>
@endsection