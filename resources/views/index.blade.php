@extends('layouts.template') 

@section('title', 'Dashboard') 

@section('content')
<!--main-container-part-->
<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="fa fa-home"></i> Home</a>
        </div>
        <h1>Dashboard</h1>
    </div>
    <!--End-breadcrumbs-->
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end-main-container-part-->

@endsection 
