
@extends('layouts.printables.report') 

@section('content') 
  <style> 
      th , tbody{ 
        text-align: center; 
      } 
 
      #content{ 
        font-family: "Times New Roman"; 
      } 
 
      @media print { 
          tr.page-break  { display: block; page-break-after: always; } 
      }    
 
  </style> 

  <div id="content" class="col-sm-12"> 
    <h4 class="text-center"> <small class="pull-right"></small></h4> 
    <table class="table table-striped table-bordered table-condensed" id="inventoryTable" width="100%" cellspacing="0"> 
      <thead> 
          @php 
            $division = 3; 
          @endphp 
          <tr rowspan="2"> 

            <div>
                      <h2>Collection Report</h2>
            </div>

              <th class="text-left" colspan="{{ $division }}"> 
                <span style="font-weight:normal"></span>  
              </th> 
              <th class="text-left" colspan="{{ $division }}"> 
                <span style="font-weight:normal"></span> 
              </th> 
          </tr> 
          <tr>  
            <th>Customer Name</th> 
            <th>No. of Guest</th> 
            <th>Food Package Price</th>  
            <th>Initial Payment</th> 
            <th>Balance</th> 
            <th>Final Payment</th> 
            <th>Status</th>
            <th>Amount</th> 
          </tr> 
      </thead> 
      <tbody> 
 
      </tbody> 
    </table> 
  </div> 
 
   
@endsection
