@extends('layouts.sidebar')

@section('title', '5')

@section('content')
<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="icon icon-home"></i> Home</a>
            <a href="#" class="current">Package Name Here</a>
        </div>
        <h1>Package Name Here</h1>
        <hr>
    </div>

 	<div class="container-new"> 	
	
        <button class="btn-1">SAMPLE 1</button>
              <div class="contentz">
                <div class="checkbox">
                     <label><input type="checkbox" value="">Option 1</label>
                </div>
                
                <div class="checkbox">
                    <label><input type="checkbox" value="">Option 1</label>
                </div>

                <div class="checkbox">
                    <label><input type="checkbox" value="">Option 1</label>
                </div>
              </div>

            <button class="btn-1">SAMPLE 2</button>
            <div class="contentz">
                <p>hayaan mo sila</p>
                <p>hayaan mo sila</p>
            </div>

            <button class="btn-1">SAMPLE 3</button>
            <div class="contentz">
                <p>hayaan mo sila hayaan mo silahayaan mo silahayaan mo silahayaan mo silahayaan mo silahayaan mo sila</p>
                <p>hayaan mo silahayaan mo silahayaan mo silahayaan mo silahayaan mo silahayaan mo silahayaan mo sila</p>
                <p>hayaan mo silahayaan mo silahayaan mo silahayaan mo silahayaan mo silahayaan mo silahayaan mo sila</p>
            </div>
    </div>
@endsection


@section('script')
<script type="text/javascript">

    var accordions = $('.btn-1');

    for (var i = 0; 1 < accordions.length; i++) {
        accordions[i].onclick = function() {
            this.classList.toggle('is-open')
            var content = this.nextElementSibling;

            if (content.style.maxHeight) {
                content.style.maxHeight = null;
            } else {
                content.style.maxHeight = content.scrollHeight + "px";
            }
        }
    }
</script>

@endsection