<?php

use Illuminate\Database\Seeder;

class EquipmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('equipments')->insert([
        	[
        		'name' => 'Chair',
        		'quantity' => '100',
        		'status' => '1',
        		'type_id' => '1',

        	],

        	[
				'name' => 'Round Table',
        		'quantity' => '100',
        		'status' => '1',
        		'type_id' => '2',
        	],

        	[
        		'name' => 'Square Table',
        		'quantity' => '100',
        		'status' => '1',
        		'type_id' => '2',
        	],

            [
                'name' => 'Spoon',
                'quantity' => '100',
                'status' => '1',
                'type_id' => '3',
            ],

            [
                'name' => 'Fork',
                'quantity' => '100',
                'status' => '1',
                'type_id' => '3',
            ]
        ]);
    }
}
