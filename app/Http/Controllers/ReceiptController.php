<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Validator;
use DB;

class ReceiptController extends Controller
{
    public function receipt($id, $id2)

    {
        $customer_id = App\Person::select('customers.id as customer')
            ->join('reservations', 'reservations.customer_id', 'customers.id')
            ->where('reservations.id', $id2)
            ->get();
        // dd($customer_id);
        $customer_id = $customer_id[0]->customer;
        $reservation = app\Reservation::find($id2);
        $customer = App\Person::find($customer_id);
        $package = App\Package::find($reservation->package_id);
        $food = DB::table('reservation_food')->where('reservation_id', '=', $id2)->pluck('food_id');
        $food  = App\Food::whereIn('id', $food)->get();
        $billing = DB::table('billings')->where('reservation_id', '=', $id2)->get();

		$service_charge = $reservation->total;
        $percentage = $service_charge * 0.1 ;
        $grand_total = $service_charge * 1.1;


        $data = [
        	'reservation' => $reservation,
            'billing' => $billing,
            'customer' => $customer,
            'packages' => $package,
            'food' => $food,
            'service_charge' => $service_charge,
            'percentage' => $percentage,
            'grand_total' => $grand_total,

        ];

        

        $filename = "Receipt-".\Carbon\Carbon::now()->format('mdYHm').".pdf";
        $view = "billing.print.receipt";

        return view($view)
                ->with($data)
                ->with($filename);

    }


public function otherreceipt($id, $id2)
    {
        $customer_id = App\Person::select('customers.id as customer')
            ->join('reservations', 'reservations.customer_id', 'customers.id')
            ->where('reservations.id', $id2)
            ->get();
        // dd($customer_id);
        $customer_id = $customer_id[0]->customer;
        $reservation = app\Reservation::find($id2);
        $customer = App\Person::find($customer_id);
        $package = App\Package::find($reservation->package_id);
        $food = DB::table('reservation_food')->where('reservation_id', '=', $id2)->pluck('food_id');
        $food  = App\Food::whereIn('id', $food)->get();
        $billing = App\Billing::find($id);

        // dd($id2);

        $data = [
            'reservation' => $reservation,
            'billing' => $billing,
            'customer' => $customer,
            'packages' => $package,


        ];

        $filename = "Receipt-".\Carbon\Carbon::now()->format('mdYHm').".pdf";
        $view = "billing.print.otherreceipt";

        return view($view)
                ->with($data)
                ->with($filename);

    }

    



}
