<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Hash;

class LoginController extends Controller
{
	public function doRegister(Request $request){
        $username = $request->get('username');
        $password = $request->get('password');

        $password = Hash::make($password);

        DB::table('users')->insert([
            'username' => $username,
            'password' => $password
        ]);

        return redirect('/');
	}

    public function doLogin(Request $req){

        // $user = $req->input('username');
        // $pass = $req->input('password');

        // if(Auth::attempt(['username'=> $user ,'password'=> $pass ])){
            return redirect('/dashboard');  
        // }
        // else{
        //     return redirect()->back()->with('message', ['text'=>'Invalid Username and/or Password.']);
        // } 
    }
}
