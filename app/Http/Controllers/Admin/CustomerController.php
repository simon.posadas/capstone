<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Model\CustomerInfo;
use App\Http\Requests\Reservation\ReservationIdRequest;
use Notification;
use App\Notifications\ReservationApprove;
use App\Notifications\ReservationDisapprove;
use App\Http\Controllers\Controller;

/**
 * 
 */
class CustomerController extends Controller {
    /**
     * View directory
     * @var type 
     */
    protected $view = 'admin.';

    // CUSTOMER
    public function customer(){
        $cust = CustomerInfo::get();
        return view($this->view . 'customer', ['cust' => $cust ]);
    }

    public function getFood(Request $req){
        dd('flkjasldf');
        // $type = FoodDetail::select('*')
        //     ->where('food_id',$req->id)
        //     ->get();
        // dd($type);
        return response()->json($types);
    }

    public function addFoodCat(){
        $category = FoodCategory::select('category_name')->get();
        $foodcat = $_POST['catname'];

        if($category == $foodcat){
            alert()->error('Food category already exists', 'Error')->persistent('Close');
        } else {
        FoodCategory::insert([ 
            'category_name' => $foodcat
            ]);
        alert()->success('Successfully added a food category', 'Success')->persistent('Close');
        }

        return redirect('/admin/foodcat');
    }

    public function editFoodCat(){
        FoodCategory::where('category_id', $_POST['id'])
            ->update(['category_name' => $_POST['catname']
        ]);
            
        alert()->success('Successfully edited a food category', 'Success')->persistent('Close');

        return redirect('/admin/foodcat');
    }
    
    public function deleteFoodCat(){
        if(FoodCategory::where('category_id', $_POST['id'])->delete()){
            FoodCategory::where('category_id', $_POST['id'])->delete();
            alert()->success('Successfully deleted a food category', 'Success')->persistent('Close');
        }else{
        alert()->error('Something went wrong deleting the food category', 'Error')->persistent('Close');
        }
        return redirect('/admin/foodcat');
    }
    // END CUSTOMER
}