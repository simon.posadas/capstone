<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Model\Equipment;
use App\Model\EquipmentType;
use App\Http\Requests\Reservation\ReservationIdRequest;
use Notification;
use App\Notifications\ReservationApprove;
use App\Notifications\ReservationDisapprove;
use App\Http\Controllers\Controller;

/**
 * 
 */
class EquipmentController extends Controller {
    /**
     * View directory
     * @var type 
     */
    protected $view = 'admin.';

    // EQUIPMENT
    public function equip(){
        $equip = Equipment::join('equipment_type as et', 'equipment.type_id', '=', 'et.type_id')
            ->get();
        $type = EquipmentType::get();
        return view($this->view . 'equipment', ['equip' => $equip,'type' => $type ]);
    }

    public function getEquip(Request $req){
        // dd('flkjasldf');
        $types = Equipment::select('*')
            ->where('equipment_id',$req->id)
            ->get();
        // dd($type);
        return response()->json($types);
    }

    public function addEquip(){
        $invname = Equipment::select('equipment_name')->get();
        $eqname  = $_POST['eqname'];


        if($invname == $eqname){
            alert()->error('Item already exists', 'Error')->persistent('Close');
        } else {
        Equipment::insert([ 
            'equipment_name' => $eqname,
            'type_id' => $_POST['equip_type'],
            'cost' => $_POST['price'],
            'quantity' => $_POST['quantity'],
            'status' => $_POST['status']
            ]);
        alert()->success('Successfully added an item', 'Success')->persistent('Close');
        }
        
        return redirect('/admin/equipment');
    }

    public function editEquip(){
        $invname = Equipment::select('equipment_name')->get();
        $eqname  = $_POST['eqname'];


        if($invname == $eqname){
            alert()->error('Item already exists', 'Error')->persistent('Close');
        } else {
            Equipment::where('equipment_id', $_POST['id'])
                ->update(['equipment_name' => $_POST['eqname'], 
                'cost' => $_POST['price'],
                'quantity' => $_POST['quantity']
            ]);   
            alert()->success('Successfully edited an item', 'Success')->persistent('Close');
        }
        return redirect('/admin/equipment');
    }
    
    public function deleteEquip(){
        if(Equipment::where('equipment_id', $_POST['id'])->delete()){
            Equipment::where('equipment_id', $_POST['id'])->delete();
            alert()->success('Successfully deleted an item', 'Success')->persistent('Close');
        }else{
        alert()->error('Something went wrong deleting the item', 'Error')->persistent('Close');
        }
        return redirect('/admin/equipment');
    }

     public function searchEquip(){

        $searchItem = $_GET["searchEquip"];

        $type = EquipmentType::get();

        $result = Equipment::where('equipment_name', 'LIKE', '%' .$searchItem  .'%')
            ->OrWhere('equipment_id', 'LIKE', '%' .$searchItem  .'%')
            ->OrWhere('quantity', 'LIKE', '%' .$searchItem  .'%')
            ->orWhere("cost", 'LIKE', '%' .$searchItem  .'%')
            ->select('equipment.*')->get();
        
        if(count($result) == 0){
            alert()->error('No result to display. Please try again.', 'Error')->persistent('Close');
        return redirect()->back();
        }

        return view('/admin/equipment')->with(['equip' => $result, 'type' => $type]);  
    }

   

    // END EQUIPMENT
}