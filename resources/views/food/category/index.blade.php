@extends('layouts.template') 

@section('content')

@include('modal.food.category.add')
@include('modal.food.category.edit')
@include('modal.food.category.delete')

<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="icon icon-home"></i> Home</a>
            <a href="{{ url('food') }}">Food</a>
            <a href="#" class="current">Food Category</a>
        </div>
        <h1>Food Categories</h1>
    </div>
    <div class="container-fluid">
        <hr>

        @if(session()->has('success-message'))
        <div class="alert alert-success">
            <ul>
                {{ session()->get('success-message') }}
            </ul>
        </div>
        @endif


        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="pull-left">
            <button class="btn btn-success btn-large icon icon-plus addFoodCat" data-toggle="modal" data-target="#addModal"> Add Food Category</button>
        </div>
        
        <div class="pull-right">
            <button class="btn btn-info btn-large" onClick="goBack()">Back</button>
        </div>

        <div class="row-fluid">
            <div id="table-margin" class="span12">
                <div class="widget-box">
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table" id="categoriesTable">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($categories as $category)
                                <tr>
                                    <td width=65%>{{ $category->id }}</td>
                                    <td width=65%>{{ $category->name }}</td>
                                    <td class="btn-center">
                                        <button id ="sizes" class="btn icon icon-pencil btn-primary edittype" data-id="{{ $category->id }}" data-name="{{ $category->name }}" data-toggle="modal" data-target="#editModal"> Edit</button>
                                        <button id ="sizes" class="btn btn-danger icon icon-trash deletetype" data-id="{{ $category->id }}" data-name="{{ $category->name }}" data-toggle="modal" data-target="#deleteModal"> Delete</button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('after_scripts')

<script type="text/javascript">

    $(document).ready(function(){

        $('#categoriesTable').on('click', '.edittype', function (event) {
            name = $(this).data('name')
            id = $(this).data('id')
            $('#editModal').find('input[name=name]').val(name);

            $('#editModal').find('#btn-edit').on('click', function () {

                $('#editForm').attr('action', function(i, value) {
                    return "{{ url('food/category') }}/" + id ;
                });

                $('#editForm').submit();
            });

        })

        $('#categoriesTable').on('click', '.deletetype', function (event) {
            id = $(this).data('id')

            $('#deleteModal').find('#btn-delete').on('click', function () {

                $('#deleteForm').attr('action', function(i, value) {
                    return "{{ url('food/category') }}/" + id ;
                });

                $('#deleteForm').submit();
            });

        })
    })

    function goBack(){
        window.location.href = "/food";
    }

    $('#categoriesTable').dataTable({
        searching   :   false
    })
</script>

@endsection   