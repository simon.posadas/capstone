<?php

use Illuminate\Database\Seeder;

class PackageLimitsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::table('package_limits')->insert([

        	[
        		'package_id' => '1',
        		'category_id' => '1',
        		'type_id' => '',
        		'amount' => '1',
        	],

        	[
        		'package_id' => '1',
        		'category_id' => '2',
        		'type_id' => '',
        		'amount' => '1',
        	],

        	[
        		'package_id' => '1',
        		'category_id' => '3',
        		'type_id' => '',
        		'amount' => '1',
        	],

            [
                'package_id' => '1',
                'category_id' => '4',
                'type_id' => '1',
                'amount' => '1',
            ],

            [
                'package_id' => '1',
                'category_id' => '4',
                'type_id' => '2',
                'amount' => '1',
            ],

            [
                'package_id' => '1',
                'category_id' => '4',
                'type_id' => '3',
                'amount' => '1',
            ],

            [
                'package_id' => '1',
                'category_id' => '4',
                'type_id' => '4',
                'amount' => '1',
            ],

            [
                'package_id' => '1',
                'category_id' => '4',
                'type_id' => '5',
                'amount' => '1',
            ],

            [
                'package_id' => '1',
                'category_id' => '4',
                'type_id' => '6',
                'amount' => '1',
            ],

            [
                'package_id' => '1',
                'category_id' => '4',
                'type_id' => '7',
                'amount' => '1',
            ],

            [
                'package_id' => '1',
                'category_id' => '5',
                'type_id' => '',
                'amount' => '1',
            ],

            [
                'package_id' => '1',
                'category_id' => '6',
                'type_id' => '',
                'amount' => '1',
            ]
        ]);

    }
}
