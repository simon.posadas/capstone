<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use DB;
use Validator;

class ItemsController extends Controller
{

    public function index2(){
        $items = App\Item::orderBy('name')
            ->get();
        return view('item.index')
            ->with('items', $items);
    }

    public function add(Request $request){
        $name = $request->get('name');
        $quantity = $request->get('quantity');
        $price = $request->get('price');
        
        $item = new App\Item;
        $item->name = $name;
        $item->quantity = $quantity;
        $item->price = $price;
        $item->save();



        alert()->success('Successfully deleted an item', 'Success')->persistent('Close');
        return redirect('/admin/item');
    }

    public function delete(Request $id){
        $id = $id->get('id');
        // $id = 'fjkasflsadl';

        if(App\Item::where('id', $id)->delete()){
            App\Item::where('id', $id)->delete();
            alert()->success('Successfully deleted an item', 'Success')->persistent('Close');
        }else{
        alert()->error('Something went wrong deleting the item', 'Error')->persistent('Close');
        }
        return redirect('/admin/item');
    }

    public function edit(Request $request){
        $id = $request->get('id');
        $name = $request->get('name');
        $quantity = $request->get('quantity');
        $price = $request->get('price');

        App\Item::where('id', $id)
                ->update([
                'name' => $name, 
                'quantity' => $quantity,
                'price' => $price
            ]);   
            alert()->success('Successfully edited an item', 'Success')->persistent('Close');
        return redirect('/admin/item');
    }
}
