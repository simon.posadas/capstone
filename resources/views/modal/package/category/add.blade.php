<!-- add modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="addModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add Food Category</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="{{ url("food/package/$package->id/limit") }}">
                    <div class="control-group">
                        <label class="control-label">Select food category</label>
                        <div class="controls select2-container">
                            <select name="category" id="category" onchange="enableCat()">
                                @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="control-group">
                        <label class="control-label">Select food type</label>
                        <div class="controls select2-container">
                            <select name="type" id="type">
                                @foreach($types as $type)
                                <option value="{{ $type->id }}">{{ $type->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="label-limit2">Limit :</label>
                        <input id="input-limit2" type="number" class="form-control" placeholder="Limit" name="limit">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Add</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end add modal -->