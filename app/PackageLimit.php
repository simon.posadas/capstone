<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageLimit extends Model
{
    /**
     * The table to fetch
     * @var type 
     */
    protected $table = 'package_limits';
    
     /**
     * Unique ID
     * @var type 
     */
    protected $primaryKey = 'id';

    /**
     * Variable mass assignement
     * @var type 
     */
    protected $fillable = ['package_id', 'category_id', 'amount ', 'type_id', ];

    protected $appends = [
        'type_name',
        'category_name'
    ];

    public function type()
    {
        return $this->belongsTo('App\FoodType', 'type_id', 'id');
    }

    // public function getTypeNameAttribute()
    // {
    //     return $this->type->name;
    // }

    public function categories()
    {
        return $this->belongsToMany('App\FoodCategory', 'category_food', 'category_id', 'food_id');
    }
    
    /**
     *
     * @var type Remove automatic generation of timestamp
     */
    public $timestamps = true;

    // public function rules()
    // {
    //     return [
    //         'name' => 'required|min:2|max:50|unique:foods,name',
    //         'category' => 'required|exists:categories,id',
    //         'type' => 'required|exists:food_types,id',
    //         'price' => ''
    //     ];
    // }

    // public function updateRules()
    // {
    //     $name = $this->name;
    //     return [
    //         'name' => 'required|min:2|max:50|unique:foods,name,' . $name . ',name' ,
    //         'type' => 'required|exists:food_types,id',
    //         'price' => ''
    //     ];
    // }

    // public function deleteRules()
    // {
    //     return [
    //         'Food' => 'required|exists:foods,id'
    //     ];
    // }

    // public function generateCode()
    // {
    //     $count = 1;
    //     $date = Carbon\Carbon::now();
    //     $constant = 'FOOD-' . $date->format('y') . '-' . $date->format('m') . '-';
    //     $id = PackageLimit::orderBy('id', 'desc')->pluck('id')->first() + 1;

    //     if(!isset($id) || $id == null || $id == '') $id = 0;

    //     return $constant . $id;
    // }
}
