<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Model\EventType;
use App\Http\Requests\Reservation\ReservationIdRequest;
use Notification;
use App\Http\Controllers\Controller;

/**
 * 
 */
class EventTypeController extends Controller {
    /**
     * View directory
     * @var type 
     */
    protected $view = 'admin.';

    // PACKAGES
    public  function eventtype()
    {
        $type = EventType::get();
        
        return view($this->view . 'eventtype', ['types' => $type]);
    }

    public function getType(Request $req){
        $types = EventType::where('type_id',$req->id)->get();
        // dd($type);
        return response()->json($types);
    }

    public function addType(){
        EventType::insert([ 
             'type_name' => $_POST['tname']
        ]);
        alert()->success('Successfully added an event type', 'Success')->persistent('Close');
        

        return redirect('/admin/eventtype');
    }

    public function editType(){
        EventType::where('type_id', $_POST['id'])
            ->update(['type_name' => $_POST['name']
        ]);
        
        alert()->success('Successfully edited an event type', 'Success')->persistent('Close');
        
        return redirect('/admin/eventtype');
    }

    public function deleteType(){
        
        EventType::where('type_id', $_POST['id'])->delete();
        alert()->success('Successfully deleted an event type', 'Success')->persistent('Close');
        
        return redirect('/admin/eventtype');
    }
    // END PACKAGES

}