@extends('layouts.login-template')

@section('after_styles')

<style>
    body {
        background: url("{{ asset('images/gradientBG.jpg')  }}");
        background-size: cover;
        padding: 0;
        margin-top: 10%;
    }
</style>
@endsection

@section('content')

<div id="loginbox">
    <form id="loginform" class="form-vertical" action="{{ url('login') }}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <div class="control-group normal_text">
            <h3>
                <img src="images/logo.png" alt="Logo" />
            </h3>
        </div>
        <div class="control-group">
            <div class="controls">
                <div class="main_input_box">
                    <span class="add-on bg_lg">
                        <i class="icon-user"> </i>
                    </span>
                    <input type="text" placeholder="Username" name="username" />
                </div>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <div class="main_input_box">
                    <span class="add-on bg_ly">
                        <i class="icon-lock"></i>
                    </span>
                    <input type="password" placeholder="Password" name="password" />
                </div>
            </div>
        </div>
        <div class="form-actions">
            <span id="center2">
                <button type="submit" class="btn btn-large btn-success" id="login"> Login</button>
                <button type="button" class="btn btn-large btn-info" onClick="reg()" id="register"> Register</button>
            </span>
        </div>
    </form>
    <form id="recoverform" action="#forgot" class="form-vertical">
        <p class="normal_text">Enter your e-mail address below and we will send you instructions how to recover a password.</p>

        <div class="controls">
            <div class="main_input_box">
                <span class="add-on bg_lo">
                    <i class="icon-envelope"></i>
                </span>
                <input type="text" placeholder="E-mail address" />
            </div>
        </div>

        <div class="form-actions" id="buttons">
            <span class="pull-left">
                <a href="#" class="flip-link btn btn-success" id="to-login">&laquo; Back to login</a>
            </span>
            <span class="pull-right">
                <a class="btn btn-info" />Recover</a>
            </span>
        </div>
    </form>
</div>
@endsection

@section('script')
<script>
function reg(){
    window.location.href = "register";
}
</script>
@endsection