<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservationEquipment extends Model
{
    /**
     * Table to connect
     * @var type 
     */
    protected $table = 'reservation_equipment';

    /**
     * Mass variable assignment
     * @var type 
     */
    protected $fillable = ['reserve_id', 'equipment_id', 'quantity', 'reserved', 'release', 'returned' ,'quantity_returned'];
    
    /**
     * Make a connection with reservation detail
     * @return type
     */
    public function reservation_detail(){
        return $this->belongsTo('App\Reservation', 'reservation_id', 'id');
    }
    
    /**
     * Make a relation with equipment model
     * @return type
     */
    public function equipment(){
        return $this->belongsTo('App\Equipment', 'equipment_id', 'equipment_id');
    }
}
