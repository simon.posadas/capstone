<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use DB;
use Validator;
use Carbon;

class ReservationsController extends Controller
{
    public $status_list = [];

    public function __construct(App\Reservation $reservation)
    {
        $this->status_list = $reservation->status_list;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reservations = App\Reservation::all();
        
        $days = Carbon\Carbon::now()
            ->addDays(5)
            ->format('Y-m-d');

        return view('reservation.index')
                ->with('days', $days)
                ->with('reservations', $reservations);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = App\EventType::orderBy('name')->get();
        $packages = App\Package::orderBy('name')->get();
        $events_data = [];

        $date = Carbon\Carbon::now()
        ->addDay(14)
        ->format('m-d-YYYY');

        return view('reservation.create')
                    ->with('types', $types)
                    ->with('packages', $packages)
                    ->with('date', $date)
                    ->with('events_data', $events_data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if($request->has('view-package'))
        {
            $package = $request->get('package');

            // $categories = db::table('package_lists_v')->where('package_id', $package)->get();

            // $limits = db::table('package_limits')->where('package_id', $package)->get();

            $categories = db::table('package_lists_v')
                            -> join('package_limits as p2','package_lists_v.type_id', '=', 'p2.type_id')
                            -> select([db::raw('distinct(package_lists_v.food_id)'), 
                                        'package_lists_v.package_id','p2.amount', 
                                        'package_lists_v.type_id',
                                        'package_lists_v.food_name']) 
                            -> where('package_lists_v.package_id', $package)
                            -> where('p2.package_id', $package)->get();        
                            
            $othercat = db::table("categories AS cate")
                            -> join("category_food AS cf", "cf.category_id", "=", "cate.id")
                            -> join("foods", "foods.id", "=", "cf.food_id")
                            -> join("package_limits AS pl", "pl.category_id", "=", "cate.id")
                            -> select("cate.id", "cate.name AS type_name", "foods.id AS food_id", "foods.name AS food_name", "pl.amount")
                            -> where('pl.package_id', $package)
                            -> get();

       
            return back()->withInput()->with([
                'categories' => $categories, 
                'othercat' => $othercat,
            ]);
        }

        $firstname = $request->get('firstname');
        $middlename = $request->get('middlename');
        $lastname = $request->get('lastname');
        $email = $request->get('email');
        $address = $request->get('address');
        $contact = $request->get('contact');

        $guest = $request->get('guest');
        $date = $request->get('reservedate');
        $time = $request->get('time');
        $event_type = $request->get('event_type');
        $location = $request->get('location');
        $total = $request->get('total');
        $package = $request->get('package');
        $food_type = $request->get('food_type');
        $service = $request->get('service');

        $foods = $request->get('food');
        $datetime = $date . " " . $time ;

        

        $food_list = []; 

        $packages = App\Package::find($package);
        $pack_price = $packages->price;
        $total_pay = $pack_price * $guest;
        $total_pay *= 1.1;

        $package_id = $packages->id;

        $customer = new App\Person;

        $validator = Validator::make([
            'firstname' => $firstname,
            'middlename' => $middlename,
            'lastname' => $lastname,
            'email' => $email,
            'address' => $address,
            'contact' => $contact
        ], $customer->rules());

        $reservation = new App\Reservation;

        $validator = Validator::make([
            'guest' => $guest,
            'event date' => $datetime,
            'event type' => $event_type,
            'location' => $location,
            'food type' => $food_type,
            'package' => $package_id
        ], $reservation->rules());

        if($foods != 0 && count($foods) > 0)
        {
            foreach($foods as $food)
            {
                array_push( $food_list, $food);
            }   
        }

        $food_list = array_unique($food_list);


        if($validator->fails())
        {
            return back()->withInput()->withErrors($validator);
        }

        DB::beginTransaction();


        /**
         * save customer details
         * @var [type]
         */
        $customer->lastname = $lastname;
        $customer->firstname = $firstname;
        // $customer->middlename = $middlename;
        $customer->email = $email;
        $customer->address = $address;
        $customer->contact_number = $contact;
        $customer->save();

        /**
         * save the reservation information in the database
         * @var [type]
         */
        $reservation->code = $reservation->generateCode();
        $reservation->guest_count = $guest;
        $reservation->event_date = Carbon\Carbon::parse( $datetime )->toDateTimeString();
        $reservation->event_end = Carbon\Carbon::parse( $datetime )->addHours(4)->toDateTimeString();
        $reservation->place = $location;
        $reservation->status = 0;
        $reservation->event_type_id = $event_type;

        /**
        *   replace total value with $total
        */
        $reservation->total = $total_pay;
        $reservation->customer_id = $customer->id;
        $reservation->service_id = $service;
        $reservation->package_id = $package_id;

        $reservation->save();

        $total = $reservation->total;

        $billing = new App\Billing;
        $billing->code = $billing->generateCode();
        $billing->status = 0;
        $billing->description = "Bill for Reservation $reservation->code";
        $billing->billing_type = 1;

        /**
        *   replace total value with $total
        */
        $billing->amount = $total;
        $billing->reservation_id = $reservation->id;
        $billing->save();

        /**
         * synchronize the food used for reservation
         */
        // $reservation->foods()->sync($food_list);
        // $reservation->service()->attach($service);
        // $reservation->package()->attach($package);

        
        $counter = count($food_list);
        
        for($i = 0 ; $i < $counter ; $i++){

            db::table('reservation_food')->insert(['reservation_id' => $reservation->id,'food_id' => $food_list[$i]]);

        }

        DB::commit();

        alert()->success('Successfully made reservation with a total of ₱' . $total_pay, 'Success')->persistent('Close');
        return redirect('reservation');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $reservation = App\Reservation::find($id);
        
        $validator = Validator::make([
            'reservation' => $id
        ], $reservation->deleteRules());

        if($validator->fails())
        {
            return back()->withInput()->withErrors($validator);
        }

        $reservation->status = array_search('Cancelled by the Customer', $this->status_list);
        $reservation->save();

        $request->session()->flash('success-message', 'Reservation Cancelled');
        return redirect('reservation');
    }

    public function cancel() {
        App\Reservation::where('id', $_POST['id'])
            ->update(['status' => 1]);
        alert()->success('Successfully cancelled the reservation', 'Success')->persistent('Close');
        return redirect('reservation');
    }
    
    public function getFoods($id){
        $data = db::table('package_food')->join('foods', 'package_food.food_id', '=', 'foods.id')->where('package_id', $id)->get()->toArray();
    }

    public function reschedule(){
        $update = $_POST['resched_date'];
        $time = $_POST['resched_time'];
        $id = $_POST['id'];

        $reservation = App\Reservation::find($id);
        $date = $reservation->event_date;

        $charge = $reservation->total;
        $charge *= 0.05;

        $event_date = Carbon\Carbon::parse($date)->format('Y-m-d');
        $datetime = $update . " " . $time ;

        if($update == $event_date){
            alert()->error('No rescheduling has been done', 'Error')->persistent('Close');
            return redirect()
                ->back();
        } else {
            App\Reservation::where('id', $id)
            ->update([
                'event_date' => $datetime,
            ]);

            $billing = new App\Billing;
            $billing->code = $billing->generateCode();
            $billing->status = 0;
            $billing->description = "Reschedule for Reservation $reservation->code";
            $billing->billing_type = 0;
            $billing->amount = $charge;
            $billing->reservation_id = $id;
            $billing->save();
        
            alert()->success('Successfully rescheduled the reservation', 'Success')->persistent('Close');
            return redirect('reservation');
        }

        
    }
}
