

<!--sidebar-menu-->
<div id="sidebar">
    <!-- <a href="#" class="visible-phone">
        <i class="fa fa-home"></i> Dashboard
    </a> -->
    <ul class="widthz">

        <li>
            <a href="{{ url ('dashboard')}}">
                <i class="icon icon-home"></i>
                <span>Dashboard</span>
            </a>
        </li>

        <div class="panel panel-default">
            <div class="panel-heading">
            <div class="panel-title" style="height:60px;">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" role="tab">
                    <i class="icon icon-calendar mr-1"></i>
                    <span>Reservation</span>
                    <span style="margin-right:130px;"></span>
                    <i class="icon icon-chevron-down"></i>
                </a>
            </div>
            </div>

            <div id="collapseOne" class="panel-collapse collapse inactive">
                <div class="panel-body">
                    <li>
                        <a href="{{ url('reservation') }}">List</a>
                    </li>
                    <li>
                        <a href="{{ url('reservation/create') }}">Create</a>
                    </li>
                </div>
            </div>
        </div>


         <div class="panel panel-default">
            <div class="panel-heading">
            <div class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                    <i class="icon icon-credit-card mr-1"></i>
                    <span>Billing</span>
                    <span style="margin-right:170px;"></span>
                    <i class="icon icon-chevron-down"></i>
                </a>
            </div>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
            <div class="panel-body">
                <li>
                    <a href="{{ url('billing') }}">List</a>
                </li>
                <li>
                    <a href="{{ url('billing/create') }}">Create</a>
                </li>
            </div>
            </div>
        </div>

         <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                        <i class="icon icon-credit-card mr-1"></i>
                        <span>Collections</span>
                        <span style="margin-right:135px;"></span>
                        <i class="icon icon-chevron-down"></i>
                    </a>
                </div>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                    <li>
                        <a href="{{ url('collection') }}">Collection</a>
                    </li>
                    <li>
                        <a href="{{ url('collection/refund') }}">Refund</a>
                    </li>
                </div>
            </div>
        </div>

         <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                        <i class="icon icon-group mr-1"></i>
                        <span>Items</span>
                        <span style="margin-right:175px;"></span>
                        <i class="icon icon-chevron-down"></i>
                    </a>
                </div>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body">
                    <li>
                        <a href="{{ url('monitoring') }}">Monitoring</a>
                    </li>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
            <div class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                    <i class="icon icon-edit mr-1"></i>
                    <span>Maintenance</span>
                    <span style="margin-right:120px;"></span>
                    <i class="icon icon-chevron-down"></i>
                </a>
            </div>
            </div>
            <div id="collapseFive" class="panel-collapse collapse">
            <div class="panel-body">
                <li>
                    <a href="{{ url('food') }}">Food</a>
                </li>
                <li>
                    <a href="{{ url('food/package') }}">Food Package</a>
                </li>
                <li>
                    <a href="/admin/item">Items</a>
                </li>

                <!-- <li>
                    <a href="{{ url('service/type') }}">Service Type</a>
                </li> -->
                <li>
                    <a href="{{ url('event/type') }}">Event Type</a>
                </li>
            </div>
            </div>
        </div>
        <li>
            <a href="{{ url ('queries')}}">
                <i class="icon icon-home"></i>
                <span>Queries</span>
            </a>
        </li>

        <div class="panel panel-default">
            <div class="panel-heading">
            <div class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
                    <i class="icon icon-list-alt mr-1"></i>
                    <span>Reports</span>
                    <span style="margin-right:155px;"></span>
                    <i class="icon icon-chevron-down"></i>
                </a>
            </div>
            </div>
            <div id="collapseSix" class="panel-collapse collapse">
            <div class="panel-body">
                <!-- <li>
                    <a method="get" href="{{ route('collection') }}" action="collectreport">Collection</a>
                </li>
                <li>
                    <a method="get" href="{{ route('refund') }}" action="collectreport2">Refund</a>
                </li>
                <li>
                    <a method="get" href="{{ route('reservesummary') }}" action="collectreport3">Reservation</a>
                </li>
                <li>
                    <a  method="get" href="{{ route('salesreport') }}" action="collectreport4">Sales</a>
                </li> -->
                <li>
                    <a href="{{ url('soa') }}">Statement of Account</a>
                </li>
                <li>
                    <a href="{{ url ('reports')}}">Sales Report</a>
                </li>
            </div>
            </div>
        </div>

        <li>
            <a href="{{ asset('files/UM.pdf') }}" target="_blank">
                <i class="icon-book"></i>
                <span>User's Manual<span>
            </a>
        </li>
        
    </div>

    </ul>
</div>
<!--sidebar-menu-->
