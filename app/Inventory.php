<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $table = 'inventory';
    protected $primaryKey = 'id';
    public $timestamps = true;
    public $fillable = [
    	'brand',
    	'model'
    ];
}
