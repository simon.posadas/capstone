@extends('layouts.template') 

@section('content')

<!--main-container-part-->
<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="icon icon-home"></i> Home</a>
                <a href="#" class="current">Reservation</a>
        </div>
        <h1>Reservation</h1>
    </div>
    <!--End-breadcrumbs-->
    <div class="container-fluid">
        <hr>
        
        @if(session()->has('success-message'))
        <div class="alert alert-success">
            <ul>
                {{ session()->get('success-message') }}
            </ul>
        </div>
        @endif


        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif




        <div class="pull-left">
            <a class="btn btn-success btn-large fa fa-plus" href="{{ url('reservation/create') }}" type="button"> Create Reservation</a>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <i class="fa fa-th fa-sm pt-1"></i>
                        <h5>Reservations</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table id="datatables" class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Code</th>
                                    <th>Location</th>
                                    <th>Guest Count</th>
                                    <th>Reservation Date</th>
                                    <th>Event Date</th>
                                    <th>Total</th>
                                    <th>Remarks</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                    <th>Generate Contract</th>
                                </tr>
                            </thead>
                            @foreach($reservations as $reservation)
                            <tbody>
                                <tr>
                                    <td>{{ $reservation->id }}</td>
                                    <td>{{ $reservation->code }}</td>
                                    <td>{{ $reservation->place }}</td>
                                    <td>{{ $reservation->guest_count }}</td>
                                    <td>{{ Carbon\Carbon::parse($reservation->created_at)->toDayDateTimeString  () }}</td>
                                    <td>{{ Carbon\Carbon::parse($reservation->event_date)->toDayDateTimeString  () }}</td>
                                    <td style="text-align: right">&#8369; {{ number_format($reservation->total, 2) }}</td>
                                    <td>{{ $reservation->details }}</td>
                                    <td>{{ $reservation->status_name }}</td>
                                    @if($reservation->status == 0)
                                    <td class="btn-center">
                                        <button id ="sizes1" class="btn btn-danger fa fa-times-circle-o first_cancel" data-id="{{$reservation->id}}"> Cancel</button>
                                    </td>
                                    @elseif($reservation->status == 1 or $reservation->status == 4 or $reservation->status == 5)
                                    <td class="btn-center">
                                        <button id ="sizes1" class="btn btn-danger fa fa-times-circle-o" disabled> Cancel</button>
                                    </td>
                                    @elseif($reservation->status == 2 or $reservation->status == 3)
                                        @if($days <= Carbon\Carbon::parse($reservation->event_date)->format('Y-m-d'))
                                        <td>
                                            <center><button type="button" id ="sizes1" class="btn btn-info icon icon-times-circle-o" disabled> Reschedule Event</button></center>
                                        </td>
                                        @else
                                        <td>
                                            <center><button type="button" id ="sizes1" class="btn btn-info icon icon-times-circle-o resched" data-id="{{$reservation->id}}"> Reschedule Event</button></center>
                                        </td>
                                        @endif
                                    @else
                                    <td>
                                        <center><button type="button" id ="sizes1" class="btn btn-info icon icon-times-circle-o" disabled> Reschedule Event</button></center>
                                    </td>
                                    @endif

                                    <td class="btn-center">
                                        <form method="get" target="_blank" action="{{ url("reservation/$reservation->id/$reservation->customer_id/contract") }}">
                                            <button class="btn btn-primary">Contract</button>
                                        </form> 
                                    </td>
                                </tr>
                            </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end-main-container-part-->

@endsection

@section('modal')
<!-- cancel event -->
<div class="modal fade" tabindex="-1" role="dialog" id="cancelModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Cancel Event</h4>
            </div>
            <form action="/first_cancel" method="post">
            <div class="modal-body">
                    {{csrf_field()}}
                    <input type="hidden" class="id" name="id" id="cancelID" required>
                    <label>Cancel Event?</label>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Confirm</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end cancel event -->

<!-- reschedule -->
<div class="modal fade" tabindex="-1" role="dialog" id="reschedModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Reschedule Event</h4>
            </div>
            <form action="/resched" method="post">
            <div class="modal-body">
                    {{csrf_field()}}
                    <input type="hidden" class="id" name="id" id="reschedID" required>
                    <div class="input-group">
                        <label class="control-label">Reschedule Date (mm-dd-yyyy)</label>
                        <div class="input-append date datepicker">
                        <input id="resched_date" name="resched_date" class="span2" size="16" type="date" min="{{ Carbon\Carbon::now()}}">
                        </div>
                    </div>
                    <div class="input-group">
                        <label class="control-label">Reschedule Event Time :</label>
                        <input type="Time" class="form-control input-eventtime" placeholder="Event Time" name="resched_time" id="time" required min='07:00:00' max='21:00:00'>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Confirm</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end reschedule -->
@endsection

@section('scripts_include')

<script type="text/javascript">
    // status 0
    $('.first_cancel').click(function () {
        $('#cancelID').val($(this).data('id'));
        $('#cancelModal').modal('show');
    });
    // end status 0


    $('.resched').click(function () {
        $('#reschedID').val($(this).data('id'));
        $('#reschedModal').modal('show');
    });

</script>

@endsection