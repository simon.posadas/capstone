<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use DB;
use Validator;

class PackagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packages = App\Package::orderBy('name')
            ->get();
        $category = App\CategoryFood::all();

        return view('package.index')
                ->with('packages', $packages);
                // ->with ('category', $category);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = $request->get('name');
        $price = $request->get('price');
        $type = $request->get('type');

        $package = new App\Package;

        $validator = Validator::make([
            'name' => $name,
            'price' => $price,
            'type' => $type
        ], $package->rules());

        if($validator->fails())
        {
            return back()->withInput()->withErrors($validator);
        }

        DB::beginTransaction();

        $package->code = $package->generateCode();
        $package->name = $name;
        $package->price = $price;
        $package->type = $type;
        $package->status = 0;
        $package->save();

        DB::commit();

        $request->session()->flash('success-message', 'Package Created');
        return redirect('food/package');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('errors.404');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name = $request->get('name');
        $price = $request->get('price');
        $type = $request->get('type');

        $package = App\Package::find($id);

        $validator = Validator::make([
            'name' => $name,
            'price' => $price,
            'type' => $type
        ], $package->rules());

        if($validator->fails())
        {
            return back()->withInput()->withErrors($validator);
        }

        DB::beginTransaction();

        $package->code = $package->generateCode();
        $package->name = $name;
        $package->price = $price;
        $package->type = $type;
        $package->status = $status;
        $package->save();

        DB::commit();

        $request->session()->flash('success-message', 'Package Created');
        return redirect('food/package');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $package = App\Package::find($id);

        if(count($package) <= 0) $package = new App\Package;

        $validator = Validator::make([
            'Package' => $id 
        ], $package->deleteRules());

        if($validator->fails())
        {
            return back()->withErrors($validator);
        }

        $package->delete();

        $request->session()->flash('success-message', 'Package Removed');
        return redirect('food/package');
    }
}
