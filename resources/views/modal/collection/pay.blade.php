
<div class="modal fade" tabindex="-1" role="dialog" id="reservationPaymentModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Payment</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="{{ url('pay') }}" id="payReservationForm">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="id" id="reservation-id">

                    <div class="form-group">
                        <label>Total: <span id="total-amount"></span> </label>
                    </div>

                    <div class="form-group">
                        <label>Half: <span id="half-amount"></span> </label>
                    </div>

                    <div class="form-group">
                        <label>Payee: </label>
                        <input type="text" class="form-control" name="payee" required>
                    </div>
                    <div class="form-group">
                        <label>Paid Amount: </label>
                        <input type="text" class="form-control" name="paid" required>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" id="payment-confirm" class="btn btn-primary">Confirm</button>
            </div>
            </form>
        </div>
    </div>
</div>