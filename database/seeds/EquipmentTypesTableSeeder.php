<?php

use Illuminate\Database\Seeder;

class EquipmentTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('equipment_types')->insert([

        	[
        		'name' => 'Chair',
        	],

        	[
        		'name' => 'Table',
        	],

        	[
        		'name' => 'Utensils',
        	]
        ]);
    }
}
