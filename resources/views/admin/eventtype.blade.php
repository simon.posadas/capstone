@extends('layouts.sidebar') 

@section('title', 'Event Type')

@section('content')

<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="fa fa-home"></i> Home</a>
            <a href="/admin/reservation">Collection</a>
            <a href="#" class="current">Event Type</a>
        </div>
        <h1>Event Type</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="pull-left">
            <button class="btn btn-success btn-large fa fa-plus addType mb-2"> Add Event Type</button>
        </div>
        <div class="row-fluid">
            <div class="colspan">
                <div class="widget-box">
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Event Type Name</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            @foreach($types as $type)
                            <tbody>
                                <tr>
                                    <td>{{$type->type_name}}</td>
                                    <td>
                                        <button class="btn btn-primary fa fa-pencil edittype" data-id="{{ $type->type_id }}"> Edit</button>
                                        <button type="submit" class="btn btn-danger fa fa-trash deletetype" data-id="{{ $type->type_id }}" > Delete</button>
                                    </td>
                                </tr>
                            </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('modal')
<!-- add modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="addModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add Event Type</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/addType">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label>Event Type Name</label>
                        <input type="text" class="form-control" placeholder="Event Type Name" name="tname" required>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Add</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end add modal -->

<!-- edit modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="editModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edit Event Type</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/editType">
                    {{csrf_field()}}
                    <input type="hidden" name="id" id="editID">
                    <div class="form-group">
                        <label>Event Type Name</label>
                        <input type="text" class="form-control inpname" name="name">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end edit modal -->

<!-- delete modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="deleteModal">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">Delete</h4>
        </div>
        <div class="modal-body">
            <form method="post" action="/deleteType">
                {{csrf_field()}}
                <input type="hidden" class="id" name="id" id="deleteID">
                <div class="form-group">
                    <h4>Delete event type?</h4>
                </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Confirm</button>
        </div>
        </form>
    </div>
</div>
</div>
<!-- end delete modal -->
@endsection

@section('script')
<script>
    $('.addType').click(function () {
        $('#addModal').modal('show');
    });

    $('.edittype').click(function () {
        $.ajax({
            type: "get",
            url: '/getType',
            data: {
                id: $(this).data('id')
            },
            dataType: "json",
            success: function (response) {
                response.forEach(function (data) {
                    $('#editModal .id').val($(this).data('id'));
                    $('#editModal .inpname').val(data.type_name);
                })
            }
        });
        $('#editModal').modal('show');
        $('#editID').val($(this).data('id'));
    });

    $('.deletetype').click(function () {
        $('#deleteID').val($(this).data('id'));
        $('#deleteModal').modal('show');
    });
</script>
@endsection