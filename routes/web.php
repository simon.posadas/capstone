<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SessionsController@getHomePage');

Route::get('login', [
    'as' => 'login',
    'uses' => 'SessionsController@getLogin'
]);
Route::post('login', 'SessionsController@login');

Route::get('register', 'SessionsController@getRegistrationForm');
Route::post('register', 'SessionsController@register');

Route::middleware('auth')->group(function(){

    Route::post('logout', 'SessionsController@logout');

    Route::resource('event/type', 'EventTypesController');

    Route::resource('food/category', 'FoodCategoriesController');

    Route::resource('food/type', 'FoodTypesController');

    Route::get('food/package/{package}/limit', 'PackageLimitsController@index');
    Route::post('food/package/{package}/limit', 'PackageLimitsController@store');
    Route::put('food/package/{package}/limit/{category}', 'PackageLimitsController@update');
    Route::delete('food/package/{package}/limit/{category}', 'PackageLimitsController@destroy');

    Route::get('food/package/{package}/list', 'PackageListsController@index');
    Route::post('food/package/{package}/list', 'PackageListsController@store');
    Route::put('food/package/{package}/list/{food}', 'PackageListsController@update');
    Route::delete('food/package/{package}/list/{food}', 'PackageListsController@destroy');

    Route::resource('food', 'FoodsController', [
        'except' => 'show'
    ]);

    Route::resource('food/package', 'PackagesController', [
        'except' => 'show'
    ]);

    Route::resource('service/type', 'ServiceTypesController');

    Route::resource('reservation', 'ReservationsController');

    Route::get('Foodpackages/{id}', ['uses' => 'ReservationsController@getFoods', 'as' => 'getFoods']);


    Route::post('reservation/{id}/pay', 'CollectionsController@pay');
    Route::get('billing/{id}/print', 'BillingsController@print');
    // Route::get('billing/receipt/{id}/{id2}', ['uses' => 'ReceiptController@receipt', 'as' => 'Refund']);
    Route::get('collection/refund/{id}/{id2}', ['uses' => 'RefundController@refundform', 'as' => 'Refund']);
    Route::get('billing/receipt/{id}/{id2}', ['uses' => 'ReceiptController@receipt', 'as' => 'Receipt']);
    Route::get('billing/otherreceipt/{id}/{id2}', ['uses' => 'ReceiptController@otherreceipt', 'as' => 'OtherReceipt']);
    Route::get('reservation/{id}/{id2}/contract', 'ContractController@contract');
    Route::resource('billing', 'BillingsController');

    // transaction
    Route::post('/resched', 'ReservationsController@reschedule');
    Route::post('/paid_first', 'CollectionsController@downpayment');
    Route::post('/first_cancel', 'ReservationsController@cancel');

    Route::post('/paid_full', 'CollectionsController@full_pay');

    Route::post('/refund_cancel', 'RefundController@cancellationWithRefund');

    Route::post('/event_done', 'CollectionsController@event_done');
    Route::post('/norefund_cancel', 'RefundController@cancellationWithoutRefund');

    Route::post('/charge_cancel', 'RefundController@cancel_charge');
        // end transaction

    Route::get('collection/{id}/billing', 'CollectionsController@getBillings');
    Route::get('collection/{id}/payment', ['uses' => 'CollectionsController@getPayments', 'as' => 'getpayments']);
    Route::get('collection/refund', 'RefundController@refund');
    Route::get('collection/print/sales', 'CollectionsController@getSales');
    //PDF generator
    Route::get('/CollectionReport', 'CollectionsController@CollectionReport');
    Route::get('/CollectionReportYear', 'CollectionsController@CollectionReportYear');

    Route::resource('collection', 'CollectionsController');
    Route::resource('payments', 'PaymentsController');

    Route::get('monitoring', 'ItemMonitoringController@index');
    Route::get('monitoring/items/{reservation_id}', 'ItemMonitoringController@getReservationItems');
    Route::get('monitoring/items/tagging/{reservation_id}', 'ItemMonitoringController@getItems');
    Route::post('monitoring/items/tagging', 'ItemMonitoringController@tagItem');
    Route::post('monitoring/items/release', 'ItemMonitoringController@releaseItem');
    Route::post('monitoring/items/return', 'ItemMonitoringController@returnItem');

    Route::resource('billing', 'BillingsController');


    Route::resource('soa', 'SoaController');
    //Route::get('/soaDetail', 'SoaController@detail');
    Route::get('generate-pdf', 'PdfGenerateController@pdfview')->name('generate-pdf');

    Route::get('dashboard', 'DashboardController@index');
    Route::get('/getData', 'DashboardController@getData');

    Route::get('/admin/item', 'ItemsController@index2');
    Route::post('/item/add', 'ItemsController@add');
    Route::post('/item/delete', 'ItemsController@delete');
    Route::post('/item/edit', 'ItemsController@edit');

    Route::get('reports', function(){
        return view ('reports/index');
    });

    Route::get('/queries','QueriesController@Reservation');



    route::post('/packagefood/delete', 'PackageListsController@destroy');

    route::get('collectreport', ['uses' => 'ReportController@collectionreport', 'as' => 'collection']);
    route::get('collectreport2', ['uses' => 'ReportController@refund', 'as' => 'refund']);
    route::get('collectreport3', ['uses' => 'ReportController@reservation', 'as' => 'reservesummary']);
    route::get('collectreport4', ['uses' => 'ReportController@salesreport', 'as' => 'salesreport']);

});

    // Route::get('/doLogin', 'LoginController@doLogin')->name('admin.login');
    // // end login

    // // dashboard
    // // Route::get('/dashboard', 'Admin\AdminController@dashboard');
    // // Route::get('/admin', function () {
    // //     return view('/dashboard');
    // // });
    // // end dashboard

    // // food
    // Route::get('/admin/food', 'Admin\FoodController@food');
    // Route::get('/foods', function () {
    //     return view('/admin/food');
    // });
Route::get('/searchFood', 'Admin\FoodController@searchFood');

// END ADMIN ROUTES

// WEBSITE ROUTES

// END WEBSITE ROUTES
