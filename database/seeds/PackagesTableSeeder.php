<?php

use Illuminate\Database\Seeder;

class PackagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Package::insert([

        [
        	'name' => 'Wedding Buffet Menu for Lunch and Dinner',
        	'code' => 'PKG-18-02',
        	'type' => 'wedding',
        	'price' => '500',
        	'status' => '1',
        ],

        [
        	'name' => 'Meryenda Package',
        	'code' => 'PKG-18-02',
        	'type' => 'wedding',
        	'price' => '400',
        	'status' => '1',
        ],

        [
        	'name' => 'Flamingo',
        	'code' => 'PKG-18-02',
        	'type' => 'wedding',
        	'price' => '450',
        	'status' => '1',
        ]
        ]);

    }
}
