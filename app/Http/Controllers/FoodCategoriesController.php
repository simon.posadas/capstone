<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use App;

class FoodCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = App\FoodCategory::orderBy('name')
            ->get();

        return view('food.category.index')
                ->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = $request->get('name');

        $category = new App\FoodCategory;

        $validator = Validator::make([
            'name' => $name
        ], $category->rules());

        if($validator->fails())
        {
            return back()->withErrors($validator);
        }

        $category->name = $name;
        $category->save();

        $request->session()->flash('success-message', 'Food Category Added');

        return redirect('food/category');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name = $request->get('name');

        $category = App\FoodCategory::find($id);

        $validator = Validator::make([
            'name' => $name
        ], $category->updateRules());

        if($validator->fails())
        {
            return back()->withErrors($validator);
        }

        $category->name = $name;
        $category->save();

        $request->session()->flash('success-message', 'Food Category Updated');

        return redirect('food/category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $category = App\FoodCategory::find($id);
        
        if(count($category) <= 0) $category = new App\FoodCategory;

        $validator = Validator::make([
            'Food Category' => $id
        ], $category->deleteRules());

        if($validator->fails())
        {
            return back()->withErrors($validator);
        }

        $category->delete();

        $request->session()->flash('success-message', 'Food Category Removed');

        return redirect('food/category');
    }
}
