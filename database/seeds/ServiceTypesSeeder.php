<?php

use Illuminate\Database\Seeder;

class ServiceTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('service_types')->insert([
        	[  
                'code' => 'BUF1802',
        		'name' => 'Buffet',
        		'percentage' => '0'
        	],

        	[
                'code' => 'PLT1802',
        		'name' => 'Plated',
        		'percentage' => '10'
        	],

        	[
                'code' => 'LAU1802',
        		'name' => 'Lauriat',
        		'percentage' => '0'
        	],

        	[
                'code' => 'PAL1802',
        		'name' => 'Paluto',
        		'percentage' => '0'
        	]
        ]);

    }
}
