@extends('layouts.template')

@section('content')

<!--main-container-part-->
<meta name="csrf-token" content="{{ csrf_token() }}">
<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="icon icon-home"></i> Home</a>
        </div>
        <h1>Dashboard</h1>
    </div>
    <hr>
    <div class="container-fluid" width="100" height="100">
        <canvas id="myChart"></canvas>
    </div>
</div>

@endsection 

@section('script')
<script>
    var today = new Date();
    var yyyy = today.getFullYear();

    $(document).ready(function() {
        $.ajax({
            url : "/getData",
            type : "GET",
            success : function(data){
                console.log(data);

                var number = {
                    month : []
                };

                var len = data.length;


                for (var i = 1; i <= len; i++) {
                    number.month.push(data[i]);
                }

                //get canvas
                var ctx = document.getElementById("myChart").getContext('2d');

                Chart.defaults.global.defaultFontSize = 17;

                var data = {
                    labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                    datasets: [{
                        label: '# of Reservation for year ' +yyyy,
                        data: number.month,
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.6)',
                            'rgba(54, 162, 235, 0.6)',
                            'rgba(255, 206, 86, 0.6)',
                            'rgba(75, 200, 192, 0.6)',
                            'rgba(153, 102, 255, 0.6)',
                            'rgba(255, 159, 64, 0.6)',
                            'rgba(153, 231, 34, 0.6)',
                            'rgba(200, 143, 69, 0.6)',
                            'rgba(25, 190, 10, 0.6)',
                            'rgba(150, 250, 200, 0.6)',
                            'rgba(50, 110, 235, 0.6)',
                            'rgba(45, 146, 54, 0.6)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 200, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)',
                            'rgba(153, 231, 34, 1)',
                            'rgba(200, 143, 69, 1)',
                            'rgba(25, 190, 10, 1)',
                            'rgba(150, 250, 200, 1)',
                            'rgba(50, 110, 235, 1)',
                            'rgba(45, 146, 54, 1)'
                        ],
                        borderWidth: 1,
                        hoverBorderColor:'#000'
                    }]
                };

                var options = {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                }
                            }]
                        },
                        title:{
                            display:true,
                            text: 'Number of Reservation for year ' +yyyy+ ' per month',
                            fontSize:20
                        },
                        legend:{
                            display:false
                        },
                        layout:{
                            
                            height:100,
                            width:100
                        }
                    };

                var chart = new Chart( ctx, {
                    type : "bar",
                    data : data,
                    options : options
                } );


            },
            error : function(data) {
                console.log(data);
                alert('ERROR');
            }
        });

    });
</script>
@endsection