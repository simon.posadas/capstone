<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique();
            $table->integer('guest_count');
            $table->datetime('event_date');
            $table->datetime('event_end')->nullable();
            $table->string('details')->nullable();
            $table->boolean('status');
            $table->decimal('total', 8, 2);
            $table->integer('package_id')->unsigned()->nullable();
            $table->integer('service_id')->unsigned()->nullable();
            $table->integer('customer_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('place')->nullable();
            $table->integer('event_type_id')->unsigned()->nullable();
            $table->timestamps();

            
            $table->foreign('package_id')
                    ->references('id')
                    ->on('packages')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('service_id')
                    ->references('id')
                    ->on('service_types')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('customer_id')
                    ->references('id')
                    ->on('customers')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('event_type_id')
                    ->references('id')
                    ->on('event_types')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
