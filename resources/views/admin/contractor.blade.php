@extends('layouts.sidebar')
@section('title', 'Contractor')
@section('content')

<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="fa fa-home"></i> Home</a>
            <a href="#" class="current">Contractor</a>
        </div>
        <h1>Contractor</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="">
        <div class="pull-left mb-2">
            <button class="btn btn-success btn-large fa fa-plus addWorker"> Add Contractor</button>
        </div>
        <form method="get" action="/searchContractor" role="search">
            <div class="pull-right">   
                <div class="input-group custom-search-form">
                    <input type="text" name="searchContractor" class="form-control" placeholder="Search" id="search_bar">
                    <button class="btn btn-info" id="adjust-height" type="submit"><i class="fa fa-search"></i> Search</button>
                </div>
            </div>
        </form>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon">
                            <i class="fa fa-th"></i>
                        </span>
                        <h5>Data table</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table" id="emp_table">
                            <thead>
                                <tr>
                                    <th>Contractor Name</th>
                                    <th>Contact Number</th>
                                    <th width="20%">Actions</th>
                                </tr>
                            </thead>
                            @foreach($contractors as $contractor)
                            <tbody>
                                <tr>
                                    <td>{{$contractor->contractor_fname .' '. $contractor->contractor_lname}}</td>
                                    <td>{{$contractor->contactNo}}</td>
                                    <td class="btn-center">
                                        <button id ="sizes" class="btn btn-primary fa fa-pencil edittype" data-id="{{$contractor->contractor_id}}"> Edit</button>
                                        <button id ="sizes" class="btn btn-danger fa fa-trash deletetype" data-id="{{$contractor->contractor_id}}"> Delete</button>
                                    </td>
                                </tr>
                            </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('modal')
<!-- add modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="addModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add Contractor</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/addContractor">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label>First Name</label>
                        <input type="text" class="form-control" placeholder="First Name" name="fname" required>
                    </div>
                    <div class="form-group">
                        <label>Middle Name</label>
                        <input type="text" class="form-control" placeholder="Middle Name" name="mname" required>
                    </div>
                    <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" class="form-control" placeholder="Last Name" name="lname" required>
                    </div>
                    <div class="form-group">
                        <label>Age</label>
                        <input type="number" class="form-control" placeholder="Age" name="age" required>
                    </div>
                    <div class="form-group">
                        <label>Contact</label>
                        <input type="number" class="form-control" placeholder="Contact Number" name="contno" required>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Add</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- end add modal -->


<!-- edit modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="editModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edit Employee</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/editContractor">
                    {{csrf_field()}}
                    <input type="hidden" class="id" name="id" id="editID">
                    <div class="form-group">
                        <label>First Name</label>
                        <input type="text" class="form-control inpfname" placeholder="First Name" name="fname" required>
                    </div>
                    <div class="form-group">
                        <label>Middle Name</label>
                        <input type="text" class="form-control inpmname" placeholder="Middle Name" name="mname" required>
                    </div>
                    <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" class="form-control inplname" placeholder="Last Name" name="lname" required>
                    </div>
                    <div class="form-group">
                        <label>Age</label>
                        <input type="number" class="form-control inpage" placeholder="Age" name="age" required>
                    </div>
                    <div class="form-group">
                        <label>Contact Number</label>
                        <input type="number" class="form-control inpno" placeholder="Contact Number" name="contno" required>
                    </div>
                    <!--  -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end edit modal -->

<!-- delete modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="deleteModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Delete</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/deleteContractor">
                    {{csrf_field()}}
                    <input type="hidden" class="id" name="id" id="deleteID">
                    <div class="form-group">
                        <h4>Delete contractor?</h4>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Confirm</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end delete modal -->
@endsection

@section('script')

<script type="text/javascript">
    $('.addWorker').click(function () {
        $('#addModal').modal('show');
    });


    $('#editModal').on('show.bs.modal', function () {
        $(this).find('#btn-primary').on('click', function () {
            $('#editModal').find('form').submit();
        });
    })

    $('.edittype').click(function () {
        $.ajax
        ({
            type : 'get',
            url : '/getContractor',
            data : {id : $(this).data('id')},
            dataType: "json",
            success: function(response) {
                response.forEach(function(data){
                    $('#editModal .id').val($(this).data('id'));
                    $('#editModal .inpfname').val(data.contractor_fname);
                    $('#editModal .inpmname').val(data.contractor_mname);
                    $('#editModal .inplname').val(data.contractor_lname);
                    $('#editModal .inpage').val(data.contractor_age);
                    $('#editModal .inpno').val(data.contactNo);
                })
            }
        });
        $('#editID').val($(this).data('id'));
        $('#editModal').modal('show');
    });


    $('.deletetype').click(function () {
        $('#deleteID').val($(this).data('id'));
        $('#deleteModal').modal('show');
    });
</script>

@endsection