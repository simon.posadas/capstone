<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Validator;

class FoodTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = App\FoodType::orderBy('name')
            ->get();

        return view('food.type.index')
                ->with('types', $types);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = $request->get('name');

        $type = new App\FoodType;

        $validator = Validator::make([
            'name' => $name
        ], $type->rules());

        if($validator->fails())
        {
            return back()->withInput()->withErrors($validator);
        }

        $type->name = $name;
        $type->save();

        $request->session()->flash('success-message', 'Food Type Added');
        return redirect('food/type');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return 'sdafsdaf';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return 'dasfdsf';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name = $request->get('name');

        $type = App\FoodType::find($id);

        $validator = Validator::make([
            'name' => $name
        ], $type->updateRules());

        if($validator->fails())
        {
            return back()->withInput()->withErrors($validator);
        }

        $type->name = $name;
        $type->save();

        $request->session()->flash('success-message', 'Food Type Updated');
        return redirect('food/type');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $type = new App\FoodType;

        if(count($type) <= 0) $type = new App\FoodType;

        $validator = Validator::make([
            'Food Type' => $id 
        ], $type->deleteRules());

        if($validator->fails())
        {
            return back()->withErrors($validator);
        }

        $type = App\FoodType::find($id);
        $type->delete();

        $request->session()->flash('success-message', 'Food Type Removed');
        return redirect('food/type');
    }
}
