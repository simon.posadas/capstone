<!-- edit modal -->
<div class="modal fade modal-size-s4" tabindex="-1" role="dialog" id="editModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edit Package</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="#" id="editForm">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="_method" value="PUT" />

                    <input type="hidden" name="id" id="editID">
                    <div class="form-group space-between">
                        <label>Package Name</label>
                        <input id="input-packageName" type="text" class="form-control inpname" name="name">
                    </div>
                    <div class="form-group space-between">
                        <label>Price</label>
                        <input id="input-packagePrice" type="text" class="form-control" placeholder="Price" name="price" required>
                    </div>
                    <div class="form-group form-fontsize">
                        <label>Type</label>
                        <input class="radio-position" type="radio" name="type" class="type" value="Buffet for Lunch and Dinner" checked> Buffet for Lunch and Dinner
                        <input class="radio-position" type="radio" name="type" class="type" value="merienda"> Merienda
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn-edit">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- end edit modal -->
