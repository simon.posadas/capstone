<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon;

class ServiceType extends Model
{
    /**
     * Table to connect
     * @var type 
     */
    protected $table = 'service_types';
    protected $primaryKey = 'id';
   
    /**
     * Remove automatic timestamp
     * @var type 
     */
    public $timestamps = false;

    public function rules()
    {
        return [
            'name' => 'required|min:2|max:20|unique:service_types,name'
        ];
    }

    public function updateRules()
    {
        $name = $this->name;
        return [
            'name' => 'required|min:2|max:20|unique:service_types,name,'.$name.',name'
        ];
    }

    public function deleteRules()
    {
        return [
            'Service Type' => 'required|exists:service_types,id'
        ];
    }

    public function generateCode()
    {
        $count = 1;
        $date = Carbon\Carbon::now();
        $constant = 'SERV-' . $date->format('y') . '-' . $date->format('m') . '-';
        $id = ServiceType::orderBy('id', 'desc')->pluck('id')->first() + 1;

        if(!isset($id) || $id == null || $id == '') $id = 0;

        return $constant . $id;
    }

}
