@extends('layouts.sidebar')

@section('title', '3')

@section('content')
<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="fa fa-home"></i> Home</a>
            <a href="#" class="current">Reservation Management</a>
        </div>
        <h1>Reservation Management</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="colspan">
                <div class="widget-box">
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Reservation ID</th>
                                    <th>Name</th>
                                    <th>No. of Guest</th>
                                    <th>Event</th>
                                    <th>Event Date</th>
                                    <th>Event Time</th>
                                    <th>Action</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td width="12%"></td>
                                    <td width="17%"></td>
                                    <td></td>
                                    <td width="17%"></td>
                                    <td width="13%"></td>
                                    <td></td>
                                    <td width="20%" class="btn-center">
                                        <button id ="sizes" class="btn btn-primary fa fa-eye-open viewtype">View</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection