<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Validator;
use DB;

class ContractController extends Controller
{
    public function contract($id, $id2)
    {
        // dd($id, $id2);
        $reservation = app\Reservation::find($id);
        $customer = App\Person::find($id2);
        $package = App\Package::find($reservation->package_id);
        $food = DB::table('reservation_food')->where('reservation_id', '=', $id)->pluck('food_id');
        $food  = App\Food::whereIn('id', $food)->get();
        
        

        $data = [
            'reservation' => $reservation,
            'customer' => $customer,
            'packages' => $package,
            'food' => $food,
            
        ];

        $filename = "Contract-".\Carbon\Carbon::now()->format('mdYHm').".pdf";
        $view = "billing.print.contract";





        return view($view)
                ->with($data)
                ->with($filename);

    }
}
