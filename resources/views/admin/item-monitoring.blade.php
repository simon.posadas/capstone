@extends('layouts.template') 

@section('content')
<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="icon icon-home"></i> Home</a>
            <a href="#" class="current">Item Monitoring</a>
        </div>
        <h1>Item Monitoring</h1>
    </div>
    <div class="row-fluid">
        <div class="colspan">
            <div class="widget-box">
                <div class="widget-content nopadding">
                    <table class="table table-bordered" id="itemTable">
                        <thead>
                            <tr>
                                <th style="display: none;">Reservation ID</th>
                                <th>Customer</th>
                                <th>Contact</th>
                                <th>Event Date</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($reservations as $reservation)
                                <tr>
                                    <td style="display: none;">{{ $reservation->id }}</td>
                                    <td>{{ $reservation->customer->firstname }} {{ $reservation->customer->lastname }}</td>
                                    <td>{{ $reservation->customer->contact_number }}</td>
                                    <td>{{ Carbon\Carbon::parse($reservation->event_date)->toDayDateTimeString () }}</td>
                                    <td>
                                        <a href="{{ url("monitoring/items/$reservation->id") }}" class="btn btn-default">Manage Items</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#itemTable').dataTable({
                searching   :   false
            });
        })
    </script>
@endsection 
