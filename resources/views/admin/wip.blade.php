@extends('layouts.sidebar')

@section('title', 'WIP')

@section('content')

<!--main-container-part-->
<meta name="csrf-token" content="{{ csrf_token() }}">
<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="fa fa-home"></i> Home</a>
            <a href="#" class="current">Budget</a>
        </div>
        <h1>Budget</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="colspan">
                <div class="widget-box">
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table" id="wipTbl">
                            <thead>
                                <tr>
                                    <th>Reservation ID</th>
                                    <th>Total Price</th>
                                    <th>Details</th>
                            </thead>
                            @foreach($details as $detail)
                            <tbody>
                                <tr>

                                    <td>{{$detail->cust_fname . " " . $detail->cust_lname}}</td>
                                    <td>{{$detail->contNo}}</td>
                                    <td>{{ config()->get('constants.status')[$detail->status] }}</td>
                                    <td class="btn-center">
                                        <button id ="sizes" class="btn btn-primary fa fa-eye viewtype" data-id="{{$detail->reserv_id}}"> View More Details</button>
                                    </td>
                                </tr>
                            </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--main-container-part-->

@endsection 

 @section('modal')
    <!-- view modal -->
    <div class="modal fade modal-size" tabindex="-1" role="dialog" id="viewModal">
        <div class="modal-dialog modal-size" role="document">
            <div class="modal-content">
                <div class="modal-header m-header-top">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">View Form</h4>
                </div>
                <div class="modal-body">
                
                <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>Expense Category</th>
                                    <th>Specifics</th>
                                    <th>Price</th>
                                    <th>Total Price</th>
                            </thead>
                            @foreach($details as $detail) 
                            <tbody>
                                <tr>
                                    <td>sample data</td>
                                    <td>sample data</td>
                                    <td>sample data</td>
                                    <td>sample data</td>
                                </tr>
                            </tbody>
                            @endforeach
                        </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection 

@section('script')
    <script type="text/javascript">
        $('.viewtype').click(function () {
            $('#viewModal').modal('show');
        });
    </script>

@endsection 
    