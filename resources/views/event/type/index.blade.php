@extends('layouts.template') 

@section('content')

@include('modal.event.type.add')
@include('modal.event.type.edit')
@include('modal.event.type.delete')

<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="icon icon-home"></i> Home</a>
            <a href="#" class="current">Event</a>
        </div>
    </div>
    <div class="container-fluid">
        <hr>

        @if(session()->has('success-message'))
        <div class="alert alert-success">
            <ul>
                {{ session()->get('success-message') }}
            </ul>
        </div>
        @endif


        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="pull-left">
            <button class="btn btn-success btn-large icon icon-plus" data-toggle="modal" data-target="#addModal" type="button"> Add Event</button>
        </div>
        <form method="get" action="/searchPackage" role="search">
            <div class="pull-right">   
                <div class="input-group custom-search-form">
                    <input type="text" name="searchPackage" class="form-control" placeholder="Search" id="search_bar">
                    <button class="btn btn-info" id="adjust-height-right" type="submit"><i class="icon icon-search"></i> Search</button>
                </div>
            </div>
        </form>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-content nopadding">

                        <table class="table table-bordered data-table" id="eventTypesTable">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Range</th>
                                    <th width="20%">Actions</th>
                                </tr>
                            </thead>
                            @foreach($types as $type)
                            <tbody>
                                <tr>
                                    <td>{{ $type->id }}</td>
                                    <td>{{ $type->name }}</td>
                                    <td style="text-align: right">{{ $type->range }}</td>
                                    <td class="btn-center">
                                        <button id ="sizes" class="btn icon icon-pencil btn-primary edittype" data-id="{{ $type->id }}" data-name="{{ $type->name }}" data-range="{{ $type->range }}" data-toggle="modal" data-category="{{ $type->category_id }}" data-target="#editModal"> Edit</button>
                                        <button id ="sizes" class="btn icon icon-trash btn-danger deletetype" data-id="{{ $type->id }}" data-name="{{ $type->name }}" data-range="{{ $type->range }}" data-toggle="modal" data-target="#deleteModal"> Delete</button>
                                    </td>
                                </tr>
                            </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection 

@section('scripts_include')

<script type="text/javascript">

    $(document).ready(function(){

        $('#eventTypesTable').on('click', '.edittype', function (event) {
            range = $(this).data('range')
            name = $(this).data('name')
            id = $(this).data('id')

            $('#editModal').find('input[name=name]').val(name);
            $('#editModal').find('input[name=range]').val(range);

            $('#editModal').find('#btn-edit').on('click', function () {

                $('#editForm').attr('action', function(i, value) {
                    return "{{ url("event/type") }}/" + id ;
                });

                $('#editForm').submit();
            });

        })

        $('#eventTypesTable').on('click', '.deletetype', function (event) {
            id = $(this).data('id')

            $('#deleteModal').find('#btn-delete').on('click', function () {

                $('#deleteForm').attr('action', function(i, value) {
                    return "{{ url("event/type") }}/" + id ;
                });

                $('#deleteForm').submit();
            });

        })

    })

</script>

@endsection