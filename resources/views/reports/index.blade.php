@extends('layouts.template') 

@section('content')
<!--main-container-part-->
<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="icon icon-home"></i> Home</a>
            <a href="#" class="current">Report</a>
        </div>
        <h1>Reports</h1>
    </div>
    <!--End-breadcrumbs-->
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="btn-reports">
                    <h2>Sales Reports</h2>
                    <button class="btn btn-large icon icon-print" onclick="print()">MONTHLY SALES</button>
                    <button class="btn btn-large icon icon-print" onclick="years()">YEARLY SALES</button>
                </div>
                <hr>
            </div>
        </div>
    </div>
</div>
<!--end-main-container-part-->

@endsection 

@section('script')

<script type="text/javascript">


   function print()
    {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    var currentMonth;
    if (mm == 1){
      currentMonth = "January";
    }
    if (mm == 2){
      currentMonth = "February";
    }
    if (mm == 3){
      currentMonth = "March";
    }
    if (mm == 4){
      currentMonth = "April";
    }
    if (mm == 5){
      currentMonth = "May";
    }
    if (mm == 6){
      currentMonth = "June";
    }
    if (mm == 7){
      currentMonth = "July";
    }
    if (mm == 8){
      currentMonth = "August";
    }
    if (mm == 9){
      currentMonth = "September";
    }
    if (mm == 10){
      currentMonth = "October";
    }
    if (mm == 11){
      currentMonth = "November";
    }
    if (mm == 12  ){
      currentMonth = "December";
    }
    if(dd<10){
        dd='0'+dd;
    } 
    if(mm<10){
        mm='0'+mm;
    }
    var today = dd+'/'+mm+'/'+yyyy;
    var total =0;
        $.ajax({
            type: "get",
            url:  "/CollectionReport",
            data: {
                month : mm,
            },
            success: function(data){

                var s = data['sum'];
                var r = data['ref'];
                var tot = data['gross'];

        
               var frame1 = $('<iframe />');
                frame1[0].name = "frame1";
                frame1.css({ "position": "absolute", "top": "-1000000px" });
                $("body").append(frame1);
                var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
                frameDoc.document.open();
                //Create a new HTML document.
                frameDoc.document.write('<html><head>');
                frameDoc.document.write('<style>table, th, td {border: 0.5px solid black;border-collapse: collapse; padding:3px;  font-size:17px; } div.container {width: 100%;}nav ul {list-style-type: none;padding: 0;} nav {float: left; max-width: 160px;margin: 0;padding: 1em;} article {margin-left: 170px;padding: 1em;overflow: hidden;} .table-size { width:100%;}.total-pos{margin-left:550px; font-size:16px; font-weight:bold;}</style>');
                frameDoc.document.write('</head><body>');
                //Append the external CSS file.
                frameDoc.document.write('<link href="printstyle.css" rel="stylesheet" type="text/css" />');
                // frameDoc.document.write('<div><br><center><img src="images/logo2.png" width="120px" height="120px" style=" margin-top:30px; right:600px;"></center><br<br></div>');
                frameDoc.document.write('<div style="font-size:30px; margin-top:30px; font-family:Algerian; "><br><center><b>Irmalyns Cake And Catering</b><br></center></div><br>');
                frameDoc.document.write('<div style="font-size:12px; "><center>#22 Lempira St. Meralco Village Taytay Rizal</center><br></div>');
                frameDoc.document.write('<div style="font-size:12px; "><center>_________________________________________________________</center><br></div>');
                
                frameDoc.document.write('<div><br><br><br></div>');
                frameDoc.document.write('<div style="font-size:25px; font-style:italic; "><center><b>Sales Report For the Month Of '+currentMonth+' </b></center><br></div>');
                frameDoc.document.write('<div><br><br><br></div>');
                frameDoc.document.write('<div style="font-size:18px; font-style:italic; "></div>');
                frameDoc.document.write('<div><br><br><br></div>');

                 frameDoc.document.write('<div style="font-size:18px; font-style:italic; text-align:justify; ">Collected<table class="table-size"><tr><th>Client Name</th><th>Event Date</th><th>Amount</th></tr></tr>');


                   for (var i = 0; i < data['month'].length; i++) {

                 frameDoc.document.write('<tr> <td>'+data['client'][i]['payee']+'</td><td>'+data['client'][i]['created_at']+'</td><td>'+data['month'][i]['paid']+'</td></tr>');

                  }
                    
                frameDoc.document.write('</table><br></div><br><br>');  

                  frameDoc.document.write('<div style="font-size:18px; font-style:italic; text-align:justify; ">Refunds<table class="table-size"><tr><th>Client Name</th><th>Amount</th></tr>');



                   for (var i = 0; i < data['refunds'].length; i++) {

                 
                 frameDoc.document.write('<tr> <td>'+data['refunds'][i]['name']+'</td><td style="color:red;">'+data['refunds'][i]['received']+'</td></tr>');

                  }
                    
                frameDoc.document.write('</table><br></div><br><br>');

                
              

                frameDoc.document.write('<div><p class="total-pos">TOTAL : P '+tot+'.00</p></div>');
                frameDoc.document.write("</div>" );
                frameDoc.document.write('</body></html>');
                frameDoc.document.close();
                setTimeout(function () {
                    window.frames["frame1"].focus();
                    window.frames["frame1"].print();
                    frame1.remove();
                }, 500);
            },
            error: function(xhr){

                alert('yow!');

            }
        });
        
    }


    function years()
    {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    var currentMonth;
    if (mm == 1){
      currentMonth = "January";
    }
    if (mm == 2){
      currentMonth = "February";
    }
    if (mm == 3){
      currentMonth = "March";
    }
    if (mm == 4){
      currentMonth = "April";
    }
    if (mm == 5){
      currentMonth = "May";
    }
    if (mm == 6){
      currentMonth = "June";
    }
    if (mm == 7){
      currentMonth = "July";
    }
    if (mm == 8){
      currentMonth = "August";
    }
    if (mm == 9){
      currentMonth = "September";
    }
    if (mm == 10){
      currentMonth = "October";
    }
    if (mm == 11){
      currentMonth = "November";
    }
    if (mm == 12  ){
      currentMonth = "December";
    }
    if(dd<10){
        dd='0'+dd;
    } 
    if(mm<10){
        mm='0'+mm;
    }
    var today = dd+'/'+mm+'/'+yyyy;
    var total =0;
        $.ajax({
            type: "get",
            url:  "/CollectionReportYear",
            data: {
                year : yyyy,
            },
            success: function(data){

                var s = data['sum'];
                var r = data['ref'];
                var tot = data['gross'];

        
               var frame1 = $('<iframe />');
                frame1[0].name = "frame1";
                frame1.css({ "position": "absolute", "top": "-1000000px" });
                $("body").append(frame1);
                var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
                frameDoc.document.open();
                //Create a new HTML document.
                frameDoc.document.write('<html><head>');
                frameDoc.document.write('<style>table, th, td {border: 0.5px solid black;border-collapse: collapse; padding:3px; font-size:17px; } div.container {width: 100%;}nav ul {list-style-type: none;padding: 0;} nav {float: left; max-width: 160px;margin: 0;padding: 1em;} article {margin-left: 170px;padding: 1em;overflow: hidden;} .table-size { width:100%;}.total-pos{margin-left:550px; font-size:16px; font-weight:bold;}</style>');
                frameDoc.document.write('</head><body>');
                //Append the external CSS file.
                frameDoc.document.write('<link href="printstyle.css" rel="stylesheet" type="text/css" />');
                // frameDoc.document.write('<div><br><center><img src="images/logo2.png" width="120px" height="120px" style=" margin-top:30px; right:600px;"></center><br<br></div>');
                frameDoc.document.write('<div style="font-size:30px; margin-top:30px; font-family:Algerian; "><br><center><b>Irmalyns Cake And Catering</b><br></center></div><br>');
                frameDoc.document.write('<div style="font-size:12px; "><center>#22 Lempira St. Meralco Village Taytay Rizal</center><br></div>');
                frameDoc.document.write('<div style="font-size:12px; "><center>_________________________________________________________</center><br></div>');
                
                frameDoc.document.write('<div><br><br><br></div>');
                frameDoc.document.write('<div style="font-size:25px; font-style:italic; "><center><b>Sales Report For the Year '+yyyy+' </b></center><br></div>');
                frameDoc.document.write('<div><br><br><br></div>');
                frameDoc.document.write('<div style="font-size:18px; font-style:italic; "></div>');
                frameDoc.document.write('<div><br><br><br></div>');

                 frameDoc.document.write('<div style="font-size:18px; font-style:italic; text-align:justify; ">Collected<table class="table-size"><tr><th>Client Name</th><th>Event Date</th><th>Amount</th></tr></tr>');


                   for (var i = 0; i < data['month'].length; i++) {

                 frameDoc.document.write('<tr> <td>'+data['client'][i]['payee']+'</td><td>'+data['client'][i]['created_at']+'</td><td>'+data['month'][i]['paid']+'</td></tr>');

                  }
                    
                frameDoc.document.write('</table><br></div><br><br>');  

                 frameDoc.document.write('<div style="font-size:18px; font-style:italic; text-align:justify; ">Refunds<table class="table-size"><tr><th>Client Name</th><th>Amount</th></tr>');


                   for (var i = 0; i < data['refunds'].length; i++) {

                 frameDoc.document.write('<tr> <td>'+data['refunds'][i]['name']+'</td><td style="color:red;">'+data['refunds'][i]['received']+'</td></tr>');

                  }
                    
                frameDoc.document.write('</table><br></div><br><br>');

                
              

                frameDoc.document.write('<div><p class="total-pos">TOTAL : P '+tot+'.00</p></div>');
                frameDoc.document.write("</div>" );
                frameDoc.document.write('</body></html>');
                frameDoc.document.close();
                setTimeout(function () {
                    window.frames["frame1"].focus();
                    window.frames["frame1"].print();
                    frame1.remove();
                }, 500);
            },
            error: function(xhr){

                alert('yow!');

            }
        });
        
    }

    </script>
@endsection