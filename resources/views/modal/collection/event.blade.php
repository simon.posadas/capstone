<div class="modal fade" tabindex="-1" role="dialog" id="eventDoneConfirmationModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Done</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/event_done">
                    {{csrf_field()}}
                    <input type="hidden" class="id" name="id" id="event_doneID">
                    <div class="form-group">
                        <h4>Event is done?</h4>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Confirm</button>
            </div>
            </form>
        </div>
    </div>
</div>