<!-- edit modal -->
<div class="modal fade modal-size-s1" tabindex="-1" role="dialog" id="editModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edit Food</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="#" id="editForm">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="_method" value="PUT" />

                    <input type="hidden" name="id" id="editID">
                    <div class="form-group">
                        <label class="input-margin2">Food Name :</label>
                        <input id = "input-addCategory" type="text" class="form-control inpname" name="name">
                    </div>
                    <!-- <div class="control-group row-flex">
                        <label class="control-label label-margin1">Select food type :</label>
                        <div class="controls select2-container select2-size">
                            <select class="select-type select-margin" name="type" id="type">
                                @foreach($types as $type)
                                <option value="{{ $type->id }}">{{ $type->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div> -->
                    <div class="form-group div-margin4">
                        <label class="label-margin1">Price :</label>
                        <input id = "input-addPrice2" type="number" class="form-control inprice" name="price">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn-edit">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- end edit modal -->
