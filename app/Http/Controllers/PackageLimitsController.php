<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use DB;
use Validator;

class PackageLimitsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($package)
    {
        $package = App\Package::find($package);
        $types = App\FoodType::all();
        $categories = App\FoodCategory::all();
        // $package_categories = DB::table('packages_v')->where('package_id', '=', $package->id)->get();
        $package_categories = App\PackageLimit::where('package_id', '=', $package->id)
            ->get();
        
        // $type_name = $
        return view('package.category.index')
                ->with('package_categories', $package_categories)
                ->with('package', $package)
                ->with('types', $types)
                ->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $package)
    {
        $category = $request->get('category');
        $type = $request->get('type');
        $limit = $request->get('limit');

        $packages = DB::table('package_limits')
                    ->where('package_id', '=', $package)
                    ->where('category_id', '=', $category)
                    ->where('type_id', '=', $type)
                    ->get();

        if(count($packages) > 0) 
        {
            return back()->withInput()->withErrors([ 'The combination is no longer available']);
        }

        $package = App\Package::find($package);

        $validator = Validator::make([
            'package' => $package->id,
            'category' => $category,
            'type' => $type,
            'limit' => $limit
        ], $package->limitRules());

        if($validator->fails())
        {
            return back()->withInput()->withErrors($validator);
        }
        $id = $package->id;
        $package_limits = new App\PackageLimit;

        $package_limits->package_id = $id;
        $package_limits->category_id = $category;
        $package_limits->type_id = $type;
        $package_limits->amount = $limit;

        $package_limits->save();
        // DB::table('package_limits')->insert([
        //     'package_id' => $package->id,
        //     'category_id' => $category,
        //     'type_id' => $type,
        //     'amount' => $limit
        // ]);

        $request->session()->flash('success-message', 'Category Added to the Package');
        return redirect("food/package/$package->id/limit");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $package,  $id)
    {
        $category = $request->get('category');
        $type = $request->get('type');
        $limit = $request->get('limit');

        $packages = new App\Package;

        $validator = Validator::make([
            'package' => $package,
            'category' => $category,
            'type' => $type,
            'limit' => $limit
        ], $packages->updateLimitRules());

        if($validator->fails())
        {
            return back()->withInput()->withErrors($validator);
        }

        DB::table('package_limits')
                    ->where('package_id', '=', $package)
                    ->where('category_id', '=', $category)
                    ->where('type_id', '=', $type)
                    ->update([
                        'amount' => $limit
                    ]);

        $request->session()->flash('success-message', 'Package Category Updated');
        return redirect("food/package/$package/limit");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $package, $category )
    {
        $type = $request->get('type');

        $packages = new App\Package;

        $validator = Validator::make([
            'package' => $package,
            'category' => $category,
            'type' => $type
        ], $packages->deleteLimitRules());

        if($validator->fails())
        {
            return back()->withInput()->withErrors($validator);
        }

        DB::table('package_limits')
        ->where('package_id', '=', $package)
        ->where('category_id', '=', $category)
        ->where('type_id', '=', $type)
        ->delete();

        $request->session()->flash('success-message', 'Category Removed from the Package');
        return redirect("food/package/$package/limit");
    }
}
