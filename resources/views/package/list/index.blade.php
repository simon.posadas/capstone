@extends('layouts.template') 

@section('content')

@include('modal.package.food_list.add')
@include('modal.package.food_list.delete')

<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="/dashboard" title="Go to Home" class="tip-bottom">
                <i class="icon icon-home"></i> Home</a>
            <a href="{{ url('food/package') }}">Package</a>
            <a href="#" class="current">Food</a>
        </div>
        <h1>{{ $package->name }}</h1>
    </div>
    <div class="container-fluid">
        <hr>

        @if(session()->has('success-message'))
        <div class="alert alert-success">
            <ul>
                {{ session()->get('success-message') }}
            </ul>
        </div>
        @endif


        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif


        <div>

                <div class="pull-left">
                    <button class="btn btn-success btn-large icon icon-plus" data-toggle="modal" data-target="#addModal" type="button"> Add Food</button>
                </div>
                <form method="get" action="/searchPackage" role="search">
                    <div class="pull-right">   
                        <div class="input-group custom-search-form">
                            <input type="text" name="searchPackage" class="form-control" placeholder="Search" id="search_bar">
                            <button class="btn btn-info" id="adjust-height-right" type="submit"><i class="icon icon-search"></i> Search</button>
                        </div>
                    </div>
                </form>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="widget-box">

                            <div class="widget-content nopadding">

                                <table class="table table-bordered data-table" id="categoriesTable">
                                    <thead>
                                        <tr rowspan="2">
                                            <th colspan=2>Name:  {{ $package->name }}</th>
                                            <th colspan=2>Type: {{ $package->type }}</th>
                                        </tr>
                                        <tr rowspan="2">
                                            <th colspan=2>Price:  {{ $package->price }}</th>
                                            <th colspan=2></th>
                                        </tr>
                                        <tr rowspan="2">
                                            <th colspan=4>Food List</th>
                                        </tr>
                                        <tr>
                                            <th>Name</th>
                                            <th>Type</th>
                                            <th width="20%">Actions</th>
                                        </tr>
                                    </thead>
                                    @foreach($package_lists as $list)
                                    <tbody>
                                        <tr>
                                            <td>{{ $list->food_name }}</td>
                                            <td>{{ $list->cat_name }}</td>
                                            <td class="btn-center">
                                                <button id ="sizes" class="btn fa fa-trash btn-danger deletetype" data-id="{{ $list->package_id }}" data-food="{{ $list->food_id }}" data-toggle="modal" data-target="#deleteModal"> Delete</button>
                                            </td>
                                        </tr>
                                    </tbody>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

        </div>

        
    </div>
</div>

@endsection 

@section('scripts_include')

<script type="text/javascript">

    $(document).ready(function(){

        $('#categoriesTable').on('click', '.deletetype', function (event) {
            food = $(this).data('food')

            $('#deleteModal').find('#btn-delete').on('click', function () {

                $('#deleteForm').attr('action', function(i, value) {
                    return "{{ url("food/package/$package->id") }}" + /list/ + food  ;
                });

                $('#deleteForm').submit();
            });

        })

    })

</script>

@endsection