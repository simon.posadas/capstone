<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Validator;
use DB;

class BillingsController extends Controller
{
    public $status_list = [];
 
    public function __construct(App\Billing $billing)
    {
        $this->status_list = $billing->status_list;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $billings = App\Billing::all();

        // $type = App\Person::select('id as customer')
        //     ->get();



        return view('billing.index')
                ->with('billings', $billings);  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $reservations = App\Reservation::pluck('code', 'id');
        $billing_types = App\Billing::$types;
        return view('billing.create')
                ->with('reservations', $reservations)
                ->with('billing_types', $billing_types);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $description = $request->get('description');
        $reservation = $request->get('reservation');
        $amount = (int) $request->get('amount');
        $type = $request->get('billing_type');

        $billing = new App\Billing;

        $validator = Validator::make([
            'description' => $description,
            'reservation' => $reservation,
            'type' => $type,
            'amount' => $amount
        ], $billing->rules());

        $billing->code = $billing->generateCode();
        $billing->description = $description;
        $billing->reservation_id = $reservation;
        $billing->amount = $amount;
        $billing->billing_type = $type;
        $billing->status = 0;
        $billing->save();

        $reservation = App\Reservation::find($reservation);
        $reservation->total = $reservation->total + $amount;
        $reservation->save();

        $request->session()->flash('success-message', "Reservation billed!");
        return redirect('billing');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->has('pay'))
        {
            $billing = App\Billing::find($id);
        
            $validator = Validator::make([
                'billing' => $id
            ], $billing->deleteRules());

            if($validator->fails())
            {
                return back()->withInput()->withErrors($validator);
            }

            DB::beginTransaction();

            $billing->status = array_search('Paid', $this->status_list);
            $billing->save();

            $payment = new App\Payment;
            $payment->code = $payment->generateCode();
            $payment->amount = $billing->amount;
            $payment->reservation_id = $billing->reservation_id;
            $payment->details = "Payment for $billing->code";
            $payment->save(); 

            DB::commit();

            $request->session()->flash('success-message', 'Bill Settled');
            return redirect('billing');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    
    public function print($id)
    {
        $billing = App\Billing::find($id);

        $data = [
            'billing' => $billing,
        ];

        $filename = "Billing-".\Carbon\Carbon::now()->format('mdYHm').".pdf";
        $view = "billing.print.show";

        return $this->printPreview($view,$data,$filename);

    }

    public function generateReceipt($id)
    {
        
    }
}
