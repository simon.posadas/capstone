<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            create view packages_v
            as
            select 
            packages.id as package_id,
            packages.name as package_name,
            packages.code as package_code,
            packages.type as package_type,
            packages.price as package_price,
            packages.status as package_status,
            category_id as category_id,
            categories.name as category_name,
            package_limits.amount as limitation,
            food_types.id as type_id,
            food_types.name as type_name
            from packages
            join package_limits
            on packages.id = package_limits.package_id
            join categories 
            on categories.id = package_limits.category_id
            join food_types
            on food_types.id = package_limits.type_id
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("drop view packages_v");
    }
}
