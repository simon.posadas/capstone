<?php

return array(


    'pdf' => array(
        'enabled' => true,

        /**
         * windows
         * enable the path to your configuration
         * disable the other configuration
         */
        'binary' => base_path('public\rendering-engine\windows\bin\wkhtmltopdf.exe'),

        /**
         * linux
         */
        // 'binary' => base_path('public/rendering-engine/linux/wkhtmltox/bin/wkhtmltopdf'),
        'timeout' => false,
        'options' => array(
            'margin-right' => 3,
            'margin-left' => 3,
            'margin-bottom' => 3,
            'footer-center' => 'Page [page] of [toPage]',
            'footer-font-size' => 3,
        ),
        'env'     => array(),
    ),
        
    'image' => array(
        'enabled' => true,

        /**
         * windows
         */
        'binary' => base_path('public\rendering-engine\windows\bin\wkhtmltoimage.exe'),

        /**
         * linux
         */
        // 'binary' => base_path('public\rendering-engine\linux\wkhtmltox\bin\wkhtmltoimage'),
        'timeout' => false,
        'options' => array(),
        'env'     => array(),
    ),


);
