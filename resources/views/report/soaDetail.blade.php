@extends('layouts.template') 

@section('content')

<!--main-container-part-->
<div id="content">
	<div id="content-header">
		<div class="row-fluid">
			<div class="span12">
				<div class="widget-box">
					<div class="widget-title">
						<i class="fa fa-th fa-sm pt-1"></i>
						<h5>Statement of Account Report</h5>
					</div>
					<table id="datatables" class="table table-bordered data-table">
						<thead>
							<tr>
								<th style="width:2%">ID</th>
								<th>Code</th>
								<th>Location</th>
								<th>Reservation Date</th>
								<th>Event Date</th>
								<th>Status</th>
								<!--th>Action</th-->
							</tr>
						</thead>
						@foreach($reservations as $reservation)
						<tbody>
							<tr>
								<td>{{ $reservation->id }}</td>
								<td>{{ $reservation->code }}</td>
								<td>{{ $reservation->place }}</td>
								<td>{{ Carbon\Carbon::parse($reservation->created_at)->toDayDateTimeString  () }}</td>
								<td>{{ Carbon\Carbon::parse($reservation->event_date)->toDayDateTimeString  () }}</td>
								<td>{{ $reservation->status_name }}</td>
								<!--td style="text-align: center;width:5%;"><a href="/soaDetail?id={{ $reservation->id }}"><button class="btn btn-inverse" data-toggle="tooltip" data-placement="right" title="View Info">Details</button></a></td-->
							</tr>
						</tbody>
						@endforeach
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!--end-main-container-part-->

@endsection

@section('scripts_include')


@endsection