<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Contractor extends Model
{
    /**
     * Unique ID
     * @var type 
     */
    protected $primaryKey = 'contractor_id';
    
    /**
     * Table to connect
     * @var type 
     */
    protected $table = 'contractor';
    
    /**
     * Disable creation of created_at and updated_at
     * @var type 
     */
    public $timestamps = false;
    
    /**
     * Mass variable assignment
     * @var type 
     */
    protected $fillable = ['contractor_lname', 'contractor_fname', 'contractor_mname', 'contractor_age', 'contactNo', 'status'];
}
