@extends('layouts.printables.report')

@section('content')
  <style>
      th , tbody{
        text-align: center;
      }

      #content{
        font-family: "Times New Roman";
      }

      @media print {
          tr.page-break  { display: block; page-break-after: always; }
      }   

  </style>
  <div id="content" class="col-sm-12">
    <h4 class="text-center"> <small class="pull-right"></small></h4>
    <table class="table table-striped table-bordered table-condensed" id="inventoryTable" width="100%" cellspacing="0">
      <thead>
          @php
            $division = 3;
          @endphp
          <tr rowspan="2">
              <th class="text-left" colspan="{{ $division }}">
                <span style="font-weight:normal"></span> 
              </th>
              <th class="text-left" colspan="{{ $division }}">
                <span style="font-weight:normal"></span>
              </th>
          </tr>
          <tr>
            <th>Stock Number</th>
            <th>Details</th>
            <th>Unit</th>
            <th>Quantity</th>
            <th>Quantity Approved</th>
            <th>Final Quantity</th>
          </tr>
      </thead>
      <tbody>

      </tbody>
    </table>
  </div>

  <div id="footer" class="col-sm-12">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th class="col-sm-1"> billing Officer</th>
          <th class="col-sm-1"> Approved By</th>
        </tr>
      </thead>
      <tbody>

        <tr>
          <td class="text-center text-muted" style="opacity: 0.5; padding-top: 4%;">Signature over Printed Name</td>
          <td class="text-center text-muted" style="opacity: 0.5; padding-top: 4%;">Signature over Printed Name</td>
        </tr>

        <tr>
          <td class="text-center text-muted" style="opacity: 0.5">Position</td>
          <td class="text-center text-muted" style="opacity: 0.5">Position</td>
        </tr>

        <tr>
          <td class="text-center text-muted" style="opacity: 0.5">Date</td>
          <td class="text-center text-muted" style="opacity: 0.5">Date</td>
        </tr>


      </tbody>
    </table>

  </div>
@endsection