<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function collectionreport()
    {
       

    	return $this->printPreview('billing.print.collectreport');
               
    }

    public function refund()

    {
      
    	return $this->printPreview('billing.print.refundsummary');
               
    }

    public function reservation()
    {
      
    	return $this->printPreview('billing.print.reservesummary');
               
    }

    public function salesreport()
    {
      
    	return $this->printPreview('billing.print.salesreport');
               
    }
}
